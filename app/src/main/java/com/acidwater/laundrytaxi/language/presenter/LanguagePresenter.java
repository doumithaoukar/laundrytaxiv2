package com.acidwater.laundrytaxi.language.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.language.LanguageMvp;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class LanguagePresenter extends MvpBasePresenter<LanguageMvp.View> implements LanguageMvp.Presenter {

    private Context context;

    public LanguagePresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }


}
