package com.acidwater.laundrytaxi.language.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.SplashActivity;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.about.presenter.AboutPresenter;
import com.acidwater.laundrytaxi.language.LanguageMvp;
import com.acidwater.laundrytaxi.language.presenter.LanguagePresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class LanguageFragment extends MvpFragment<LanguageMvp.View, LanguageMvp.Presenter> implements View.OnClickListener, LanguageMvp.View {


    private MainActivityListener mListener;
    private Button english;
    private Button arabic;
    private TextView selectLanguage;

    public static LanguageFragment newInstance() {
        LanguageFragment aboutFragment = new LanguageFragment();
        return aboutFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setButtonColor();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_language, container, false);
        selectLanguage = (TextView) mView.findViewById(R.id.select_language);
        english = (Button) mView.findViewById(R.id.english);
        arabic = (Button) mView.findViewById(R.id.arabic);
        english.setOnClickListener(this);
        arabic.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.english:
                confirmChangeLanguage(getString(R.string.English), ConstantStrings.LANGUAGE_EN);
                setButtonColor();
                break;

            case R.id.arabic:
                confirmChangeLanguage(getString(R.string.Arabic), ConstantStrings.LANGUAGE_AR);
                setButtonColor();
                break;

        }
    }

    private void setButtonColor() {
        if (GeneralUtils.getSharedPref(getContext(), ConstantStrings.APP_LANGUAGE).equals("") || GeneralUtils.getSharedPref(getContext(), ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
            english.setBackground(getResources().getDrawable(R.drawable.bg_rectangle_rounded_corners_grey));
            arabic.setBackground(getResources().getDrawable(R.drawable.btn_rounded_transparent));

        } else {

            english.setBackground(getResources().getDrawable(R.drawable.btn_rounded_transparent));
            arabic.setBackground(getResources().getDrawable(R.drawable.bg_rectangle_rounded_corners_grey));
        }
    }

    @Override
    public LanguageMvp.Presenter createPresenter() {
        return new LanguagePresenter(getContext());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

    private void confirmChangeLanguage(String language, final String id) {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.change_language))
                .setMessage(getString(R.string.confirm_language) + " " + language)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        GeneralUtils.setSharedPref(getContext(), ConstantStrings.APP_LANGUAGE, id);
                        Intent it = new Intent(getActivity(), SplashActivity.class);
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                    }

                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }
}