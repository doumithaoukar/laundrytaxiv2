package com.acidwater.laundrytaxi;

/**
 * Created by Doumith on 7/8/2017.
 */
public class ConstantStrings {

    public static final String Base_URL = "https://laundry-taxi.com/api/";

    public static final String SERVICE_LOGIN = "login.php";
    public static final String SERVICE_PROFILE = "getProfile.php";
    public static final String SERVICE_UPLOAD_PIC = "updateProfilePic.php";
    public static final String SERVICE_LOGOUT = "logout.php";
    public static final String SERVICE_CANCEL_ORDER = "cancelOrder.php";
    public static final String SERVICE_PLACE_ORDER = "placeOrder.php";
    public static final String SERVICE_ORDER_HISTORY = "orderHistory.php";
    public static final String SERVICE_ORDER_DETAILS = "getOrderDetail.php";
    public static final String SERVICE_REGISTER_PROFILE = "registerProfile.php";
    public static final String SERVICE_REGISTER_GOOGLE_PLUS = "registrationThroughGoogle.php";
    public static final String SERVICE_REGISTER_FACEBOOK = "registrationThroughFacebook.php";
    public static final String SERVICE_VERIFY_CODE = "verifyCode.php";
    public static final String SERVICE_RESEND_VERIFY_CODE = "resendActivationCode.php";
    public static final String SERVICE_EDIT_PROFILE = "editProfile.php";
    public static final String SERVICE_NOTIFICAIONS = "getUserNotifications.php";
    public static final String SERVICE_CATALOG = "getCatalog.php";
    public static final String SERVICE_FORGOT_PASSWORD = "forgotPassword.php";
    public static final String SERVICE_CHANGE_PASSWORD = "updatePassword.php";
    public static final String SERVICE_PICKUP_DROP_OFF = "getPickupDropoffTimes.php";
    public static final String SERVICE_UPLOAD_PUSH_TOKEN = "uploadPushToken.php";
    public static final String SERVICE_HOW_INVITES_WORK = "howInvitesWork.php";
    public static final String SERVICE_APP_CONFIG = "appConfig.php";

    public static final int MENU_HOME = 0;
    public static final int MENU_PRICE_LIST = 1;
    public static final int MENU_PROFILE = 2;
    public static final int MENU_ORDERS = 3;
    public static final int MENU_MANAGE_PAYMENT = 4;
    public static final int MENU_NOTIFICATIONS = 5;
    public static final int MENU_FREE_LAUNDRY = 6;
    public static final int MENU_SETTINGS = 7;


    public static final int SELECT_PICTURE = 1000;

    public static final int SETTING_CHANGE_PASSWORD = 8;
    public static final int SETTING_CHANGE_LANGUAGE = 9;
    public static final int SETTING_CONTACT_US = 10;
    public static final int SETTING_ABOUT = 11;
    public static final int SETTING_FAQ = 12;
    public static final int SETTING_T_AND_C = 13;
    public static final int SETTING_LOGOUT = 14;
    public static final int HOW_INVITES_WORK = 15;
    public static final int EDIT_ORDER = 16;
    public static final int REWARD_POINTS = 17;


    public static final int MAP_FRAGMENT = 100;
    public static final int ORDER_DETAILS = 101;
    public static final int ORDER_ITEMS = 102;
    public static final int ORDER_CONFIRMATION = 103;


    public static final int ADDRESS_MAP_FRAGMENT = 201;

    public static final int ACCOUNT_ACTIVATED = 1;
    public static final int ACCOUNT_NOT_ACTIVATED = 0;
    public static final int ACCOUNT_BLOCKED = 2;

    public static final String APP_LANGUAGE = "app_language";
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_AR = "ar";

    public static final String USER_SESSION = "user_session";
    public static final String USER_TYPE = "user_type";
    public static final String USER_FACEBOOK = "user_facebook";
    public static final String USER_GOOGLE = "user_google";
    public static final String USER_EMAIL = "user_email";

    public static final String user_is_logged_in = "is_logged_in";
    public static final String user_need_verification = "user_need_verification";


    public static final int NB_CHARACTERS_OTHER_CONSTRUCTIONS = 150;


    public static final String TAG_MAP = "tag_map";
    public static final String TAG_ORDER_DETAILS = "tag_order_details";
    public static final String TAG_ORDER_ITEMS = "tag_order_items";
    public static final String TAG_ORDER_CONFIRMATION = "tag_order_confirmation";
    public static final String TAG_CHANGE_PASSWORD = "tag_change_password";


    public static final String MY_ORDERS_IN_PROGRESS = "In Progress";
    public static final String MY_ORDERS_History = "History";

    public static final String TYPE_ORDER_CREATED = "Created";
    public static final String TYPE_ORDER_PICKING_UP = "Picking Up";
    public static final String TYPE_ORDER_CLEANING = "Cleaning";
    public static final String TYPE_ORDER_DELIVERING = "Delivering";
    public static final String TYPE_ORDER_DELIVERED = "Delivered";
    public static final String TYPE_ORDER_CANCEL = "Cancelled";

 
    public static final String SERVICE_ABOUT = "About.php";
    public static final String SERVICE_FAQ = "FAQ.php";
    public static final String SERVICE_TERMS = "TermsAndConditions.php";
    public static final String SERVICE_CONTACT_US = "ContactUs.php";
    public static final String SERVICE_REWARD_POINTS = "HowFreeLaundryWorks.php";


    public static final int MULTIPLY_BY = 2;

}
