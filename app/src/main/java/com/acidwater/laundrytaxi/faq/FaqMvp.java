package com.acidwater.laundrytaxi.faq;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Doumith on 8/13/2017.
 */
public interface FaqMvp {


    interface Presenter extends MvpPresenter<View> {


    }

    interface View extends MvpView {



    }
}
