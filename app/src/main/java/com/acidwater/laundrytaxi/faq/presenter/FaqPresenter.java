package com.acidwater.laundrytaxi.faq.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.faq.FaqMvp;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class FaqPresenter extends MvpBasePresenter<FaqMvp.View> implements FaqMvp.Presenter {

    private Context context;

    public FaqPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }


}
