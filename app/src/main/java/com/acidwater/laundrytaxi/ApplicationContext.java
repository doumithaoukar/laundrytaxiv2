package com.acidwater.laundrytaxi;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Doumith on 7/3/2017.
 */
public class ApplicationContext extends Application {

    private static Context context;


    public static Context getAppContext()
    {
        return ApplicationContext.context;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationContext.context = getApplicationContext();

    }
}
