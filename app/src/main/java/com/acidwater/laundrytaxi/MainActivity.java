package com.acidwater.laundrytaxi;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.acidwater.laundrytaxi.about.view.AboutFragment;
import com.acidwater.laundrytaxi.address.view.AddressFragment;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.GPSONEvent;
import com.acidwater.laundrytaxi.contact.view.ContactUsFragment;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.faq.view.FaqFragment;
import com.acidwater.laundrytaxi.freelaundry.view.FreeLaundryFragment;
import com.acidwater.laundrytaxi.freelaundry.view.HowInvitesWorkFragment;
import com.acidwater.laundrytaxi.language.view.LanguageFragment;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.managepayment.view.ManagePaymentFragment;
import com.acidwater.laundrytaxi.myorders.view.OrdersFragment;
import com.acidwater.laundrytaxi.orderconfirmation.view.OrderConfirmationFragment;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.orderitems.view.OrderItemsFragment;
import com.acidwater.laundrytaxi.changepassword.view.ChangePasswordFragment;
import com.acidwater.laundrytaxi.home.view.HomeFragment;
import com.acidwater.laundrytaxi.menu.view.MenuAdapter;
import com.acidwater.laundrytaxi.map.view.MapFragment;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.menu.model.MenuInteractor;
import com.acidwater.laundrytaxi.notifications.view.NotificationsFragment;
import com.acidwater.laundrytaxi.orderdetails.view.OrderDetailsFragment;
import com.acidwater.laundrytaxi.ordershistory.details.view.OrderHistoryDetailsFragment;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.pricelist.view.PriceListFragment;
import com.acidwater.laundrytaxi.profile.model.ProfileInteractor;
import com.acidwater.laundrytaxi.profile.view.ProfileFragment;
import com.acidwater.laundrytaxi.rewardpoints.view.RewardPointsFragment;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.settings.view.SettingsFragment;
import com.acidwater.laundrytaxi.termsandconditions.view.TermsFragment;
import com.acidwater.laundrytaxi.utils.FireBaseConfig;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.NotificationUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.io.File;
import java.io.IOException;
import java.util.List;

import retrofit2.http.Body;

/**
 * Created by Doumith on 7/3/2017.
 */
public class MainActivity extends AppCompatActivity implements MainActivityListener {

    private Toolbar mToolbar;
    private Drawable mBurgerIcon;
    boolean doubleBackToExitPressedOnce = false;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final int MY_PERMISSIONS_READ_STORAGE = 100;
    public static final int ACTION_OPEN_GPS = 100;

    private Fragment selectedFragment = null;

    private Handler mHandler = new Handler();
    private Runnable mRunnable;

    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;

    ProgressDialog progressDialog;
    MenuAdapter menuAdapter;

    public boolean activityFirstLaunch = true;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getProfile();
        getRegistrationID();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        mToolbar.setTitle(R.string.app_name);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        new Handler().post(new Runnable() {
            @Override
            public void run() {
                getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            // show back button
                            mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
                            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        } else {
                            //show hamburger
                            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                            mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
                            mToolbar.setTitle(R.string.app_name);

                        }
                    }
                });

            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(FireBaseConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(FireBaseConfig.TOPIC_GLOBAL);
                    getRegistrationID();

                } else if (intent.getAction().equals(FireBaseConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    navigationToScreen(ConstantStrings.MENU_NOTIFICATIONS);
                }
            }
        };


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        setNavigationDrawer();

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    String mTag = getSupportFragmentManager().getFragments().get(getSupportFragmentManager().
                            getBackStackEntryCount() - 1).getTag();
                    if (mTag != null && !mTag.equals(getString(R.string.tag_home_screen))) {
                        mToolbar.setTitle(mTag);
                    } else {
                        mToolbar.setTitle(R.string.app_name);
                    }
                    onBackPressed();

                } else {
                    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        drawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        drawerLayout.openDrawer(GravityCompat.START);
                    }
                }
            }
        });

        replaceFragment(HomeFragment.newInstance(), getString(R.string.app_name));

        if (getIntent().getExtras() != null) {
            //from push notification
            for (String key : getIntent().getExtras().keySet()) {
                if (key != null && key.contains("google")) {
                    navigationToScreen(ConstantStrings.MENU_NOTIFICATIONS);
                    break;
                }
            }
        }

        mBurgerIcon = mToolbar.getNavigationIcon();
    }


    private void setNavigationDrawer() {
        MenuInteractor menuInteractor = new MenuInteractor(this);
        List<Menu> menus = menuInteractor.getMenuList();
        menuAdapter = new MenuAdapter(this, menus);
        menuAdapter.setOnItemClickListener(new MenuAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(Menu menu) {
                navigationToScreen(menu.getId());
                drawerLayout.closeDrawer(GravityCompat.START);
            }

            @Override
            public void onProfileClicked() {
                drawerLayout.closeDrawer(GravityCompat.START);
                selectPicture();
            }
        });
        recyclerView.setAdapter(menuAdapter);
    }


    @Override
    public void onBackPressed() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        } else if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {
            if (selectedFragment instanceof HomeFragment) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getString(R.string.double_press_to_exit), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                replaceFragment(HomeFragment.newInstance(), getString(R.string.app_name));
            }
        }
    }

    @Override
    public void navigationToScreen(int sectionID) {
        switch (sectionID) {

            case ConstantStrings.MENU_HOME:
                replaceFragment(HomeFragment.newInstance(), getString(R.string.app_name));
                break;

            case ConstantStrings.MENU_PRICE_LIST:
                replaceFragment(PriceListFragment.newInstance(), getString(R.string.menu_price_list));
                break;

            case ConstantStrings.MENU_PROFILE:
                replaceFragment(ProfileFragment.newInstance(), getString(R.string.menu_profile));
                break;
            case ConstantStrings.MENU_ORDERS:
                replaceFragment(OrdersFragment.newInstance(), getString(R.string.menu_orders));
                break;

            case ConstantStrings.MENU_NOTIFICATIONS:
                replaceFragment(NotificationsFragment.newInstance(), getString(R.string.menu_notifications));
                break;

            case ConstantStrings.MENU_SETTINGS:
                replaceFragment(SettingsFragment.newInstance(), getString(R.string.menu_settings));
                break;

            case ConstantStrings.MAP_FRAGMENT:
                //clearing data
                GeneralUtils.saveOrderItems(getBaseContext(), null);
                addFragment(MapFragment.newInstance(null), getString(R.string.choose_location));
                break;


            case ConstantStrings.ADDRESS_MAP_FRAGMENT:
                addFragment(AddressFragment.newInstance(), getString(R.string.select_address));
                break;


            case ConstantStrings.ORDER_DETAILS:
                addFragment(OrderDetailsFragment.newInstance(null), getString(R.string.order_details));
                break;


            case ConstantStrings.SETTING_CHANGE_PASSWORD:
                addFragment(ChangePasswordFragment.newInstance(), getString(R.string.change_password));
                break;


            case ConstantStrings.SETTING_ABOUT:
                addFragment(AboutFragment.newInstance(), getString(R.string.about));
                break;

            case ConstantStrings.SETTING_FAQ:
                addFragment(FaqFragment.newInstance(), getString(R.string.faq));
                break;

            case ConstantStrings.SETTING_T_AND_C:
                addFragment(TermsFragment.newInstance(), getString(R.string.T_and_c));

                break;

            case ConstantStrings.SETTING_CONTACT_US:
                addFragment(ContactUsFragment.newInstance(), getString(R.string.contact_us));
                break;

            case ConstantStrings.SETTING_CHANGE_LANGUAGE:
                addFragment(LanguageFragment.newInstance(), getString(R.string.change_language));
                break;

            case ConstantStrings.MENU_FREE_LAUNDRY:
                replaceFragment(FreeLaundryFragment.newInstance(), getString(R.string.menu_free_laundry));
                break;

            case ConstantStrings.MENU_MANAGE_PAYMENT:
                replaceFragment(ManagePaymentFragment.newInstance(), getString(R.string.menu_manage_payment));
                break;

            case ConstantStrings.HOW_INVITES_WORK:
                addFragment(HowInvitesWorkFragment.newInstance(), getString(R.string.menu_free_laundry));
                break;

            case ConstantStrings.REWARD_POINTS:
                addFragment(RewardPointsFragment.newInstance(), getString(R.string.my_reward_points));

                break;


        }
    }

    private void selectPicture() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showReadPermissionDialog();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_READ_STORAGE);
            }

        } else {
            choosePictureFromGallery();
        }

    }

    private void addFragment(Fragment fragment, String title) {
        selectedFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, title).addToBackStack(title);
        fragmentTransaction.commit();
        mToolbar.setTitle(title);
    }

    private void replaceFragment(Fragment fragment, String title) {
        selectedFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFrag = fragmentManager.findFragmentByTag(title);
        if (currentFrag != null) {
            if (fragment.getClass().getSimpleName().equalsIgnoreCase(currentFrag.getClass().getSimpleName())) {
                return;
            }
        }
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .replace(R.id.container, selectedFragment, title)
                .commit();
        mToolbar.setTitle(title);
    }

    @Override
    public void initializeMapView() {

        if (selectedFragment != null && selectedFragment instanceof MapFragment) {

            MapFragment mapFragment = (MapFragment) selectedFragment;

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showLocationPermissionDialog();
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }

            } else {
                mapFragment.syncMap();
            }
        } else if (selectedFragment != null && selectedFragment instanceof AddressFragment) {

            AddressFragment addressFragment = (AddressFragment) selectedFragment;

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showLocationPermissionDialog();
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }

            } else {
                addressFragment.syncMap();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void destroyFragments() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    public void navigateToOrderDetails(OrderHistory orderHistory) {
        selectedFragment = OrderHistoryDetailsFragment.newInstance(orderHistory);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, selectedFragment, getString(R.string.menu_orders)).
                addToBackStack(getString(R.string.menu_orders));
        fragmentTransaction.commit();
        mToolbar.setTitle(getString(R.string.menu_orders));
    }

    @Override
    public void navigateToEditOrder(OrderHistory orderHistory) {
        selectedFragment = MapFragment.newInstance(orderHistory);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, selectedFragment, getString(R.string.menu_orders)).
                addToBackStack(getString(R.string.menu_orders));
        fragmentTransaction.commit();
        mToolbar.setTitle(getString(R.string.menu_orders));
    }

    @Override
    public void navigateToOrderItems(Timepoint[] pickupTimePoints, Timepoint[] dropOffTimePoints,
                                     DeliveryMethod deliveryMethod, OrderHistory orderHistory, double minOrder
    ) {
        addFragment(OrderItemsFragment.newInstance(pickupTimePoints, dropOffTimePoints, deliveryMethod, orderHistory, minOrder), getString(R.string.order_details));

    }

    @Override
    public Drawable getNavigationIcon() {
        return mBurgerIcon;
    }

    @Override
    public void getUserProfile() {
        getProfile();
    }

    @Override
    public void navigationToScreen(int sectionID, OrderHistory orderHistory) {
        switch (sectionID) {

            case ConstantStrings.ORDER_DETAILS:
                addFragment(OrderDetailsFragment.newInstance(orderHistory), getString(R.string.order_details));
                break;


            case ConstantStrings.ORDER_CONFIRMATION:
                addFragment(OrderConfirmationFragment.newInstance(orderHistory), getString(R.string.order_confirmed));
                break;
        }

    }


    private void showLocationPermissionDialog() {

        new AlertDialog.Builder(this).setTitle(getString(R.string.location_permission_needed))
                .setMessage(getString(R.string.location_permission_text))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_LOCATION);
                    }
                })
                .create()
                .show();
    }

    private void showReadPermissionDialog() {

        new AlertDialog.Builder(this).setTitle(getString(R.string.permission_needed))
                .setMessage(getString(R.string.permission_read_file))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_READ_STORAGE);
                    }
                })
                .create()
                .show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (selectedFragment != null && selectedFragment instanceof MapFragment) {
                        ((MapFragment) selectedFragment).buildGoogleApiClient();
                    }
                } else {

                    Toast.makeText(MainActivity.this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
                }

            }
            break;


            case MY_PERMISSIONS_READ_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePictureFromGallery();
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

    @Override
    public void showGpsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.Gps_off_title))
                .setMessage(getString(R.string.Gps_off_text))
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), ACTION_OPEN_GPS);
                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }

        GeneralUtils.saveOrderItems(getBaseContext(), null);
    }

    private void choosePictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ConstantStrings.SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_OPEN_GPS) {
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    MainBus.getInstance().send(new GPSONEvent());
                }
            };
            mHandler.postDelayed(mRunnable, 1000);
        } else if (data != null) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == ConstantStrings.SELECT_PICTURE) {
                    Uri selectedImageUri = data.getData();
                    Intent intent = CropImage.activity(selectedImageUri).getIntent(MainActivity.this);
                    startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

                } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    uploadProfilePicture(GeneralUtils.getRealPathFromURIPath(result.getUri(), MainActivity.this));
                }

            }
        }

    }

    private void uploadProfilePicture(String filePath) {
        File file = new File(filePath);
        final ProfileInteractor profileInteractor = new ProfileInteractor(this);
        profileInteractor.uploadProfilePicture(file).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage(getString(R.string.uploading_image));
                progressDialog.show();
            }

            @Override
            public void onComplete() {
                progressDialog.dismiss();
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                //getProfile
                profileInteractor.getProfile().subscribeWith(new ServiceObserver<UserWrapper>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(UserWrapper userWrapper) {
                        menuAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(LaundryTaxiException e) {

                    }
                });
            }

            @Override
            public void onError(LaundryTaxiException e) {
                Toast.makeText(getBaseContext(), e.errorMessage, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });
    }

    public void getProfile() {
        ProfileInteractor profileInteractor = new ProfileInteractor(this);
        profileInteractor.getProfile();
    }


    private String getRegistrationID() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(FireBaseConfig.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
            ProfileInteractor profileInteractor = new ProfileInteractor(this);
            profileInteractor.uploadPushToke(regId).subscribeWith(new ServiceObserver<ServerResponse>() {
                @Override
                public void onStart() {

                }

                @Override
                public void onComplete() {

                }

                @Override
                public void onNext(ServerResponse serverResponse) {
                    String s = "hello";
                }

                @Override
                public void onError(LaundryTaxiException e) {
                    String s = "hello";
                }
            });
        }
        return regId;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(FireBaseConfig.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(FireBaseConfig.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
