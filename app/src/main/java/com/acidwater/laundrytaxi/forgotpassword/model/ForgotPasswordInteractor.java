package com.acidwater.laundrytaxi.forgotpassword.model;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.forgotpassword.ForgotPasswordMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ForgotPasswordInteractor implements ForgotPasswordMvp.Interactor {


    private ForgotPasswordApi forgotPasswordApi;


    public ForgotPasswordInteractor() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        forgotPasswordApi = retrofit.create(ForgotPasswordApi.class);
    }


    @Override
    public Observable<ServerResponse> submitEmail(String email) {

        Observable<ServerResponse> observable = null;
        Email mEmail = new Email(email);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (mEmail.toJson()).toString());
        observable =
                forgotPasswordApi.submitEmail(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface ForgotPasswordApi {
        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_FORGOT_PASSWORD)
        Observable<ServerResponse> submitEmail(@Body RequestBody body);
    }

    private class Email {
        private String email;

        public Email(String email) {
            this.email = email;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }

}
