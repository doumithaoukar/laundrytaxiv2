package com.acidwater.laundrytaxi.forgotpassword.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.IntroActivityListener;
import com.acidwater.laundrytaxi.forgotpassword.ForgotPasswordMvp;
import com.acidwater.laundrytaxi.forgotpassword.presenter.ForgotPasswordPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class ForgotPasswordFragment extends MvpFragment<ForgotPasswordMvp.View, ForgotPasswordMvp.Presenter> implements View.OnClickListener, ForgotPasswordMvp.View {


    private Button btnContinue;
    private EditText email;
    private RelativeLayout progressBar;
    private IntroActivityListener mListener;


    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        return forgotPasswordFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_forgot_password, container, false);

        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        email = (EditText) mView.findViewById(R.id.email);
        btnContinue.setOnClickListener(this);

        return mView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validateEmail(email.getText().toString());
                break;

        }
    }

    @Override
    public ForgotPasswordMvp.Presenter createPresenter() {
        return new ForgotPasswordPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void setResultSuccessful(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        destroyThis();
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }
}