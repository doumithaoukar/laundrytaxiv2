package com.acidwater.laundrytaxi.registration.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.forgotpassword.ForgotPasswordMvp;
import com.acidwater.laundrytaxi.forgotpassword.model.ForgotPasswordInteractor;
import com.acidwater.laundrytaxi.login.model.LoginInteractor;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.registration.RegistrationMvp;
import com.acidwater.laundrytaxi.registration.model.RegistrationInteractor;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

/**
 * Created by Doumith on 7/13/2017.
 */
public class RegistrationPresenter extends MvpBasePresenter<RegistrationMvp.View> implements RegistrationMvp.Presenter {

    private Context context;
    private RegistrationInteractor registrationInteractor;
    private LoginInteractor loginInteractor;
    private CompositeDisposable compositeDisposable;

    public RegistrationPresenter(Context context) {
        this.context = context;
        registrationInteractor = new RegistrationInteractor(context);
        loginInteractor = new LoginInteractor(context);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void register(String firstName, String lastName, String email, String password, String mobileNumber, String invitationCode) {

        compositeDisposable.add(registrationInteractor.register(firstName, lastName, email, password, mobileNumber, invitationCode).subscribeWith(new ServiceObserver<UserWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(UserWrapper userWrapper) {

                GeneralUtils.setSharedPref(context, ConstantStrings.USER_TYPE, ConstantStrings.USER_EMAIL);
                GeneralUtils.saveUserProfile(context, userWrapper);

                if (isViewAttached()) {
                    if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_NOT_ACTIVATED) {
                        getView().openVerificationFragment();
                        GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_need_verification);
                    } else if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_BLOCKED) {
                        getView().showErrorMessage(context.getString(R.string.account_blocked));

                    } else {
                        GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                        getView().openMainActivity();
                    }
                }

            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void validate(String firstName, String lastName, String email, String password, String confirmPassword, String mobileNumber, String invitationCode) {
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(email)
                || TextUtils.isEmpty(password) || TextUtils.isEmpty(mobileNumber) || TextUtils.isEmpty(confirmPassword)) {

            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_required_fields));
                return;
            }
        } else if (!GeneralUtils.isEmailValid(email)) {
            getView().showErrorMessage(context.getString(R.string.invalid_email));
            return;
        } else if (!confirmPassword.equals(password)) {
            getView().showErrorMessage(context.getString(R.string.password_dont_match));
            return;
        }

        register(firstName, lastName, email, password, mobileNumber, invitationCode);
    }

    @Override
    public void registerWithGooglePlus(String email, String firstname, String lastname, String userID, String image, String token) {

        compositeDisposable.add(registrationInteractor.registerWithGooglePlus(email, firstname, lastname, userID, image, token).subscribeWith(new ServiceObserver<UserWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(UserWrapper userWrapper) {


                GeneralUtils.setSharedPref(context, ConstantStrings.USER_TYPE, ConstantStrings.USER_GOOGLE);
                GeneralUtils.saveUserProfile(context, userWrapper);

                if (isViewAttached()) {
                    if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_NOT_ACTIVATED) {
                        getView().openVerificationFragment();
                        GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_need_verification);
                    } else if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_BLOCKED) {
                        getView().showErrorMessage(context.getString(R.string.account_blocked));

                    } else {
                        GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                        getView().openMainActivity();
                    }
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }

                if (isViewAttached()) {
                    if (e.reason == LaundryTaxiException.Reason.ALREADY_LOGIN) {
                        signOut();
                    } else {
                        getView().showErrorMessage(e.errorMessage);
                    }

                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void registerWithFacebook(String email, String firstname, String lastname, final String userID, String image, String token) {

        compositeDisposable.add(registrationInteractor.registerWithFacebook(email, firstname, lastname, userID, image, token).subscribeWith(new ServiceObserver<UserWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(UserWrapper userWrapper) {

                GeneralUtils.setSharedPref(context, ConstantStrings.USER_TYPE, ConstantStrings.USER_FACEBOOK);
                GeneralUtils.saveUserProfile(context, userWrapper);

                if (isViewAttached()) {
                    if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_NOT_ACTIVATED) {
                        getView().openVerificationFragment();
                        GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_need_verification);
                    } else if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_BLOCKED) {
                        getView().showErrorMessage(context.getString(R.string.account_blocked));

                    } else {
                        GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                        getView().openMainActivity();
                    }
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }

                if (isViewAttached()) {
                    if (e.reason == LaundryTaxiException.Reason.ALREADY_LOGIN) {
                        signOut();
                    } else {
                        getView().showErrorMessage(e.errorMessage);
                    }

                    getView().HideLoadingView();
                }
            }
        }));
    }


    @Override
    public void signOut() {

        compositeDisposable.add(loginInteractor.logout().subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                ClearableCookieJar cookieJar =
                        new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                cookieJar.clear();
                GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, "");
            }

            @Override
            public void onError(LaundryTaxiException e) {

            }
        }));
    }


}
