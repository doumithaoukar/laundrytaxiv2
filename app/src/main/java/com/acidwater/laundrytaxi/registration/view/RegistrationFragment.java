package com.acidwater.laundrytaxi.registration.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.laundrytaxi.BaseFragment;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.IntroActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.registration.RegistrationMvp;
import com.acidwater.laundrytaxi.registration.presenter.RegistrationPresenter;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.google.android.gms.auth.api.Auth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Doumith on 7/6/2017.
 */
public class RegistrationFragment extends MvpFragment<RegistrationMvp.View, RegistrationMvp.Presenter> implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, RegistrationMvp.View {


    private Button btnContinue;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private EditText mobileNumber;
    private EditText invitationCode;
    private RelativeLayout progressBar;

    private LinearLayout btnFacebook;
    private LinearLayout btnGooglePlus;

    public static final int GOOGLE_PLUS_SIGN_IN = 100;

    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;

    private IntroActivityListener mListener;

    public static RegistrationFragment newInstance() {
        RegistrationFragment registrationFragment = new RegistrationFragment();
        return registrationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_registration, container, false);

        firstName = (EditText) mView.findViewById(R.id.firstname);
        lastName = (EditText) mView.findViewById(R.id.lastname);
        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        email = (EditText) mView.findViewById(R.id.email);
        password = (EditText) mView.findViewById(R.id.password);
        confirmPassword = (EditText) mView.findViewById(R.id.confirmPassword);
        mobileNumber = (EditText) mView.findViewById(R.id.mobileNumber);
        invitationCode = (EditText) mView.findViewById(R.id.invitationCode);
        btnContinue.setOnClickListener(this);

        btnFacebook = (LinearLayout) mView.findViewById(R.id.btn_facebook);
        btnGooglePlus = (LinearLayout) mView.findViewById(R.id.btn_google_plus);
        btnFacebook.setOnClickListener(this);
        btnGooglePlus.setOnClickListener(this);

        mobileNumber.setText(getString(R.string.mobile_prefix));
        Selection.setSelection(mobileNumber.getText(), mobileNumber.getText().length());


        mobileNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith(getString(R.string.mobile_prefix))){
                    mobileNumber.setText(getString(R.string.mobile_prefix));
                    Selection.setSelection(mobileNumber.getText(), mobileNumber.getText().length());

                }

            }
        });
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.webclient_id))
                .requestEmail()
                .requestProfile()
                .requestId()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        handleFacebookSignInResult();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validate(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(),
                        password.getText().toString(), confirmPassword.getText().toString(), mobileNumber.getText().toString(), invitationCode.getText().toString()
                );
                break;


            case R.id.btn_facebook:
                signInByFacebook();
                break;

            case R.id.btn_google_plus:
                signInByGooglePlus();
                break;

        }
    }

    @Override
    public RegistrationMvp.Presenter createPresenter() {
        return new RegistrationPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void openMainActivity() {
        mListener.navigateToSection(IntroActivity.MAIN_ACTIVITY);
    }

    @Override
    public void openVerificationFragment() {
        destroyThis();
        mListener.navigateToSection(IntroActivity.VERIFICATION);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });

    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == GOOGLE_PLUS_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGooglePlusSignInResult(result);
        }
    }

    private void signInByGooglePlus() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_PLUS_SIGN_IN);
    }


    private void signInByFacebook() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(),
                Arrays.asList("public_profile"));

    }

    private void handleGooglePlusSignInResult(GoogleSignInResult result) {
        Log.d("Google", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String token = acct.getIdToken();
            String firstname = acct.getGivenName();
            String lastname = acct.getFamilyName();
            String userId = acct.getId();
            Uri image = acct.getPhotoUrl();
            String pictureUrl = "";
            if (image != null) {
                pictureUrl = image.toString();
            }
            String email = acct.getEmail();
            presenter.registerWithGooglePlus(email, firstname, lastname, userId, pictureUrl, token);
            signOut();
            revokeAccess();
        } else {
            // Signed out, show unauthenticated UI.
            signOut();
        }
    }

    private void handleFacebookSignInResult() {
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject json,
                                            GraphResponse response) {


                                        String full_name = json.optString("name");
                                        String last_name = json.optString("last_name");
                                        String first_name = json.optString("first_name");
                                        String gender = json.optString("gender");
                                        String id = json.optString("id");
                                        String email = json.optString("email");
                                        String img_value = "http://graph.facebook.com/" + id + "/picture?type=large";
                                        presenter.registerWithFacebook(email, first_name, last_name, id, img_value, loginResult.getAccessToken().getToken());


                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday,first_name,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("TEST", "On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        String ersdsdror = "";
                    }

                });
    }

}