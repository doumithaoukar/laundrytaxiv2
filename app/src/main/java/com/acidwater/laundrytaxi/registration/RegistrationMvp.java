package com.acidwater.laundrytaxi.registration;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Doumith on 7/6/2017.
 */
public interface RegistrationMvp {

    interface Interactor {
        Observable<UserWrapper> register(String firstName, String lastName, String email, String password, String mobileNumber, String invitationCode);

        Observable<UserWrapper> registerWithGooglePlus(String email, String firstname, String lastname, String userID, String image, String token);

        Observable<UserWrapper> registerWithFacebook(String email, String firstname, String lastname, String userID, String image, String token);

    }

    interface Presenter extends MvpPresenter<View> {

        void register(String firstName, String lastName, String email, String password, String mobileNumber, String invitationCode);

        void validate(String firstName, String lastName, String email, String password, String confirmPassword, String mobileNumber, String invitationCode);

        void registerWithGooglePlus(String email, String firstname, String lastname, String userID, String image, String token);

        void registerWithFacebook(String email, String firstname, String lastname, String userID, String image, String token);


        void signOut();
    }

    interface View extends MvpView {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void openMainActivity();

        void openVerificationFragment();

    }
}
