package com.acidwater.laundrytaxi.registration.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.registration.RegistrationMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class RegistrationInteractor implements RegistrationMvp.Interactor {


    private RegistrationApi registrationApi;


    public RegistrationInteractor(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().cookieJar(cookieJar).addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        registrationApi = retrofit.create(RegistrationApi.class);
    }


    @Override
    public Observable<UserWrapper> register(String firstName, String lastName, String email, String password, String mobileNumber, String invitationCode) {

        Observable<UserWrapper> observable = null;
        UserRequest mUser = new UserRequest(firstName, lastName, email, password, mobileNumber, invitationCode);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (mUser.toJson()).toString());
        observable =
                registrationApi.register(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<UserWrapper> registerWithGooglePlus(String email, String firstname, String lastname, String userID, String image, String token) {
        Observable<UserWrapper> observable = null;
        GoogleRequest mUser = new GoogleRequest(email, firstname, lastname, userID, image, token);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (mUser.toJson()).toString());
        observable =
                registrationApi.registerWithGooglePlus(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<UserWrapper> registerWithFacebook(String email, String firstname, String lastname, String userID, String image, String token) {
        Observable<UserWrapper> observable = null;
        GoogleRequest mUser = new GoogleRequest(email, firstname, lastname, userID, image, token);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (mUser.toJson()).toString());
        observable =
                registrationApi.registerWithFacebook(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface RegistrationApi {
        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_REGISTER_PROFILE)
        Observable<UserWrapper> register(@Body RequestBody body);


        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_REGISTER_GOOGLE_PLUS)
        Observable<UserWrapper> registerWithGooglePlus(@Body RequestBody body);


        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_REGISTER_FACEBOOK)
        Observable<UserWrapper> registerWithFacebook(@Body RequestBody body);

    }

    private class UserRequest {
        @SerializedName("user")
        private String email;

        @SerializedName("pass")
        private String pass;

        @SerializedName("firstname")
        private String firstName;

        @SerializedName("lastname")
        private String lastName;

        @SerializedName("phone")

        private String mobileNumber;

        @SerializedName("invitationCode")
        private String invitationCode;


        public UserRequest(String firstname, String lastname, String email, String pass, String mobileNumber, String invitationCode) {
            this.firstName = firstname;
            this.lastName = lastname;
            this.email = email;
            this.pass = pass;
            this.mobileNumber = mobileNumber;
            this.invitationCode = invitationCode;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }

    private class GoogleRequest {

        @SerializedName("email")
        private String email;

        @SerializedName("firstname")
        private String firstName;

        @SerializedName("lastname")
        private String lastName;


        @SerializedName("userId")
        private String userID;


        @SerializedName("profileImageUrl")
        private String image;

        @SerializedName("idToken")
        private String token;

        public GoogleRequest(String email, String firstName, String lastName, String userID, String image, String token) {
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
            this.userID = userID;
            this.image = image;
            this.token = token;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }

}
