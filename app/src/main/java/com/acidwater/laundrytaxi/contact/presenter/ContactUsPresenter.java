package com.acidwater.laundrytaxi.contact.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.contact.ContactMvp;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ContactUsPresenter extends MvpBasePresenter<ContactMvp.View> implements ContactMvp.Presenter {

    private Context context;

    public ContactUsPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }

    @Override
    public void getData() {
        if (isViewAttached()) {

        }
    }

    @Override
    public String getEmail() {
        return context.getString(R.string.laundryEmail);
    }

    @Override
    public String getPhoneNumber() {
        return context.getString(R.string.laundryMobileNumber).trim();
    }
}
