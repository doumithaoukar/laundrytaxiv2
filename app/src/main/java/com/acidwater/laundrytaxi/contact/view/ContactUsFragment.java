package com.acidwater.laundrytaxi.contact.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.about.presenter.AboutPresenter;
import com.acidwater.laundrytaxi.contact.ContactMvp;
import com.acidwater.laundrytaxi.contact.presenter.ContactUsPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class ContactUsFragment extends MvpFragment<ContactMvp.View, ContactMvp.Presenter> implements View.OnClickListener, ContactMvp.View {


    private MainActivityListener mListener;
    private WebView webView;


    public static ContactUsFragment newInstance() {
        ContactUsFragment contactUsFragment = new ContactUsFragment();
        return contactUsFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_contact_us, container, false);

        webView = (WebView) mView.findViewById(R.id.webview);
        webView.loadUrl(ConstantStrings.Base_URL + "" + ConstantStrings.SERVICE_CONTACT_US+ "?lang="+ GeneralUtils.getSharedPref(getContext(),ConstantStrings.APP_LANGUAGE));
        webView.setBackgroundColor(Color.TRANSPARENT);
        return mView;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public ContactMvp.Presenter createPresenter() {
        return new ContactUsPresenter(getContext());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }


}