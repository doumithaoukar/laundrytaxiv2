package com.acidwater.laundrytaxi.contact;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Doumith on 8/13/2017.
 */
public interface ContactMvp {


    interface Presenter extends MvpPresenter<View> {

        void getData();
        String getEmail();

        String getPhoneNumber();
    }

    interface View extends MvpView {



    }
}
