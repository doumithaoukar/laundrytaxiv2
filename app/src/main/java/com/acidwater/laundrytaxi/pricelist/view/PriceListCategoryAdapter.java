package com.acidwater.laundrytaxi.pricelist.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.pricelist.PriceListMvp;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategory;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.SpacesItemDecoration;


import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class PriceListCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CatalogueCategory> list;
    private Context context;
    private onItemClickListener onItemClickListener;
    private PriceListMvp.Presenter presenter;

    private static final int TYPE_CATEGORY = 1;
    private static final int TYPE_FOOTER = 2;
    private LayoutInflater inflater;

    public PriceListCategoryAdapter(Context context, List<CatalogueCategory> list, PriceListMvp.Presenter presenter) {
        this.list = list;
        this.context = context;
        this.presenter = presenter;
        this.inflater = LayoutInflater.from(context);

    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView ic_expand_collapse;
        public RecyclerView recyclerView;


        public CategoryViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            title = (TextView) itemLayoutView.findViewById(R.id.categoryName);
            recyclerView = (RecyclerView) itemLayoutView.findViewById(R.id.recycler_view);
            ic_expand_collapse = (ImageView) itemLayoutView.findViewById(R.id.ic_expand_collapse);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        public TextView footer;

        public FooterViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            footer = (TextView) itemLayoutView.findViewById(R.id.footer);

        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {


        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {

            case TYPE_CATEGORY:
                viewHolder = new CategoryViewHolder(
                        inflater.inflate(R.layout.row_price_list_category, parent, false));

                break;

            case TYPE_FOOTER:

                viewHolder = new FooterViewHolder(
                        inflater.inflate(R.layout.row_price_list_category_footer, parent, false));
                break;

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {


        switch (viewHolder.getItemViewType()) {

            case TYPE_CATEGORY:

                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) viewHolder;
                String categoryName;
                if (GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals("") ||
                        GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
                    categoryName = list.get(position).getNameEn();
                } else {
                    categoryName = list.get(position).getNameAr();
                }
                categoryViewHolder.title.setText(categoryName);

                if (list.get(position).getCategoryItems() != null) {
                    CategoryItemAdapter categoryItemAdapter = new CategoryItemAdapter(context, list.get(position).getCategoryItems());
                    categoryViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    categoryViewHolder.recyclerView.setAdapter(categoryItemAdapter);
                }

                if (list.get(position).isExpanded()) {

                    categoryViewHolder.recyclerView.setVisibility(View.VISIBLE);
                    categoryViewHolder.ic_expand_collapse.setImageResource(R.drawable.ic_pricelist_collapse);

                } else {
                    categoryViewHolder.recyclerView.setVisibility(View.GONE);
                    categoryViewHolder.ic_expand_collapse.setImageResource(R.drawable.ic_pricelist_expand);
                }

                categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (list.get(position).isExpanded()) {
                            list.get(position).setExpanded(false);
                            notifyDataSetChanged();
                        } else {
                            list.get(position).setExpanded(true);
                            notifyDataSetChanged();
                        }


                    }
                });
                break;


            case TYPE_FOOTER:

                FooterViewHolder footerViewHolder = (FooterViewHolder) viewHolder;

                break;

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position == list.size()) {
            return TYPE_FOOTER;
        }

        return TYPE_CATEGORY;
    }

    public interface onItemClickListener {
        void onClick(CatalogueCategory place, int position);
    }

    public void setOnItemClickListener(PriceListCategoryAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}