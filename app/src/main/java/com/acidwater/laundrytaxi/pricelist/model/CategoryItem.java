package com.acidwater.laundrytaxi.pricelist.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 8/1/2017.
 */
public class CategoryItem implements Parcelable {


    @SerializedName("ArticleId")
    private String itemID;

    @SerializedName("CategoryId")
    private String categoryID;

    @SerializedName("NameEn")
    private String nameEn;

    @SerializedName("NameAr")
    private String nameAr;


    @SerializedName("Order")
    private String order;

    @SerializedName("ImageUrl")
    private String image;

    @SerializedName("Price")
    private String price;

    private int orderQuantity;

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }


    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public CategoryItem() {
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.itemID);
        dest.writeString(this.categoryID);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeString(this.order);
        dest.writeString(this.image);
        dest.writeString(this.price);
        dest.writeInt(this.orderQuantity);
    }

    protected CategoryItem(Parcel in) {
        this.itemID = in.readString();
        this.categoryID = in.readString();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.order = in.readString();
        this.image = in.readString();
        this.price = in.readString();
        this.orderQuantity = in.readInt();
    }

    public static final Creator<CategoryItem> CREATOR = new Creator<CategoryItem>() {
        @Override
        public CategoryItem createFromParcel(Parcel source) {
            return new CategoryItem(source);
        }

        @Override
        public CategoryItem[] newArray(int size) {
            return new CategoryItem[size];
        }
    };
}
