package com.acidwater.laundrytaxi.pricelist.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.pricelist.PriceListMvp;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.acidwater.laundrytaxi.pricelist.model.PriceListInteractor;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class PriceListPresenter extends MvpBasePresenter<PriceListMvp.View> implements PriceListMvp.Presenter {


    private Context context;
    private CompositeDisposable compositeDisposable;
    private PriceListInteractor priceListInteractor;


    public PriceListPresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        priceListInteractor = new PriceListInteractor(context);
    }


    @Override
    public void getCatalog() {
        compositeDisposable.add(priceListInteractor.getCatalog().subscribeWith(new ServiceObserver<Catalog>() {
            @Override
            public void onStart() {

                if (isViewAttached()) {
                    getView().showLoading(false);
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(Catalog catalog) {
                if (isViewAttached()) {
                    getView().setData(catalog);
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }
}
