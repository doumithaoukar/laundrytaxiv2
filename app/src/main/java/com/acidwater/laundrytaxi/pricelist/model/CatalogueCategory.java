package com.acidwater.laundrytaxi.pricelist.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/30/2017.
 */
public class CatalogueCategory implements Parcelable {


    @SerializedName("CategoryId")
    private String categoryID;
    @SerializedName("Order")
    private String order;

    @SerializedName("NameEn")
    private String nameEn;

    @SerializedName("NameAr")
    private String nameAr;


    boolean isExpanded = false;

    @SerializedName("Articles")
    private ArrayList<CategoryItem> categoryItems;


    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }



    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }


    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public CatalogueCategory() {
    }

    public ArrayList<CategoryItem> getCategoryItems() {
        return categoryItems;
    }

    public void setCategoryItems(ArrayList<CategoryItem> categoryItems) {
        this.categoryItems = categoryItems;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.categoryID);
        dest.writeString(this.order);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.categoryItems);
    }

    protected CatalogueCategory(Parcel in) {
        this.categoryID = in.readString();
        this.order = in.readString();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.isExpanded = in.readByte() != 0;
        this.categoryItems = in.createTypedArrayList(CategoryItem.CREATOR);
    }

    public static final Creator<CatalogueCategory> CREATOR = new Creator<CatalogueCategory>() {
        @Override
        public CatalogueCategory createFromParcel(Parcel source) {
            return new CatalogueCategory(source);
        }

        @Override
        public CatalogueCategory[] newArray(int size) {
            return new CatalogueCategory[size];
        }
    };
}
