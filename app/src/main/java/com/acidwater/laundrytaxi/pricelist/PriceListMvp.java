package com.acidwater.laundrytaxi.pricelist;

import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategory;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

import io.reactivex.Observable;


/**
 * Created by Doumith on 7/30/2017.
 */
public interface PriceListMvp {

    interface Interactor {

        Observable<Catalog> getCatalog();

    }

    interface Presenter extends MvpPresenter<View> {

        void getCatalog();
    }


    interface View extends MvpLceView<Catalog> {

        void setError(String message);

        void showLoadingView();

        void HideLoadingView();

        void showErrorMessage(String message);

    }
}
