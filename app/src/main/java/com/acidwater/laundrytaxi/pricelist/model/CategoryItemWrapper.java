package com.acidwater.laundrytaxi.pricelist.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 8/1/2017.
 */
public class CategoryItemWrapper implements Parcelable {

    private int status;
    private String message;

    @SerializedName("Articles")
    private ArrayList<CategoryItem> categoryItems;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoryItem> getCategoryItems() {
        return categoryItems;
    }

    public void setCategoryItems(ArrayList<CategoryItem> categoryItems) {
        this.categoryItems = categoryItems;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.status);
        dest.writeString(this.message);
        dest.writeTypedList(this.categoryItems);
    }

    public CategoryItemWrapper() {
    }

    protected CategoryItemWrapper(Parcel in) {
        this.status = in.readInt();
        this.message = in.readString();
        this.categoryItems = in.createTypedArrayList(CategoryItem.CREATOR);
    }

    public static final Parcelable.Creator<CategoryItemWrapper> CREATOR = new Parcelable.Creator<CategoryItemWrapper>() {
        @Override
        public CategoryItemWrapper createFromParcel(Parcel source) {
            return new CategoryItemWrapper(source);
        }

        @Override
        public CategoryItemWrapper[] newArray(int size) {
            return new CategoryItemWrapper[size];
        }
    };
}
