package com.acidwater.laundrytaxi.pricelist.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/30/2017.
 */
public class CatalogueCategoriesWrapper implements Parcelable {


    private int status;
    private String message;

    @SerializedName("Categories")
    private ArrayList<CatalogueCategory> categories;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CatalogueCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CatalogueCategory> categories) {
        this.categories = categories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.status);
        dest.writeString(this.message);
        dest.writeTypedList(this.categories);
    }

    public CatalogueCategoriesWrapper() {
    }

    protected CatalogueCategoriesWrapper(Parcel in) {
        this.status = in.readInt();
        this.message = in.readString();
        this.categories = in.createTypedArrayList(CatalogueCategory.CREATOR);
    }

    public static final Parcelable.Creator<CatalogueCategoriesWrapper> CREATOR = new Parcelable.Creator<CatalogueCategoriesWrapper>() {
        @Override
        public CatalogueCategoriesWrapper createFromParcel(Parcel source) {
            return new CatalogueCategoriesWrapper(source);
        }

        @Override
        public CatalogueCategoriesWrapper[] newArray(int size) {
            return new CatalogueCategoriesWrapper[size];
        }
    };
}
