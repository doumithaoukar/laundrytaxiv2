package com.acidwater.laundrytaxi.pricelist.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Doumith on 7/8/2017.
 */
public class CategoryItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<CategoryItem> list;
    private Context context;

    private final static int TYPE_HEADER = 0;
    private final static int TYPE_ITEM = 1;
    private AppConfig appConfig;
    private LayoutInflater inflater;

    public CategoryItemAdapter(Context context, List<CategoryItem> list) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);
        appConfig = GeneralUtils.getAppConfig(context);
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView items;
        public TextView currency;


        public HeaderViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            items = (TextView) itemLayoutView.findViewById(R.id.items);
            currency = (TextView) itemLayoutView.findViewById(R.id.currency);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView price;
        public ImageView image;


        public ItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            price = (TextView) itemLayoutView.findViewById(R.id.price);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            image = (ImageView) itemLayoutView.findViewById(R.id.image);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case TYPE_HEADER:
                viewHolder = new HeaderViewHolder(
                        inflater.inflate(R.layout.row_category_item_header, parent, false));

                break;

            case TYPE_ITEM:

                viewHolder = new ItemViewHolder(
                        inflater.inflate(R.layout.row_category_item, parent, false));
                break;
        }

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int pos) {


        switch (holder.getItemViewType()) {

            case TYPE_HEADER:
                HeaderViewHolder categoryViewHolder = (HeaderViewHolder) holder;
                categoryViewHolder.currency.setText(appConfig.getCurrency());
                break;

            case TYPE_ITEM:
                int position = pos - 1;
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                String itemName;
                if (GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals("") ||
                        GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
                    itemName = list.get(position).getNameEn();
                } else {
                    itemName = list.get(position).getNameAr();
                }
                itemViewHolder.name.setText(itemName);
                itemViewHolder.price.setText(list.get(position).getPrice());
                if (!TextUtils.isEmpty(list.get(position).getImage())) {
                    Picasso.with(context).load(list.get(position).getImage()).into(itemViewHolder.image);
                }
                break;
        }


    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;

        return TYPE_ITEM;
    }
}
