package com.acidwater.laundrytaxi.pricelist.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.orderdetails.presenter.OrderDetailsPresenter;
import com.acidwater.laundrytaxi.pricelist.PriceListMvp;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategory;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.pricelist.presenter.PriceListPresenter;
import com.acidwater.laundrytaxi.utils.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 7/8/2017.
 */
public class PriceListFragment extends MvpLceFragment<View, Catalog, PriceListMvp.View, PriceListMvp.Presenter> implements PriceListMvp.View {


    private MainActivityListener mListener;
    private RecyclerView recyclerView;
    private PriceListCategoryAdapter adapter;
    private RelativeLayout progressBar;

    public static PriceListFragment newInstance() {
        PriceListFragment priceListFragment = new PriceListFragment();
        return priceListFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_price_list, container, false);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.addItemDecoration(new SpacesItemDecoration(10));
        recyclerView.setLayoutManager(layoutManager);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }


    @Override
    public PriceListMvp.Presenter createPresenter() {
        return new PriceListPresenter(getContext());
    }

    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void setData(Catalog data) {
        adapter = new PriceListCategoryAdapter(getContext(), data.getCategories(), presenter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getCatalog();
    }
}
