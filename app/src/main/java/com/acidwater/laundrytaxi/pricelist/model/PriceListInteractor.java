package com.acidwater.laundrytaxi.pricelist.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.pricelist.PriceListMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Doumith on 6/18/2017.
 */
public class PriceListInteractor implements PriceListMvp.Interactor {

    private PriceListApi priceListApi;
    private Context context;

    public PriceListInteractor(Context context) {
        this.context = context;
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        priceListApi = retrofit.create(PriceListApi.class);
    }

    @Override
    public Observable<Catalog> getCatalog() {
        String url = ConstantStrings.Base_URL + ConstantStrings.SERVICE_CATALOG + "?lang=" + GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE);
        Observable<Catalog> observable =
                priceListApi.getCatalogueCategories(url).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface PriceListApi {
        @Headers("Content-Type: application/json")
        @GET
        Observable<Catalog> getCatalogueCategories(@Url String url);

    }


}
