package com.acidwater.laundrytaxi.changepassword.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.IntroActivityListener;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.changepassword.ChangePasswordMvp;
import com.acidwater.laundrytaxi.changepassword.presenter.ChangePasswordPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class ChangePasswordFragment extends MvpFragment<ChangePasswordMvp.View, ChangePasswordMvp.Presenter> implements View.OnClickListener, ChangePasswordMvp.View {


    private Button btnContinue;

    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmNewPassword;

    private RelativeLayout progressBar;

    private MainActivityListener mListener;

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
        return changePasswordFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_change_password, container, false);


        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        oldPassword = (EditText) mView.findViewById(R.id.oldPassword);
        newPassword = (EditText) mView.findViewById(R.id.newPassword);
        confirmNewPassword = (EditText) mView.findViewById(R.id.confirmNewPassword);

        btnContinue.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validate(oldPassword.getText().toString(), newPassword.getText().toString(), confirmNewPassword.getText().toString());
                break;
        }
    }

    @Override
    public ChangePasswordMvp.Presenter createPresenter() {
        return new ChangePasswordPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void setResultSuccess(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        oldPassword.setText("");
        newPassword.setText("");
        confirmNewPassword.setText("");
        destroyThis();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

}