package com.acidwater.laundrytaxi.changepassword.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.changepassword.ChangePasswordMvp;
import com.acidwater.laundrytaxi.changepassword.model.ChangePasswordInteractor;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.registration.RegistrationMvp;
import com.acidwater.laundrytaxi.registration.model.RegistrationInteractor;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ChangePasswordPresenter extends MvpBasePresenter<ChangePasswordMvp.View> implements ChangePasswordMvp.Presenter {

    private Context context;
    private ChangePasswordInteractor changePasswordInteractor;
    private CompositeDisposable compositeDisposable;

    public ChangePasswordPresenter(Context context) {
        this.context = context;
        changePasswordInteractor = new ChangePasswordInteractor(context);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }


    @Override
    public void changePassword(String oldPassword, String newPassword, String confirmNewPassword) {

        compositeDisposable.add(changePasswordInteractor.changePassword(oldPassword, newPassword, confirmNewPassword).subscribeWith(new ServiceObserver<ServerResponse>() {

            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }


            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    getView().setResultSuccess(serverResponse.getMessage());
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void validate(String oldPassword, String newPassword, String confirmNewPassword) {
        if (TextUtils.isEmpty(oldPassword) || TextUtils.isEmpty(newPassword) || TextUtils.isEmpty(confirmNewPassword)
                ) {
            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_required_fields));
                return;
            }

        }

        changePassword(oldPassword, newPassword, confirmNewPassword);
    }
}
