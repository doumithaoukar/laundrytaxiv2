package com.acidwater.laundrytaxi.changepassword.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.changepassword.ChangePasswordMvp;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.registration.RegistrationMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ChangePasswordInteractor implements ChangePasswordMvp.Interactor {


    private ChangePasswordApi changePasswordApi;


    public ChangePasswordInteractor(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().cookieJar(cookieJar).addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        changePasswordApi = retrofit.create(ChangePasswordApi.class);
    }


    @Override
    public Observable<ServerResponse> changePassword(String oldPassword, String newPassword,String confirmNewPassword) {
        Observable<ServerResponse> observable = null;
        PasswordRequest mUser = new PasswordRequest(oldPassword, newPassword,confirmNewPassword);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (mUser.toJson()).toString());
        observable =
                changePasswordApi.changePassword(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface ChangePasswordApi {
        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_CHANGE_PASSWORD)
        Observable<ServerResponse> changePassword(@Body RequestBody body);
    }

    private class PasswordRequest {
        @SerializedName("oldPassword")
        private String oldPassword;

        @SerializedName("newPassword")
        private String newPassword;

        @SerializedName("confirmNewPassword")
        private String confirmNewPassword;

        public PasswordRequest(String oldPassword, String newPassword,String confirmNewPassword) {

            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
            this.confirmNewPassword = confirmNewPassword;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }

}
