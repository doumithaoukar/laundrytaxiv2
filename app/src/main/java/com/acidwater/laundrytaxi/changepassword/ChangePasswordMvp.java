package com.acidwater.laundrytaxi.changepassword;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/6/2017.
 */
public interface ChangePasswordMvp {

    interface Interactor {
        Observable<ServerResponse> changePassword(String oldPassword, String newPassword,String confirmNewPassword);
    }

    interface Presenter extends MvpPresenter<View> {

        void changePassword(String oldPassword,String newPassword,String confirmNewPassword);

        void validate(String oldPassword,String newPassword,String confirmNewPassword);

    }

    interface View extends MvpView {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccess(String message);

    }
}
