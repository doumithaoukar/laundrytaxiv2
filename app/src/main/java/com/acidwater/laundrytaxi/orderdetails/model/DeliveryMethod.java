package com.acidwater.laundrytaxi.orderdetails.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 9/21/2017.
 */
public class DeliveryMethod implements Parcelable {


    @SerializedName("Name")
    private String name;
    @SerializedName("Label")
    private String label;
    @SerializedName("Multiply")
    private int multiply;
    @SerializedName("Minimum")
    private int minimum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getMultiply() {
        return multiply;
    }

    public void setMultiply(int multiply) {
        this.multiply = multiply;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.label);
        dest.writeInt(this.multiply);
        dest.writeInt(this.minimum);
    }

    public DeliveryMethod() {
    }

    protected DeliveryMethod(Parcel in) {
        this.name = in.readString();
        this.label = in.readString();
        this.multiply = in.readInt();
        this.minimum = in.readInt();
    }

    public static final Parcelable.Creator<DeliveryMethod> CREATOR = new Parcelable.Creator<DeliveryMethod>() {
        @Override
        public DeliveryMethod createFromParcel(Parcel source) {
            return new DeliveryMethod(source);
        }

        @Override
        public DeliveryMethod[] newArray(int size) {
            return new DeliveryMethod[size];
        }
    };

    @Override
    public String toString() {
        return label;
    }
}
