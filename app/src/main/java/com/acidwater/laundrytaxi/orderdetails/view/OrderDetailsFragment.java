package com.acidwater.laundrytaxi.orderdetails.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.orderdetails.model.PickupDropOff;
import com.acidwater.laundrytaxi.orderdetails.model.PickupDropOffWrapper;
import com.acidwater.laundrytaxi.orderdetails.model.TimeRange;
import com.acidwater.laundrytaxi.orderdetails.presenter.OrderDetailsPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;


/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderDetailsFragment extends MvpLceFragment<View, PickupDropOffWrapper, OrderDetailsMvp.View, OrderDetailsMvp.Presenter> implements View.OnClickListener, OrderDetailsMvp.View, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {


    private MainActivityListener mListener;
    private Spinner SpinnerPaymentMethod;
    private TextView pickupDate;
    private TextView dropOffDate;
    private EditText promoCode;
    private EditText otherInstructions;
    private TextView minimumOrder;
    private double minOrderValue;
    private AppConfig appConfig;
    private String[] paymentMethodSpinnerArray;
    private Button btnContinue;
    private TextView charactersLeft;
    private Spinner spinnerDeliveryMethod;

    private MyOrder myOrder;

    private PickupDropOff mPickUpDropOff;

    private String deliveryMethod;
    private DeliveryMethod selectedDeliveryMethod;
    private String paymentMethod;
    private String promo;
    private String pickupTimeDate;
    private String dropOffTimeDate;
    private String otherConstruct;
    private long mSelectedPickUp;
    private long mSelectedDropOff;

    LinkedHashMap<Long, List<Long>> pickupRanges = new LinkedHashMap<>();
    LinkedHashMap<Long, List<Long>> dropOffRanges = new LinkedHashMap<>();
    LinkedHashMap<Long, List<Long>> dropOffRangesStandard = new LinkedHashMap<>();
    LinkedHashMap<Long, List<Long>> dropOffRangesExpress = new LinkedHashMap<>();

    private boolean isPickupDate;

    Calendar pickupCalendar;
    Calendar dropOffCalendar;

    private Timepoint[] pickupTimePoints;
    private Timepoint[] dropOffTimePoints;

    OrderHistory mOrderHistory;

    private boolean isPickupEditable = true;
    private boolean isDropOffEditable = true;

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            pickupTimeDate = mOrderHistory.getPickupDateTime();
            mSelectedPickUp = mOrderHistory.getPickupTimeStamp();
            dropOffTimeDate = mOrderHistory.getDropOffDateTime();
            mSelectedDropOff = mOrderHistory.getDropOffTimeStamp();
            pickupDate.setText(pickupTimeDate);
            dropOffDate.setText(dropOffTimeDate);

            isPickupEditable = GeneralUtils.isPickupDropOffTimeEditable(mOrderHistory.getPickupTimeStamp());
            if (isPickupEditable) {
                pickupDate.setClickable(true);
                spinnerDeliveryMethod.setEnabled(true);
                SpinnerPaymentMethod.setEnabled(true);
                promoCode.setEnabled(true);
                otherInstructions.setEnabled(true);
            } else {
                pickupDate.setClickable(false);
                spinnerDeliveryMethod.setEnabled(false);
                SpinnerPaymentMethod.setEnabled(false);
                promoCode.setEnabled(false);
                otherInstructions.setEnabled(false);
            }
            isDropOffEditable = GeneralUtils.isPickupDropOffTimeEditable(mOrderHistory.getDropOffTimeStamp());
            if (isDropOffEditable) {
                dropOffDate.setClickable(true);

            } else {
                dropOffDate.setClickable(false);
            }

            if (mSelectedPickUp != 0) {
                filterDropOffDate();
            }

        }
    };


    public static OrderDetailsFragment newInstance(OrderHistory orderHistory) {
        OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
        if (orderHistory != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("orderHistory", orderHistory);
            orderDetailsFragment.setArguments(bundle);
        }
        return orderDetailsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRunnable != null && mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_order_details, container, false);

        spinnerDeliveryMethod = (Spinner) mView.findViewById(R.id.spinner_delivery_method);
        SpinnerPaymentMethod = (Spinner) mView.findViewById(R.id.spinner_payment_mode);
        promoCode = (EditText) mView.findViewById(R.id.promo_code);
        pickupDate = (TextView) mView.findViewById(R.id.pickup_date);
        dropOffDate = (TextView) mView.findViewById(R.id.drop_off_date);
        promoCode = (EditText) mView.findViewById(R.id.promo_code);
        otherInstructions = (EditText) mView.findViewById(R.id.other_constructions);
        minimumOrder = (TextView) mView.findViewById(R.id.minimum_order);
        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        charactersLeft = (TextView) mView.findViewById(R.id.characters_left);
        charactersLeft.setText(ConstantStrings.NB_CHARACTERS_OTHER_CONSTRUCTIONS + " " + getString(R.string.Characters_left));


        myOrder = GeneralUtils.getMyOrder(getContext());

        if (savedInstanceState != null) {
            deliveryMethod = savedInstanceState.getString("deliveryMethod");
            selectedDeliveryMethod = savedInstanceState.getParcelable("selectedDeliveryMethod");
            paymentMethod = savedInstanceState.getString("paymentMethod");
            promo = savedInstanceState.getString("promo");
            pickupTimeDate = savedInstanceState.getString("pickupTimeDate");
            dropOffTimeDate = savedInstanceState.getString("dropOffTimeDate");
            otherConstruct = savedInstanceState.getString("otherConstruct");
            pickupCalendar = (Calendar) savedInstanceState.getSerializable("pickupCalendar");
            mOrderHistory = savedInstanceState.getParcelable("orderHistory");
            mSelectedPickUp = savedInstanceState.getLong("mSelectedPickUp");
            mSelectedDropOff = savedInstanceState.getLong("mSelectedDropOff");
        } else {
            if (getArguments() != null) {
                mOrderHistory = getArguments().getParcelable("orderHistory");
            }
        }

        otherInstructions.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                charactersLeft.setText(ConstantStrings.NB_CHARACTERS_OTHER_CONSTRUCTIONS - s.length()
                        + " " + getString(R.string.Characters_left));
            }
        });

        pickupDate.setOnClickListener(this);
        dropOffDate.setOnClickListener(this);
        btnContinue.setOnClickListener(this);

        appConfig = GeneralUtils.getAppConfig(getContext());


        return mView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("deliveryMethod", deliveryMethod);
        outState.putParcelable("selectedDeliveryMethod", selectedDeliveryMethod);
        outState.putString("paymentMethod", paymentMethod);
        outState.putString("promo", promo);
        outState.putString("pickupTimeDate", pickupTimeDate);
        outState.putString("dropOffTimeDate", dropOffTimeDate);
        outState.putString("otherConstruct", otherConstruct);
        outState.putSerializable("pickupCalendar", pickupCalendar);
        outState.putParcelable("orderHistory", mOrderHistory);
        outState.putLong("mSelectedPickUp", mSelectedPickUp);
        outState.putLong("mSelectedDropOff", mSelectedDropOff);

    }

    private void initializeDeliveryMethods() {

        ArrayAdapter<DeliveryMethod> adapter = new ArrayAdapter<DeliveryMethod>(getContext(), R.layout.row_spinner_delivery_method, mPickUpDropOff.getDeliveryMethods());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDeliveryMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clearPickupDate();
                clearDropOffDate();
                dropOffRanges.clear();
                deliveryMethod = mPickUpDropOff.getDeliveryMethods().get(position).getName();
                selectedDeliveryMethod = mPickUpDropOff.getDeliveryMethods().get(position);
                minOrderValue = mPickUpDropOff.getDeliveryMethods().get(position).getMinimum();
                if (mPickUpDropOff.getDeliveryMethods().get(position).getMultiply() > 1) {
                    minimumOrder.setText(getString(R.string.minimum_order) + " " + minOrderValue + " " + appConfig.getCurrency()
                            + "\n" + getString(R.string.all_prices) + " " + mPickUpDropOff.getDeliveryMethods().get(position).getMultiply());
                } else {
                    minimumOrder.setText(getString(R.string.minimum_order) + " " + minOrderValue + " " + appConfig.getCurrency());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                minimumOrder.setText(appConfig.getCurrency());
            }
        });
        spinnerDeliveryMethod.setAdapter(adapter);


    }


    private void initializePaymentMethod() {
        paymentMethodSpinnerArray = appConfig.getPaymentMethods().toArray(new String[appConfig.getPaymentMethods().size()]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.row_spinner_delivery_method, paymentMethodSpinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerPaymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                paymentMethod = paymentMethodSpinnerArray[position];
                if (dropOffRanges != null && mSelectedPickUp != 0) {
                    dropOffRanges.clear();
                    dropOffDate.setText(getString(R.string.select_drop_off_date));
                    filterDropOffDate();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        SpinnerPaymentMethod.setAdapter(adapter);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public OrderDetailsMvp.Presenter createPresenter() {
        return new OrderDetailsPresenter(getContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.pickup_date:
                isPickupDate = true;
                setPickupDate();
                break;

            case R.id.drop_off_date:
                isPickupDate = false;
                setDropOffDate();
                break;

            case R.id.btn_continue:
                promo = promoCode.getText().toString();
                pickupTimeDate = pickupDate.getText().toString();
                dropOffTimeDate = dropOffDate.getText().toString();
                otherConstruct = otherInstructions.getText().toString();
                presenter.validateOrder(deliveryMethod, paymentMethod, promo, pickupTimeDate, dropOffTimeDate, otherConstruct);
                break;
        }
    }

    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSuccess() {
        if (myOrder != null) {
            myOrder.setDeliveryMethod(deliveryMethod);
            myOrder.setPaymentMethod(paymentMethod);
            myOrder.setPromoCode(promo);
            myOrder.setOtherInstructions(otherConstruct);
            myOrder.setPickUpTimeStamp(mSelectedPickUp);
            myOrder.setDropOffTimeStamp(mSelectedDropOff);
            GeneralUtils.saveMyOrder(getContext(), myOrder);
        }
        mListener.navigateToOrderItems(pickupTimePoints, dropOffTimePoints, selectedDeliveryMethod, mOrderHistory, minOrderValue);
    }

    @Override
    public void setData(PickupDropOffWrapper data) {
        mPickUpDropOff = data.getPickupDropOff();
        initializeDeliveryMethods();
        initializePaymentMethod();

        PickupDropOff pickupDropOff = data.getPickupDropOff();

        for (TimeRange timeRange : pickupDropOff.getRanges()) {

            dropOffRangesStandard.put(timeRange.getStart(), timeRange.getDropOffRanges().getStandard());
            dropOffRangesExpress.put(timeRange.getStart(), timeRange.getDropOffRanges().getExpress());

            long timeMidnight = GeneralUtils.getMidnight(timeRange.getStart());

            if (pickupRanges.containsKey(timeMidnight)) {
                pickupRanges.get(timeMidnight).add(timeRange.getStart());

            } else {
                ArrayList<Long> myRanges = new ArrayList<>();
                myRanges.add(timeRange.getStart());
                pickupRanges.put(timeMidnight, myRanges);
            }
        }

        if (mOrderHistory != null) {
            for (int i = 0; i < mPickUpDropOff.getDeliveryMethods().size(); i++) {
                DeliveryMethod deliveryMethod = mPickUpDropOff.getDeliveryMethods().get(i);
                if (deliveryMethod.getName().equalsIgnoreCase(mOrderHistory.getDeliveryMethod())) {
                    spinnerDeliveryMethod.setSelection(i);
                }
            }

            for (int i = 0; i < appConfig.getPaymentMethods().size(); i++) {
                String paymentMethod = appConfig.getPaymentMethods().get(i);
                if (paymentMethod.equalsIgnoreCase(mOrderHistory.getPaymentMethod())) {
                    SpinnerPaymentMethod.setSelection(i);
                }
            }


            mHandler.postDelayed(mRunnable, 300);
            promoCode.setText(mOrderHistory.getPromoCode());
            otherInstructions.setText(mOrderHistory.getInstruction());


        }

    }


    @Override
    public void loadData(boolean pullToRefresh) {
        if (mOrderHistory != null) {
            presenter.getPickupDropOff(GeneralUtils.getMidnight(mOrderHistory.getPickupTimeStamp()));
        } else {
            presenter.getPickupDropOff(0);
        }

    }

    private void setPickupDate() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                OrderDetailsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)

        );
        dpd.show(getActivity().getFragmentManager(), "show");

        if (pickupRanges != null) {
            List<Long> validDates = new ArrayList<Long>(pickupRanges.keySet());
            Calendar[] days = new Calendar[validDates.size()];

            for (int i = 0; i < validDates.size(); i++) {

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(validDates.get(i) * 1000);
                days[i] = c;

            }
            dpd.setSelectableDays(days);
        }


    }

    private void filterDropOffDate() {

        List<Long> validDates;
        if (((DeliveryMethod) spinnerDeliveryMethod.getSelectedItem()).getName().equals("Standard")) {
            validDates = dropOffRangesStandard.get(mSelectedPickUp);

        } else {
            validDates = dropOffRangesExpress.get(mSelectedPickUp);

        }
        for (Long timeRange : validDates) {
            long timeMidnight = GeneralUtils.getMidnight(timeRange + mSelectedPickUp);

            if (dropOffRanges.containsKey(timeMidnight)) {
                dropOffRanges.get(timeMidnight).add(timeRange + mSelectedPickUp);

            } else {
                ArrayList<Long> myRanges = new ArrayList<>();
                myRanges.add(timeRange + mSelectedPickUp);
                dropOffRanges.put(timeMidnight, myRanges);
                String s = "";
            }

        }
    }

    private void setDropOffDate() {
        if (mSelectedPickUp == 0) {
            Toast.makeText(getContext(), getString(R.string.Please_enter_pickup_date), Toast.LENGTH_SHORT).show();
            return;
        }

        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                OrderDetailsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)

        );
        dpd.show(getActivity().getFragmentManager(), "show");

        if (dropOffRanges != null) {
            List<Long> validDates = new ArrayList<Long>(dropOffRanges.keySet());
            Calendar[] days = new Calendar[validDates.size()];

            for (int i = 0; i < validDates.size(); i++) {

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(validDates.get(i) * 1000);
                days[i] = c;

            }
            dpd.setSelectableDays(days);
        }
    }

    private void setTimePicker(List<Long> validTimes) {
        Timepoint[] timePoints = new Timepoint[validTimes.size()];

        for (int i = 0; i < validTimes.size(); i++) {
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(validTimes.get(i) * 1000);
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minutes = c.get(Calendar.MINUTE);
            int seconds = c.get(Calendar.SECOND);
            timePoints[i] = new Timepoint(hour, minutes, seconds);
        }

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(
                OrderDetailsFragment.this,
                timePoints[0].getHour(),
                timePoints[0].getMinute(),
                timePoints[0].getSecond(), false
        );
        timePickerDialog.setSelectableTimes(timePoints);
        if (isPickupDate) {
            pickupTimePoints = timePoints;
        } else {
            dropOffTimePoints = timePoints;
        }
        timePickerDialog.show(getActivity().getFragmentManager(), "timedialog");

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        if (isPickupDate) {
            pickupCalendar = Calendar.getInstance();
            pickupCalendar.set(Calendar.YEAR, year);
            pickupCalendar.set(Calendar.MONTH, monthOfYear);
            pickupCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            pickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
            pickupCalendar.set(Calendar.MINUTE, 0);
            pickupCalendar.set(Calendar.SECOND, 0);
            pickupCalendar.set(Calendar.MILLISECOND, 0);
            mSelectedPickUp = pickupCalendar.getTimeInMillis() / 1000;
            setTimePicker(pickupRanges.get(mSelectedPickUp));

        } else {
            dropOffCalendar = Calendar.getInstance();
            dropOffCalendar.set(Calendar.YEAR, year);
            dropOffCalendar.set(Calendar.MONTH, monthOfYear);
            dropOffCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            dropOffCalendar.set(Calendar.HOUR_OF_DAY, 0);
            dropOffCalendar.set(Calendar.MINUTE, 0);
            dropOffCalendar.set(Calendar.SECOND, 0);
            dropOffCalendar.set(Calendar.MILLISECOND, 0);
            mSelectedDropOff = dropOffCalendar.getTimeInMillis() / 1000;
            setTimePicker(dropOffRanges.get(mSelectedDropOff));
        }

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        if (isPickupDate) {
            pickupCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            pickupCalendar.set(Calendar.MINUTE, minute);
            pickupCalendar.set(Calendar.SECOND, second);
            String nameOfTheDay = pickupCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
            String date = String.format("%02d", pickupCalendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", pickupCalendar.get(Calendar.MONTH) + 1) + "-" + pickupCalendar.get(Calendar.YEAR);
            date = nameOfTheDay + " " + date;
            String hour = String.format("%02d", GeneralUtils.getHourAmPm(hourOfDay));
            String min = String.format("%02d", minute);
            String time = hour + ":" + min;

            // add time range;
            Calendar addRange = Calendar.getInstance();
            addRange.set(Calendar.YEAR, pickupCalendar.get(Calendar.YEAR));
            addRange.set(Calendar.MONTH, pickupCalendar.get(Calendar.MONTH));
            addRange.set(Calendar.DAY_OF_MONTH, pickupCalendar.get(Calendar.DAY_OF_MONTH));
            addRange.set(Calendar.HOUR_OF_DAY, hourOfDay);
            addRange.set(Calendar.MINUTE, minute);
            addRange.set(Calendar.SECOND, second);
            addRange.add(Calendar.SECOND, (int) mPickUpDropOff.getRangeDuration());
            String hourWithRange = String.format("%02d", GeneralUtils.getHourAmPm(addRange.get(Calendar.HOUR_OF_DAY)));
            String minWithRange = String.format("%02d", addRange.get(Calendar.MINUTE));
            String timeWithRange = hourWithRange + ":" + minWithRange;

            pickupDate.setText(time + " " + GeneralUtils.isAmOrPm(hourOfDay) + " - " + timeWithRange + " " + GeneralUtils.isAmOrPm(addRange.get(Calendar.HOUR_OF_DAY)) + "  " + date);
            mSelectedPickUp = pickupCalendar.getTimeInMillis() / 1000;
            clearDropOffDate();
            dropOffRanges.clear();
            filterDropOffDate();
            setDropOffDateProgrammatically();

        } else {
            dropOffCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dropOffCalendar.set(Calendar.MINUTE, minute);
            dropOffCalendar.set(Calendar.SECOND, second);
            String nameOfTheDay = dropOffCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
            String date = String.format("%02d", dropOffCalendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", dropOffCalendar.get(Calendar.MONTH) + 1) + "-" + dropOffCalendar.get(Calendar.YEAR);
            date = nameOfTheDay + " " + date;
            String hour = String.format("%02d", GeneralUtils.getHourAmPm(hourOfDay));
            String min = String.format("%02d", minute);
            String time = hour + ":" + min;

            // add time range;
            Calendar addRange = Calendar.getInstance();
            addRange.set(Calendar.YEAR, dropOffCalendar.get(Calendar.YEAR));
            addRange.set(Calendar.MONTH, dropOffCalendar.get(Calendar.MONTH));
            addRange.set(Calendar.DAY_OF_MONTH, dropOffCalendar.get(Calendar.DAY_OF_MONTH));
            addRange.set(Calendar.HOUR_OF_DAY, hourOfDay);
            addRange.set(Calendar.MINUTE, minute);
            addRange.set(Calendar.SECOND, second);
            addRange.add(Calendar.SECOND, (int) mPickUpDropOff.getRangeDuration());

            String hourWithRange = String.format("%02d", GeneralUtils.getHourAmPm(addRange.get(Calendar.HOUR_OF_DAY)));
            String minWithRange = String.format("%02d", addRange.get(Calendar.MINUTE));
            String timeWithRange = hourWithRange + ":" + minWithRange;

            dropOffDate.setText(time + " " + GeneralUtils.isAmOrPm(hourOfDay) + " - " + timeWithRange + " " + GeneralUtils.isAmOrPm(addRange.get(Calendar.HOUR_OF_DAY)) + " " + date);
            mSelectedDropOff = dropOffCalendar.getTimeInMillis() / 1000;
        }

    }

    private void setDropOffDateProgrammatically() {

        if (dropOffRanges != null) {
            List<Long> validDates = new ArrayList<Long>(dropOffRanges.keySet());
            if (validDates.size() > 0) {
                Collections.sort(validDates);
                Calendar mCalendar = Calendar.getInstance();
                mCalendar.setTimeInMillis(validDates.get(0) * 1000);

                dropOffCalendar = Calendar.getInstance();
                dropOffCalendar.set(Calendar.YEAR, mCalendar.get(Calendar.YEAR));
                dropOffCalendar.set(Calendar.MONTH, mCalendar.get(Calendar.MONTH));
                dropOffCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.get(Calendar.DAY_OF_MONTH));
                dropOffCalendar.set(Calendar.HOUR_OF_DAY, 0);
                dropOffCalendar.set(Calendar.MINUTE, 0);
                dropOffCalendar.set(Calendar.SECOND, 0);
                dropOffCalendar.set(Calendar.MILLISECOND, 0);
                mSelectedDropOff = dropOffCalendar.getTimeInMillis() / 1000;

                Calendar mDropOffCalendar = Calendar.getInstance();
                mDropOffCalendar.setTimeInMillis(dropOffRanges.get(mSelectedDropOff).get(0) * 1000);


                String nameOfTheDay = mDropOffCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
                String date = String.format("%02d", mDropOffCalendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", mDropOffCalendar.get(Calendar.MONTH) + 1) + "-" + mDropOffCalendar.get(Calendar.YEAR);
                date = nameOfTheDay + " " + date;
                String hour = String.format("%02d", GeneralUtils.getHourAmPm(mDropOffCalendar.get(Calendar.HOUR_OF_DAY)));
                String min = String.format("%02d", mDropOffCalendar.get(Calendar.MINUTE));
                String time = hour + ":" + min;

                // add time range;
                Calendar addRange = Calendar.getInstance();
                addRange.set(Calendar.YEAR, mDropOffCalendar.get(Calendar.YEAR));
                addRange.set(Calendar.MONTH, mDropOffCalendar.get(Calendar.MONTH));
                addRange.set(Calendar.DAY_OF_MONTH, mDropOffCalendar.get(Calendar.DAY_OF_MONTH));
                addRange.set(Calendar.HOUR_OF_DAY, mDropOffCalendar.get(Calendar.HOUR_OF_DAY));
                addRange.set(Calendar.MINUTE, mDropOffCalendar.get(Calendar.MINUTE));
                addRange.set(Calendar.SECOND, mDropOffCalendar.get(Calendar.SECOND));
                addRange.add(Calendar.SECOND, (int) mPickUpDropOff.getRangeDuration());
                String hourWithRange = String.format("%02d", GeneralUtils.getHourAmPm(addRange.get(Calendar.HOUR_OF_DAY)));
                String minWithRange = String.format("%02d", addRange.get(Calendar.MINUTE));
                String timeWithRange = hourWithRange + ":" + minWithRange;

                dropOffDate.setText(time + " " + GeneralUtils.isAmOrPm(mDropOffCalendar.get(Calendar.HOUR_OF_DAY)) + " - " + timeWithRange + " " + GeneralUtils.isAmOrPm(addRange.get(Calendar.HOUR_OF_DAY)) + "  " + date);
                mSelectedDropOff = mDropOffCalendar.getTimeInMillis() / 1000;
            }
        }


    }

    private void clearPickupDate() {
        mSelectedPickUp = 0;
        pickupTimePoints = null;
        pickupTimeDate = getString(R.string.select_pickup_date);
        pickupDate.setText(pickupTimeDate);
    }

    private void clearDropOffDate() {
        mSelectedDropOff = 0;
        dropOffTimePoints = null;
        dropOffTimeDate = getString(R.string.select_drop_off_date);
        dropOffDate.setText(dropOffTimeDate);
    }


}
