package com.acidwater.laundrytaxi.orderdetails.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 9/21/2017.
 */
public class PickupDropOff implements Parcelable {

    @SerializedName("DeliveryMethods")
    ArrayList<DeliveryMethod> deliveryMethods;

    @SerializedName("RangeDuration")
    private double rangeDuration;


    @SerializedName("Ranges")
    private ArrayList<TimeRange> ranges;

    public ArrayList<DeliveryMethod> getDeliveryMethods() {
        return deliveryMethods;
    }

    public void setDeliveryMethods(ArrayList<DeliveryMethod> deliveryMethods) {
        this.deliveryMethods = deliveryMethods;
    }

    public double getRangeDuration() {
        return rangeDuration;
    }

    public void setRangeDuration(double rangeDuration) {
        this.rangeDuration = rangeDuration;
    }

    public ArrayList<TimeRange> getRanges() {
        return ranges;
    }

    public void setRanges(ArrayList<TimeRange> ranges) {
        this.ranges = ranges;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.deliveryMethods);
        dest.writeDouble(this.rangeDuration);
        dest.writeTypedList(this.ranges);
    }

    public PickupDropOff() {
    }

    protected PickupDropOff(Parcel in) {
        this.deliveryMethods = in.createTypedArrayList(DeliveryMethod.CREATOR);
        this.rangeDuration = in.readDouble();
        this.ranges = in.createTypedArrayList(TimeRange.CREATOR);
    }

    public static final Parcelable.Creator<PickupDropOff> CREATOR = new Parcelable.Creator<PickupDropOff>() {
        @Override
        public PickupDropOff createFromParcel(Parcel source) {
            return new PickupDropOff(source);
        }

        @Override
        public PickupDropOff[] newArray(int size) {
            return new PickupDropOff[size];
        }
    };
}
