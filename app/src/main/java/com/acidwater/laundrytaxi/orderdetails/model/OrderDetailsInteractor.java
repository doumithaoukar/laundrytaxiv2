package com.acidwater.laundrytaxi.orderdetails.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrderDetailsInteractor implements OrderDetailsMvp.Interactor {


    private PickupDropOffAPI pickupDropOffAPI;

    public OrderDetailsInteractor(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        pickupDropOffAPI = retrofit.create(PickupDropOffAPI.class);
    }


    @Override
    public Observable<PickupDropOffWrapper> getPickUpDropOff(long pickUpTimeStamp) {

        String url = ConstantStrings.Base_URL + "" + ConstantStrings.SERVICE_PICKUP_DROP_OFF;
        if (pickUpTimeStamp != 0) {
            url += "/?time="+ pickUpTimeStamp;
        }

        Observable<PickupDropOffWrapper> observable =
                pickupDropOffAPI.getPickupDropOff(String.valueOf(url)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface PickupDropOffAPI {
        @Headers("Content-Type: application/json")
        @GET
        Observable<PickupDropOffWrapper> getPickupDropOff(@Url String url);

    }
}
