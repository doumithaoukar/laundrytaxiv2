package com.acidwater.laundrytaxi.orderdetails.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 9/21/2017.
 */
public class TimeRange implements Parcelable {


    @SerializedName("Start")
    private long start;
    @SerializedName("DropoffRanges")
    private DropOffRange dropOffRanges;

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public DropOffRange getDropOffRanges() {
        return dropOffRanges;
    }

    public void setDropOffRanges(DropOffRange dropOffRanges) {
        this.dropOffRanges = dropOffRanges;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.start);
        dest.writeParcelable(this.dropOffRanges, flags);
    }

    public TimeRange() {
    }

    protected TimeRange(Parcel in) {
        this.start = in.readLong();
        this.dropOffRanges = in.readParcelable(DropOffRange.class.getClassLoader());
    }

    public static final Parcelable.Creator<TimeRange> CREATOR = new Parcelable.Creator<TimeRange>() {
        @Override
        public TimeRange createFromParcel(Parcel source) {
            return new TimeRange(source);
        }

        @Override
        public TimeRange[] newArray(int size) {
            return new TimeRange[size];
        }
    };
}
