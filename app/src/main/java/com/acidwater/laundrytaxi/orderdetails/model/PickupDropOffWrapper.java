package com.acidwater.laundrytaxi.orderdetails.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 9/21/2017.
 */
public class PickupDropOffWrapper {

    private int status;
    private String message;
    @SerializedName("Data")
    private PickupDropOff pickupDropOff;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PickupDropOff getPickupDropOff() {
        return pickupDropOff;
    }

    public void setPickupDropOff(PickupDropOff pickupDropOff) {
        this.pickupDropOff = pickupDropOff;
    }
}
