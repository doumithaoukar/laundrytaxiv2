package com.acidwater.laundrytaxi.orderdetails;

import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.orderdetails.model.PickupDropOffWrapper;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by Doumith on 7/30/2017.
 */
public interface OrderDetailsMvp {


    interface Interactor {


        Observable<PickupDropOffWrapper> getPickUpDropOff(long pickUpTimeStamp);

    }


    interface Presenter extends MvpPresenter<View> {


        void getPickupDropOff(long timestamp);

        void validateOrder(String deliveryMethod, String paymentMethod, String promo, String pickupTimeDate, String dropOffTimeDate, String otherConstructions);
    }


    interface View extends MvpLceView<PickupDropOffWrapper> {

        void setError(String message);

        void setSuccess();

    }
}
