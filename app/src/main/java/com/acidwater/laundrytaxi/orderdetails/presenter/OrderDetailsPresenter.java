package com.acidwater.laundrytaxi.orderdetails.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.orderdetails.model.OrderDetailsInteractor;
import com.acidwater.laundrytaxi.orderdetails.model.PickupDropOffWrapper;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class OrderDetailsPresenter extends MvpBasePresenter<OrderDetailsMvp.View> implements OrderDetailsMvp.Presenter {


    private Context context;
    private OrderDetailsInteractor interactor;
    private CompositeDisposable compositeDisposable;

    public OrderDetailsPresenter(Context context) {
        this.context = context;
        this.interactor = new OrderDetailsInteractor(context);
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void getPickupDropOff(long pickUpTimeStamp) {
        compositeDisposable.add(interactor.getPickUpDropOff(pickUpTimeStamp).subscribeWith(new ServiceObserver<PickupDropOffWrapper>() {
            @Override
            public void onStart() {

                if (isViewAttached()) {
                    getView().showLoading(false);
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(PickupDropOffWrapper pickupDropOffWrapper) {
                if (isViewAttached()) {
                    getView().setData(pickupDropOffWrapper);
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void validateOrder(String deliveryMethod, String paymentMethod, String promo, String pickupTimeDate, String dropOffTimeDate, String otherConstructions) {

        if (TextUtils.isEmpty(deliveryMethod) ||
                TextUtils.isEmpty(paymentMethod)) {
            if (isViewAttached()) {
                getView().setError(context.getString(R.string.enter_required_fields));
            }

        } else {
            if (pickupTimeDate.equals(context.getString(R.string.select_pickup_date)) ||
                    dropOffTimeDate.equals(context.getString(R.string.select_drop_off_date))) {
                if (isViewAttached()) {
                    getView().setError(context.getString(R.string.enter_required_fields));
                }
            } else {

                //for now consider it success for later do so logic regarding the date

                if (isViewAttached()) {
                    getView().setSuccess();
                }
            }

        }

    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

}
