package com.acidwater.laundrytaxi.orderdetails.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 9/21/2017.
 */
public class DropOffRange implements Parcelable {

    @SerializedName("Express")
    @Expose
    private ArrayList<Long> express;

    @SerializedName("Standard")
    @Expose
    private ArrayList<Long> standard;

    public ArrayList<Long> getStandard() {
        return standard;
    }

    public void setStandard(ArrayList<Long> standard) {
        this.standard = standard;
    }

    public ArrayList<Long> getExpress() {
        return express;
    }

    public void setExpress(ArrayList<Long> express) {
        this.express = express;
    }

    public DropOffRange() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.standard);
        dest.writeList(this.express);
    }

    protected DropOffRange(Parcel in) {
        this.standard = new ArrayList<Long>();
        in.readList(this.standard, Long.class.getClassLoader());
        this.express = new ArrayList<Long>();
        in.readList(this.express, Long.class.getClassLoader());
    }

    public static final Creator<DropOffRange> CREATOR = new Creator<DropOffRange>() {
        @Override
        public DropOffRange createFromParcel(Parcel source) {
            return new DropOffRange(source);
        }

        @Override
        public DropOffRange[] newArray(int size) {
            return new DropOffRange[size];
        }
    };
}
