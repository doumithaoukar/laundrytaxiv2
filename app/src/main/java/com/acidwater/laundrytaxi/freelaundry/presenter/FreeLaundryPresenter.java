package com.acidwater.laundrytaxi.freelaundry.presenter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.freelaundry.FreeLaundryMvp;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class FreeLaundryPresenter extends MvpBasePresenter<FreeLaundryMvp.View> implements FreeLaundryMvp.Presenter {

    private Context context;

    public FreeLaundryPresenter(Context context) {
        this.context = context;
    }

    @Override
    public String getInvitationCode() {
        User user = GeneralUtils.getUserProfile(context);
        if (isViewAttached()) {
            getView().setInvitationCode(user.getInvitationCode());
        }
        return user.getInvitationCode();
    }

    @Override
    public void copyInvitationCodeTOClipboard() {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied Text", getInvitationCode());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, "Text Copied",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setUpToPrice() {
        AppConfig appConfig = GeneralUtils.getAppConfig(context);
        if (appConfig != null) {
            if (isViewAttached()) {
                if (GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
                    getView().setUpToPrice(appConfig.getFreeOrderUptoValue() + " " + appConfig.getCurrency());
                } else {
                    getView().setUpToPrice(appConfig.getCurrency() + " " + appConfig.getFreeOrderUptoValue());
                }
            }
        }
    }

    @Override
    public String getShareURL() {
        AppConfig appConfig = GeneralUtils.getAppConfig(context);
        return appConfig.getShareURL();
    }

    @Override
    public String getShareMessage() {
        AppConfig appConfig = GeneralUtils.getAppConfig(context);
        String message = "";

        message = context.getString(R.string.share_with_invitation_code) + " [" + getInvitationCode() + "] " + context.getString(R.string.share_with_invitation_code_2) +
                " ";
        if (appConfig.getShareURL() != null) {
            message += " " + appConfig.getShareURL();
        } else {
            message += " " + GeneralUtils.getDefaultAppURL(context);
        }

        return message;
    }
}
