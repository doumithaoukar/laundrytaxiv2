package com.acidwater.laundrytaxi.freelaundry;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Doumith on 8/13/2017.
 */
public interface FreeLaundryMvp {


    interface Presenter extends MvpPresenter<View> {

        String getInvitationCode();

        void copyInvitationCodeTOClipboard();

        void setUpToPrice();

        String getShareURL();

        String getShareMessage();
    }

    interface View extends MvpView {

        void setInvitationCode(String code);

        void setUpToPrice(String value);
    }
}
