package com.acidwater.laundrytaxi.freelaundry.view;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.about.presenter.AboutPresenter;
import com.acidwater.laundrytaxi.freelaundry.FreeLaundryMvp;
import com.acidwater.laundrytaxi.freelaundry.presenter.FreeLaundryPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class FreeLaundryFragment extends MvpFragment<FreeLaundryMvp.View, FreeLaundryMvp.Presenter> implements View.OnClickListener, FreeLaundryMvp.View {


    private MainActivityListener mListener;
    private TextView freeLaundryOne;
    private TextView freeLaundryTwo;
    private TextView freeLaundryThree;
    private TextView freeLaundryFour;
    private TextView freeLaundryFive;
    private TextView invitationCode;
    private TextView btnCopy;
    private TextView copy;
    private Button sendInvite;


    public static FreeLaundryFragment newInstance() {
        FreeLaundryFragment aboutFragment = new FreeLaundryFragment();
        return aboutFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getInvitationCode();
        presenter.setUpToPrice();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_free_laundry, container, false);

        freeLaundryOne = (TextView) mView.findViewById(R.id.free_laundry_one);
        freeLaundryTwo = (TextView) mView.findViewById(R.id.free_laundry_two);
        freeLaundryThree = (TextView) mView.findViewById(R.id.free_laundry_three);
        freeLaundryFour = (TextView) mView.findViewById(R.id.free_laundry_four);
        freeLaundryFive = (TextView) mView.findViewById(R.id.free_laundry_five);
        invitationCode = (TextView) mView.findViewById(R.id.invitation_code);
        sendInvite = (Button) mView.findViewById(R.id.btn_send_inviation);
        btnCopy = (TextView) mView.findViewById(R.id.copy);

        btnCopy.setOnClickListener(this);
        sendInvite.setOnClickListener(this);
        freeLaundryFour.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_inviation:
                openShareCode();
                break;

            case R.id.copy:
                presenter.copyInvitationCodeTOClipboard();
                break;

            case R.id.free_laundry_four:
                openHowInvitesWork();
                break;


        }
    }

    @Override
    public FreeLaundryMvp.Presenter createPresenter() {
        return new FreeLaundryPresenter(getContext());
    }

    private void openHowInvitesWork() {
        mListener.navigationToScreen(ConstantStrings.HOW_INVITES_WORK);
    }

    private void openShareCode() {


        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, presenter.getShareMessage());
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }


    @Override
    public void setInvitationCode(String code) {
        invitationCode.setText(code);
    }

    @Override
    public void setUpToPrice(String value) {
        if (GeneralUtils.getSharedPref(getContext(), ConstantStrings.LANGUAGE_AR).equals(ConstantStrings.LANGUAGE_EN)) {
            freeLaundryOne.setText(value + " " + getResources().getString(R.string.free_laundry_one) + " " + value);
        } else {
            freeLaundryOne.setText(getResources().getString(R.string.free_laundry_one) + " " + value);
        }
    }
}