package com.acidwater.laundrytaxi.menu;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Doumith on 7/8/2017.
 */
public class MenuMvp {


    public interface Interactor {
        List<Menu> getMenuList();
    }


    public interface Presenter extends MvpPresenter<View> {
        void getMenuList();
    }


    public interface View extends MvpView {

        void renderMenuList(List<Menu> menuList);
    }
}
