package com.acidwater.laundrytaxi.menu.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.menu.MenuMvp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 7/13/2017.
 */
public class MenuInteractor implements MenuMvp.Interactor {


    private Context context;

    public MenuInteractor(Context context) {
        this.context = context;
    }

    @Override
    public List<Menu> getMenuList() {

        ArrayList<Menu> list = new ArrayList<>();
        list.add(new Menu(ConstantStrings.MENU_HOME, R.drawable.ic_menu_home, context.getString(R.string.menu_home)));
        list.add(new Menu(ConstantStrings.MENU_PRICE_LIST, R.drawable.ic_menu_price_list, context.getString(R.string.menu_price_list)));
        list.add(new Menu(ConstantStrings.MENU_PROFILE, R.drawable.ic_menu_profile, context.getString(R.string.menu_profile)));
        list.add(new Menu(ConstantStrings.MENU_ORDERS, R.drawable.ic_menu_orders, context.getString(R.string.menu_orders)));
        list.add(new Menu(ConstantStrings.MENU_MANAGE_PAYMENT, R.drawable.ic_menu_manage_payment, context.getString(R.string.menu_manage_payment)));
        list.add(new Menu(ConstantStrings.MENU_NOTIFICATIONS, R.drawable.ic_menu_notifications, context.getString(R.string.menu_notifications)));
        list.add(new Menu(ConstantStrings.MENU_FREE_LAUNDRY, R.drawable.ic_menu_free_laundry, context.getString(R.string.menu_free_laundry)));
        list.add(new Menu(ConstantStrings.MENU_SETTINGS, R.drawable.ic_menu_settings, context.getString(R.string.menu_settings)));

        return list;
    }
}
