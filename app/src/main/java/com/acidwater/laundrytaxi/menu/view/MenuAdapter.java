package com.acidwater.laundrytaxi.menu.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Doumith on 7/8/2017.
 */
public class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_PROFILE = 0;
    private final int TYPE_MENU = 1;

    private List<Menu> list;
    private Context context;
    private OnItemClickedListener onItemClickListener;

    public MenuAdapter(Context context, List<Menu> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder_Profile extends RecyclerView.ViewHolder {

        public CircleImageView user_image;
        public TextView user_name;

        public ViewHolder_Profile(View itemLayoutView) {
            super(itemLayoutView);

            user_image = (CircleImageView) itemLayoutView.findViewById(R.id.user_image);
            user_name = (TextView) itemLayoutView.findViewById(R.id.user_name);
        }
    }

    public class ViewHolder_Menu extends RecyclerView.ViewHolder {

        public ImageView menu_image;
        public TextView menu_name;


        public ViewHolder_Menu(View itemLayoutView) {
            super(itemLayoutView);

            menu_image = (ImageView) itemLayoutView.findViewById(R.id.menu_image);
            menu_name = (TextView) itemLayoutView.findViewById(R.id.menu_name);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_PROFILE) {
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_menu_profile, parent, false);
            return new ViewHolder_Profile(itemLayoutView);


        } else if (viewType == TYPE_MENU) {
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_menu_list, parent, false);
            return new ViewHolder_Menu(itemLayoutView);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int pos) {

        if (holder instanceof ViewHolder_Menu) {

            final int position = pos - 1;
            ViewHolder_Menu viewHolder_menu = (ViewHolder_Menu) holder;

            viewHolder_menu.menu_name.setText(list.get(position).getName());
            viewHolder_menu.menu_image.setImageResource(list.get(position).getImage());

            viewHolder_menu.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClicked(list.get(position));
                    }
                }
            });

        } else if (holder instanceof ViewHolder_Profile) {
            User user = GeneralUtils.getUserProfile(context);
            ViewHolder_Profile viewHolder_profile = (ViewHolder_Profile) holder;
            if (user != null) {
                if (user.getFirstName() != null && user.getLastName() != null) {
                    viewHolder_profile.user_name.setText(user.getFirstName() + " " + user.getLastName());
                }
                if (!TextUtils.isEmpty(user.getImage())) {
                    Picasso.with(context).load(user.getImage()).into(viewHolder_profile.user_image);
                }
                viewHolder_profile.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onProfileClicked();
                        }
                    }
                });
            }

        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(Menu menu);

        void onProfileClicked();
    }

    public void setOnItemClickListener(OnItemClickedListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_PROFILE;

        return TYPE_MENU;
    }
}
