package com.acidwater.laundrytaxi.menu.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.menu.MenuMvp;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.menu.model.MenuInteractor;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Doumith on 7/13/2017.
 */
public class MenuPresenter extends MvpBasePresenter<MenuMvp.View> implements MenuMvp.Presenter {


    private MenuInteractor menuInteractor;
    private Context context;

    public MenuPresenter(Context context) {
        this.context = context;
        menuInteractor = new MenuInteractor(context);
    }

    @Override
    public void getMenuList() {

        List<Menu> list = menuInteractor.getMenuList();
        if (isViewAttached()) {
            getView().renderMenuList(list);
        }
    }
}
