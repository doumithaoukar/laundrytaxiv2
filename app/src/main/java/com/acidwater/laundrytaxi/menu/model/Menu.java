package com.acidwater.laundrytaxi.menu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Doumith on 7/8/2017.
 */
public class Menu implements Parcelable {


    private int id;
    private int image;
    private String name;

    public Menu() {
    }

    public Menu(int id, int image, String name) {
        this.id = id;
        this.image = image;
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.image);
        dest.writeString(this.name);
    }

    protected Menu(Parcel in) {
        this.id = in.readInt();
        this.image = in.readInt();
        this.name = in.readString();
    }

    public static final Creator<Menu> CREATOR = new Creator<Menu>() {
        @Override
        public Menu createFromParcel(Parcel source) {
            return new Menu(source);
        }

        @Override
        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };
}
