package com.acidwater.laundrytaxi.signup.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.acidwater.laundrytaxi.BaseFragment;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.IntroActivityListener;


/**
 * Created by Doumith on 7/6/2017.
 */
public class SignUpFragment extends BaseFragment implements View.OnClickListener {


    private Button btnSignUp;
    private LinearLayout btnSignIn;

    private IntroActivityListener mListener;

    public static SignUpFragment newInstance() {
        SignUpFragment signUpFragment = new SignUpFragment();
        return signUpFragment;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_signup, container, false);
        btnSignIn = (LinearLayout) mView.findViewById(R.id.btn_signIn);
        btnSignUp = (Button) mView.findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_up:
                mListener.navigateToSection(IntroActivity.SIGN_UP);
                break;

            case R.id.btn_signIn:
                mListener.navigateToSection(IntroActivity.LOGIN);
                break;
        }
    }
}
