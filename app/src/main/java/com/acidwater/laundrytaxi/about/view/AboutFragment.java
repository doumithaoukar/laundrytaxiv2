package com.acidwater.laundrytaxi.about.view;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.about.presenter.AboutPresenter;
import com.acidwater.laundrytaxi.changepassword.ChangePasswordMvp;
import com.acidwater.laundrytaxi.changepassword.presenter.ChangePasswordPresenter;
import com.acidwater.laundrytaxi.faq.FaqMvp;
import com.acidwater.laundrytaxi.faq.presenter.FaqPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class AboutFragment extends MvpFragment<AboutMvp.View, AboutMvp.Presenter> implements View.OnClickListener, AboutMvp.View {


    private MainActivityListener mListener;
    private WebView webView;


    public static AboutFragment newInstance() {
        AboutFragment aboutFragment = new AboutFragment();
        return aboutFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_about_us, container, false);

        webView = (WebView) mView.findViewById(R.id.webview);
        webView.loadUrl(ConstantStrings.Base_URL + "" + ConstantStrings.SERVICE_ABOUT+ "?lang="+ GeneralUtils.getSharedPref(getContext(),ConstantStrings.APP_LANGUAGE));
        webView.setBackgroundColor(Color.TRANSPARENT);
        return mView;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public AboutMvp.Presenter createPresenter() {
        return new AboutPresenter(getContext());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }
}