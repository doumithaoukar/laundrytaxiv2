package com.acidwater.laundrytaxi.about;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 8/13/2017.
 */
public interface AboutMvp {


    interface Presenter extends MvpPresenter<View> {



    }

    interface View extends MvpView {


    }
}
