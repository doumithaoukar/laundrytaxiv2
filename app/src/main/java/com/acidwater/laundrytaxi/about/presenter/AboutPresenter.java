package com.acidwater.laundrytaxi.about.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.changepassword.ChangePasswordMvp;
import com.acidwater.laundrytaxi.changepassword.model.ChangePasswordInteractor;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class AboutPresenter extends MvpBasePresenter<AboutMvp.View> implements AboutMvp.Presenter {

    private Context context;

    public AboutPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }

}
