package com.acidwater.laundrytaxi.myorders.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.acidwater.laundrytaxi.BaseFragment;
import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.adapters.BasePagerAdapter;
import com.acidwater.laundrytaxi.ordershistory.view.OrdersHistoryFragment;
import com.acidwater.laundrytaxi.utils.NonSwipeableViewPager;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrdersFragment extends BaseFragment implements View.OnClickListener {


    private Button btnInProgress;
    private Button btnHistory;

    private NonSwipeableViewPager pager;


    public static OrdersFragment newInstance() {
        OrdersFragment ordersFragment = new OrdersFragment();
        return ordersFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_orders, container, false);
        pager = (NonSwipeableViewPager) mView.findViewById(R.id.pager);
        btnInProgress = (Button) mView.findViewById(R.id.btn_in_progress);
        btnHistory = (Button) mView.findViewById(R.id.btn_history);
        btnHistory.setOnClickListener(this);
        btnInProgress.setOnClickListener(this);
        setAdapter();
        return mView;

    }

    private void setAdapter() {
        BasePagerAdapter pagerAdapter = new BasePagerAdapter(getChildFragmentManager());
        pagerAdapter.addFragment(OrdersHistoryFragment.newInstance(ConstantStrings.MY_ORDERS_IN_PROGRESS), "");
        pagerAdapter.addFragment(OrdersHistoryFragment.newInstance(ConstantStrings.MY_ORDERS_History), "");
        pager.setAdapter(pagerAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_in_progress:
                pager.setCurrentItem(0);
                break;

            case R.id.btn_history:
                pager.setCurrentItem(1);
                break;
        }
    }
}
