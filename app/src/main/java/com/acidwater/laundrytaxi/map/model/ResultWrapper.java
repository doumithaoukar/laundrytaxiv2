package com.acidwater.laundrytaxi.map.model;

import java.util.ArrayList;

/**
 * Created by Doumith on 12/21/2017.
 */
public class ResultWrapper {

    ArrayList<Result> results;
    String status ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Result> getResults() {
        return results;
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
    }
}
