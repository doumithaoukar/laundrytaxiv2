package com.acidwater.laundrytaxi.map.model;

/**
 * Created by Doumith on 12/20/2017.
 */
public class Southwest {
    private double lat;

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    private double lng;

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
