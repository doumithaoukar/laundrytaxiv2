package com.acidwater.laundrytaxi.map.view;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.AddressEvent;
import com.acidwater.laundrytaxi.bus.events.GPSONEvent;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.data.order.OrderGps;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.acidwater.laundrytaxi.map.MapMvp;
import com.acidwater.laundrytaxi.map.presenter.MapPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Doumith on 7/3/2017.
 */
public class MapFragment extends MvpFragment<MapMvp.View, MapMvp.Presenter> implements View.OnClickListener, MapMvp.View, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private TextView pickupLocation;
    private TextView dropOffLocation;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastKnownLocation;
    float DEFAULT_ZOOM = 14.0f;


    private LinearLayout pickupContainer;
    private LinearLayout dropOffContainer;

    private final int PICK_UP_LOCATION = 0;
    private final int DROP_OF_LOCATION = 1;
    private int selectedLocation = PICK_UP_LOCATION;


    private MarkerOptions pickupMarker;
    private MarkerOptions dropOffMarker;

    private Marker markerPickup;
    private Marker markerDropOff;

    private MainActivityListener mListener;
    private Button btnContinue;
    private RelativeLayout progressBar;

    private Handler mHandler;
    private Runnable mRunnable;


    private MyOrder myOrder;
    private CompositeDisposable compositeDisposable;
    private OrderHistory mOrderHistory;

    private boolean isPickupEditable = true;
    private boolean isDropOffEditable = true;

    public static MapFragment newInstance(OrderHistory orderHistory) {
        MapFragment mapFragment = new MapFragment();
        if (orderHistory != null) {
            Bundle b = new Bundle();
            b.putParcelable("orderHistory", orderHistory);
            mapFragment.setArguments(b);
        }
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        RxUtils.disposeIfNotNull(compositeDisposable);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        progressBar = (RelativeLayout) rootView.findViewById(R.id.main_progress);
        setHasOptionsMenu(true);
        pickupLocation = (TextView) rootView.findViewById(R.id.pickup_location);
        dropOffLocation = (TextView) rootView.findViewById(R.id.drop_off_location);
        btnContinue = (Button) rootView.findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(this);
        compositeDisposable = new CompositeDisposable();
        registerToBus();
        pickupContainer = (LinearLayout) rootView.findViewById(R.id.pickup_container);
        dropOffContainer = (LinearLayout) rootView.findViewById(R.id.dropOff_container);

        pickupContainer.setOnClickListener(this);
        dropOffContainer.setOnClickListener(this);


        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i("DEBUG", "ON create view initializeMapView ");
        mListener.initializeMapView();

        if (savedInstanceState != null) {
            selectedLocation = savedInstanceState.getInt("selectedLocation");
            mLastKnownLocation = savedInstanceState.getParcelable("lastKnowLocation");
            pickupMarker = savedInstanceState.getParcelable("pickupMarker");
            dropOffMarker = savedInstanceState.getParcelable("dropOffMarker");
            myOrder = savedInstanceState.getParcelable("myOrder");
            mOrderHistory = savedInstanceState.getParcelable("orderHistory");
        } else {
            if (getArguments() != null) {
                mOrderHistory = getArguments().getParcelable("orderHistory");
            }
        }

        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                dropOffContainerClicked();
            }
        };
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_burger, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_navigation).setIcon(mListener.getNavigationIcon());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_navigation:
                mListener.openDrawer();
                break;
        }
        return false;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedLocation", selectedLocation);
        outState.putParcelable("lastKnowLocation", mLastKnownLocation);
        outState.putParcelable("pickupMarker", pickupMarker);
        outState.putParcelable("pickupMarker", dropOffMarker);
        outState.putParcelable("myOrder", myOrder);
        outState.putParcelable("orderHistory", mOrderHistory);

    }

    public void syncMap() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                setMarkerListener();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //Location Permission already granted
                        buildGoogleApiClient();
                        mGoogleMap.setMyLocationEnabled(true);
                    } else {
                        //Request Location Permission
                        mListener.initializeMapView();
                    }
                } else {
                    buildGoogleApiClient();
                    mGoogleMap.setMyLocationEnabled(true);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();

    }

    @Override
    public MapMvp.Presenter createPresenter() {
        return new MapPresenter(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    public synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        progressBar.setVisibility(View.VISIBLE);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            if (mGoogleMap != null) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                Log.i("DEBUG", "initializeMapView:build google api client");
                mListener.initializeMapView();
            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (getActivity() != null) {

            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                mLastKnownLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);
                progressBar.setVisibility(View.GONE);


                if (mLastKnownLocation != null) {
                    if (mOrderHistory != null) {
                        setOrderHistory();
                    }
                    pickupContainerClicked();
                    mHandler.postDelayed(mRunnable, 500);
                } else {
                    buildGoogleApiClient();
                }
            }
        }
    }


    private void setOrderHistory() {
        if (mOrderHistory != null) {
            setPickUpMarker(mOrderHistory.getPickupLocation().getOrderGps().getLatitude(),
                    mOrderHistory.getPickupLocation().getOrderGps().getLongitude());
            setDropOffMarker(mOrderHistory.getDropOffLocation().getOrderGps().getLatitude(),
                    mOrderHistory.getDropOffLocation().getOrderGps().getLongitude());
        }
        checkEditableItems();
    }

    private void checkEditableItems() {

        isPickupEditable = GeneralUtils.isPickupDropOffTimeEditable(mOrderHistory.getPickupTimeStamp());
        isDropOffEditable = GeneralUtils.isPickupDropOffTimeEditable(mOrderHistory.getDropOffTimeStamp());

        if (isPickupEditable) {
            pickupContainer.setClickable(true);
            markerPickup.setDraggable(true);
        } else {
            pickupContainer.setClickable(false);
            markerPickup.setDraggable(false);
        }

        if (isDropOffEditable) {
            dropOffContainer.setClickable(true);
            markerDropOff.setDraggable(true);
        } else {
            dropOffContainer.setClickable(false);
            markerDropOff.setDraggable(false);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }


    private void setPickUpMarker(double latitude, double longitude) {
        pickupMarker = new MarkerOptions().position(new LatLng(latitude, longitude));
        pickupMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_bus));
        pickupMarker.title(getResources().getString(R.string.Pickup_location));
        markerPickup = mGoogleMap.addMarker(pickupMarker);
        markerPickup.setDraggable(isPickupEditable);
        presenter.getPickOffAddress(pickupMarker.getPosition().latitude, pickupMarker.getPosition().longitude);

    }

    private void setDropOffMarker(double latitude, double longitude) {
        dropOffMarker = new MarkerOptions().position(new LatLng(latitude, longitude));
        dropOffMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_bus));
        dropOffMarker.title(getResources().getString(R.string.drop_off_location));
        markerDropOff = mGoogleMap.addMarker(dropOffMarker);
        markerDropOff.setDraggable(isDropOffEditable);
        presenter.getDropOffAddress(dropOffMarker.getPosition().latitude, dropOffMarker.getPosition().longitude);


    }

    private void setMarkerListener() {
        mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                if (marker.getTitle().equalsIgnoreCase(getResources().getString(R.string.Pickup_location))) {
                    pickupMarker = new MarkerOptions().position(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                    presenter.getPickOffAddress(marker.getPosition().latitude, marker.getPosition().longitude);
                } else {
                    dropOffMarker = new MarkerOptions().position(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                    presenter.getDropOffAddress(marker.getPosition().latitude, marker.getPosition().longitude);
                }

            }
        });
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                resetMarkers(latLng);
            }
        });
    }

    private void resetMarkers(LatLng latLng) {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
            if (selectedLocation == PICK_UP_LOCATION) {
                if (isPickupEditable) {
                    setPickUpMarker(latLng.latitude, latLng.longitude);
                } else {
                    // set pick up location from order history
                    setPickUpMarker(mOrderHistory.getPickupLocation().getOrderGps().getLatitude(),
                            mOrderHistory.getDropOffLocation().getOrderGps().getLongitude());
                }
                if (dropOffMarker != null) {
                    setDropOffMarker(dropOffMarker.getPosition().latitude, dropOffMarker.getPosition().longitude);
                }
            } else {
                if (isDropOffEditable) {
                    setDropOffMarker(latLng.latitude, latLng.longitude);
                } else {
                    // drop off not editable set drop off location
                    setDropOffMarker(mOrderHistory.getDropOffLocation().getOrderGps().getLatitude(),
                            mOrderHistory.getDropOffLocation().getOrderGps().getLongitude());
                }
                if (pickupMarker != null) {
                    setPickUpMarker(pickupMarker.getPosition().latitude, pickupMarker.getPosition().longitude);
                }
            }
            animateCamera();
        }
    }

    @Override
    public void setPickOffAddress(String address) {
        if (address == null || address.toString().toString().isEmpty()) {
            pickupLocation.setText(getString(R.string.unknown_location) + " (" + pickupMarker.getPosition().latitude + "," +
                    pickupMarker.getPosition().longitude + ")");
        } else {
            pickupLocation.setText(address);
        }
    }

    @Override
    public void setDropOffAddress(String address) {
        if (address == null || address.toString().toString().isEmpty()) {
            dropOffLocation.setText(getString(R.string.unknown_location) + " (" + dropOffMarker.getPosition().latitude + "," +
                    dropOffMarker.getPosition().longitude + ")");
        } else {
            dropOffLocation.setText(address);
        }
    }

    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSuccess() {
        myOrder = new MyOrder();
        OrderLocation pickupOrderLocation = new OrderLocation();
        OrderGps pickupOrderGps = new OrderGps();
        pickupOrderGps.setLatitude(pickupMarker.getPosition().latitude);
        pickupOrderGps.setLongitude(pickupMarker.getPosition().longitude);
        pickupOrderLocation.setOrderGps(pickupOrderGps);
        pickupOrderLocation.setAddress(pickupLocation.getText().toString());
        myOrder.setPickupLocation(pickupOrderLocation);


        OrderLocation dropOffOrderLocation = new OrderLocation();
        OrderGps dropOffOrderGps = new OrderGps();
        dropOffOrderGps.setLatitude(dropOffMarker.getPosition().latitude);
        dropOffOrderGps.setLongitude(dropOffMarker.getPosition().longitude);
        dropOffOrderLocation.setOrderGps(dropOffOrderGps);
        dropOffOrderLocation.setAddress(dropOffLocation.getText().toString());
        myOrder.setDropOffLocation(dropOffOrderLocation);


        GeneralUtils.saveMyOrder(getContext(), myOrder);
        mListener.navigationToScreen(ConstantStrings.ORDER_DETAILS, mOrderHistory);

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    private void pickupContainerClicked() {
        selectedLocation = PICK_UP_LOCATION;
        if (pickupMarker == null) {
            if (mLastKnownLocation != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(mLastKnownLocation.getLatitude(),
                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                setPickUpMarker(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            } else {
                buildGoogleApiClient();
            }

        } else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(pickupMarker.getPosition().latitude,
                            pickupMarker.getPosition().longitude), DEFAULT_ZOOM));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

        }
    }

    private void dropOffContainerClicked() {
        selectedLocation = DROP_OF_LOCATION;
        if (dropOffMarker == null) {
            if (mLastKnownLocation != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(mLastKnownLocation.getLatitude(),
                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                setDropOffMarker(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());

            } else {
                buildGoogleApiClient();
            }
        } else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(dropOffMarker.getPosition().latitude,
                            dropOffMarker.getPosition().longitude), DEFAULT_ZOOM));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pickup_container:
                selectedLocation = PICK_UP_LOCATION;
                mListener.navigationToScreen(ConstantStrings.ADDRESS_MAP_FRAGMENT);
                break;

            case R.id.dropOff_container:
                selectedLocation = DROP_OF_LOCATION;
                mListener.navigationToScreen(ConstantStrings.ADDRESS_MAP_FRAGMENT);
                break;

            case R.id.btn_continue:
                if (pickupMarker != null && dropOffMarker != null) {
                    presenter.validateDate(pickupMarker.getPosition().latitude, pickupMarker.getPosition().longitude, dropOffMarker.getPosition().latitude,
                            dropOffMarker.getPosition().longitude, pickupLocation.getText().toString(), dropOffLocation.getText().toString());
                } else {
                    Toast.makeText(getContext(), getString(R.string.enter_pickup_dropOff), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }


    private void registerToBus() {
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof AddressEvent) {

                    AddressEvent addressEvent = (AddressEvent) object;

                    LatLng latLng = new LatLng(addressEvent.getLatitude(), addressEvent.getLongitude());
                    resetMarkers(latLng);
                }

                if (object instanceof GPSONEvent) {
                    buildGoogleApiClient();
                }
            }
        }));
    }

    private void animateCamera() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        ArrayList<Marker> markers = new ArrayList<>();
        if (markerPickup != null) {
            markers.add(markerPickup);
        }
        if (markerDropOff != null) {
            markers.add(markerDropOff);
        }
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();

        int padding = 300; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mGoogleMap.moveCamera(cu);
        mGoogleMap.animateCamera(cu);
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomBy(-1.5f));

    }
}
