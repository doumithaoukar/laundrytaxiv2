package com.acidwater.laundrytaxi.map.model;

/**
 * Created by Doumith on 12/20/2017.
 */
public class Bounds {
    private Northeast northeast;

    public Northeast getNortheast() {
        return this.northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }

    private Southwest southwest;

    public Southwest getSouthwest() {
        return this.southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }
}
