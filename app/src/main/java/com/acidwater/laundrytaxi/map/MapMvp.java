package com.acidwater.laundrytaxi.map;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.map.model.Result;
import com.acidwater.laundrytaxi.map.model.ResultWrapper;
import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/8/2017.
 */
public interface MapMvp {


    interface Interactor {

        Observable<ResultWrapper> getAddress(double latitude, double longitude);

    }

    interface Presenter extends MvpPresenter<View> {

        void getPickOffAddress(double latitude, double longitude);

        void getDropOffAddress(double latitude, double longitude);

        void validateDate(double pickOffLatitude, double pickOffLongitude, double dropOffLatitude,
                          double dropOffLongitude, String pickOffAddress, String dropOffAddress);


    }

    interface View extends MvpView {

        void setPickOffAddress(String address);

        void setDropOffAddress(String address);

        void setError(String message);

        void setSuccess();

        void showLoadingView();

        void hideLoadingView();
    }
}
