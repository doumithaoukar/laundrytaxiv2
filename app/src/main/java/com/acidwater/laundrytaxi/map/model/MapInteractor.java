package com.acidwater.laundrytaxi.map.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.map.MapMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;


/**
 * Created by Doumith on 12/20/2017.
 */
public class MapInteractor implements MapMvp.Interactor {

    private Context context;
    private GeoAPI geoAPI;

    private static final String GEOCODING_API_KEY = "AIzaSyAjKG9MFngETMAnoaLJ2G9ALD_mR4aftxI";

    public MapInteractor(Context context) {
        this.context = context;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new TokenInterceptor()).build();
        String baseURL = "https://maps.googleapis.com/maps/api/geocode/";

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        geoAPI = retrofit.create(GeoAPI.class);
    }



    @Override
    public Observable<ResultWrapper> getAddress(double latitude, double longitude) {
        Observable<ResultWrapper> observable = null;
        Map<String, String> params = new HashMap<String, String>();
        params.put("latlng", latitude + "," + longitude);
        params.put("key", GEOCODING_API_KEY);
        observable =
                geoAPI.getGPS(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    public interface GeoAPI {
        @GET("json")
        Observable<ResultWrapper> getGPS(@QueryMap Map<String, String> params);
    }
}
