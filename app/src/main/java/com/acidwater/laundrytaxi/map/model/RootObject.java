package com.acidwater.laundrytaxi.map.model;

import java.util.ArrayList;

/**
 * Created by Doumith on 12/20/2017.
 */
public class RootObject {
    private ArrayList<Result> results;

    public ArrayList<Result> getResults() {
        return this.results;
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
    }

    private String status;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
