package com.acidwater.laundrytaxi.map.model;

/**
 * Created by Doumith on 12/20/2017.
 */
public class Viewport {
    private Northeast2 northeast;

    public Northeast2 getNortheast() {
        return this.northeast;
    }

    public void setNortheast(Northeast2 northeast) {
        this.northeast = northeast;
    }

    private Southwest2 southwest;

    public Southwest2 getSouthwest() {
        return this.southwest;
    }

    public void setSouthwest(Southwest2 southwest) {
        this.southwest = southwest;
    }
}
