package com.acidwater.laundrytaxi.map.model;

import java.util.ArrayList;

/**
 * Created by Doumith on 12/20/2017.
 */
public class Result {
    private ArrayList<AddressComponent> address_components;

    public ArrayList<AddressComponent> getAddressComponents() {
        return this.address_components;
    }

    public void setAddressComponents(ArrayList<AddressComponent> address_components) {
        this.address_components = address_components;
    }

    private String formatted_address;

    public String getFormattedAddress() {
        return this.formatted_address;
    }

    public void setFormattedAddress(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    private Geometry geometry;

    public Geometry getGeometry() {
        return this.geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    private String place_id;

    public String getPlaceId() {
        return this.place_id;
    }

    public void setPlaceId(String place_id) {
        this.place_id = place_id;
    }

    private ArrayList<String> types;

    public ArrayList<String> getTypes() {
        return this.types;
    }

    public void setTypes(ArrayList<String> types) {
        this.types = types;
    }
}
