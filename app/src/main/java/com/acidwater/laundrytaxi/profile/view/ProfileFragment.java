package com.acidwater.laundrytaxi.profile.view;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.AddressEvent;
import com.acidwater.laundrytaxi.data.order.OrderGps;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.notifications.presenter.NotificationsPresenter;
import com.acidwater.laundrytaxi.profile.ProfileMvp;
import com.acidwater.laundrytaxi.profile.presenter.ProfilePresenter;
import com.acidwater.laundrytaxi.settings.model.Setting;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Doumith on 7/8/2017.
 */
public class ProfileFragment extends MvpFragment<ProfileMvp.View, ProfileMvp.Presenter> implements ProfileMvp.View, View.OnClickListener {


    private MainActivityListener mListener;
    private EditText firstName;
    private EditText lastName;
    private EditText address;
    private EditText building;
    private EditText apartmentNo;
    private EditText email;
    private EditText mobileNumber;
    private Button btnContinue;
    private LinearLayout addressContainer;
    private ImageView btnLocation;
    private CompositeDisposable compositeDisposable;
    private OrderLocation orderLocation;
    private User user;

    private RelativeLayout progressBar;

    public static ProfileFragment newInstance() {
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        RxUtils.disposeIfNotNull(compositeDisposable);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_profile, container, false);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        setHasOptionsMenu(true);
        firstName = (EditText) mView.findViewById(R.id.firstname);
        lastName = (EditText) mView.findViewById(R.id.lastname);
        email = (EditText) mView.findViewById(R.id.email);
        mobileNumber = (EditText) mView.findViewById(R.id.mobileNumber);
        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(this);
        address = (EditText) mView.findViewById(R.id.address);
        building = (EditText) mView.findViewById(R.id.building_name);
        apartmentNo = (EditText) mView.findViewById(R.id.apartment_nb);
        addressContainer = (LinearLayout) mView.findViewById(R.id.address_container);
        btnLocation = (ImageView) mView.findViewById(R.id.btnLocation);

        compositeDisposable = new CompositeDisposable();
        registerToBus();
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (presenter.isLocationEnabled()) {
                    mListener.navigationToScreen(ConstantStrings.ADDRESS_MAP_FRAGMENT);
                } else {
                    mListener.showGpsDialog();
                }
            }
        });
        disableViews();


        Selection.setSelection(mobileNumber.getText(), mobileNumber.getText().length());


        mobileNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith(getString(R.string.mobile_prefix))) {
                    mobileNumber.setText(getString(R.string.mobile_prefix));
                    Selection.setSelection(mobileNumber.getText(), mobileNumber.getText().length());

                }

            }
        });
        return mView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_profle:
                enableViews();
                break;
        }
        return true;

    }

    private void enableViews() {
        firstName.setEnabled(true);
        lastName.setEnabled(true);
        address.setEnabled(true);
        building.setEnabled(true);
        apartmentNo.setEnabled(true);
        mobileNumber.setEnabled(true);

        if (GeneralUtils.getSharedPref(getContext(), ConstantStrings.USER_TYPE).equals(ConstantStrings.USER_EMAIL)) {
            email.setEnabled(false);
        } else {
            email.setEnabled(true);
        }
    }

    private void disableViews() {
        firstName.setEnabled(false);
        lastName.setEnabled(false);
        address.setEnabled(false);
        building.setEnabled(false);
        apartmentNo.setEnabled(false);
        mobileNumber.setEnabled(false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getData();
    }

    @Override
    public ProfileMvp.Presenter createPresenter() {
        return new ProfilePresenter(getContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                if (orderLocation == null) {
                    orderLocation = new OrderLocation();
                    OrderGps orderGps = new OrderGps();
                    orderLocation.setOrderGps(orderGps);
                }
                orderLocation.setAddress(address.getText().toString());
                orderLocation.setApartmentNO(apartmentNo.getText().toString());
                orderLocation.setBuilding(building.getText().toString());

                presenter.validate(firstName.getText().toString(),
                        lastName.getText().toString(),
                        orderLocation,
                        email.getText().toString(),
                        mobileNumber.getText().toString());
                break;
        }
    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void HideLoadingView() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setResultSuccessful(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        disableViews();
    }

    @Override
    public void setData(User user) {
        this.user = user;
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        email.setText(user.getUsername());
        mobileNumber.setText(user.getPhoneNumber());

        if (user.getHomeLocation() != null && !TextUtils.isEmpty(user.getHomeLocation().getAddress())) {
            address.setText(user.getHomeLocation().getAddress());
        }
        if (user.getHomeLocation() != null && !TextUtils.isEmpty(user.getHomeLocation().getBuilding())) {
            building.setText(user.getHomeLocation().getBuilding());
        }
        if (user.getHomeLocation() != null && !TextUtils.isEmpty(user.getHomeLocation().getApartmentNO())) {
            apartmentNo.setText(user.getHomeLocation().getApartmentNO());
        }
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void registerToBus() {
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof AddressEvent) {
                    AddressEvent messageEvent = (AddressEvent) object;
                    address.setText(messageEvent.getLocation());
                    orderLocation = new OrderLocation();
                    OrderGps orderGps = new OrderGps();
                    orderGps.setLatitude(messageEvent.getLatitude());
                    orderGps.setLongitude(messageEvent.getLongitude());
                    orderLocation.setOrderGps(orderGps);
                    orderLocation.setAddress(messageEvent.getLocation());
                }

            }

        }));
    }
}
