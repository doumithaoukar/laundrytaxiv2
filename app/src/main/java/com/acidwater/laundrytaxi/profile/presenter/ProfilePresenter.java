package com.acidwater.laundrytaxi.profile.presenter;

import android.content.Context;
import android.location.LocationManager;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.PriceListInteractor;
import com.acidwater.laundrytaxi.profile.ProfileMvp;
import com.acidwater.laundrytaxi.profile.model.ProfileInteractor;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.w3c.dom.Text;

import java.io.File;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ProfilePresenter extends MvpBasePresenter<ProfileMvp.View> implements ProfileMvp.Presenter {


    private Context context;
    private CompositeDisposable compositeDisposable;
    private ProfileInteractor profileInteractor;

    public ProfilePresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        profileInteractor = new ProfileInteractor(context);
    }


    @Override
    public void getData() {
        if (isViewAttached()) {
            getView().setData(GeneralUtils.getUserProfile(context));
        }
    }

    @Override
    public void validate(String firstname, String lastname, OrderLocation homeLocation, String email, String phone) {
        if (TextUtils.isEmpty(firstname) || TextUtils.isEmpty(lastname) || homeLocation == null || TextUtils.isEmpty(homeLocation.getAddress())
                || TextUtils.isEmpty(email) || TextUtils.isEmpty(phone)) {
            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_required_fields));
                return;
            }
        }
        editProfile(firstname, lastname, homeLocation, email, phone);

    }

    @Override
    public void editProfile(String firstname, String lastname, OrderLocation homeLocation, String email, String phone) {
        compositeDisposable.add(profileInteractor.editProfile(firstname, lastname, homeLocation, email, phone).subscribeWith(new ServiceObserver<UserWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(UserWrapper userWrapper) {
                GeneralUtils.saveUserProfile(context, userWrapper);
                if (isViewAttached()) {
                    getView().setResultSuccessful(context.getString(R.string.profile_updated_successfully));
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {
                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }
}
