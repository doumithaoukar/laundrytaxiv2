package com.acidwater.laundrytaxi.profile;

import android.graphics.Bitmap;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/30/2017.
 */
public interface ProfileMvp {


    interface Interactor {

        Observable<ServerResponse> uploadProfilePicture(File file);

        Observable<UserWrapper> getProfile();

        Observable<UserWrapper> editProfile(String firstname, String lastname, OrderLocation homeLocation, String email, String phone);


        Observable<ServerResponse> uploadPushToke(String regID);

    }


    interface Presenter extends MvpPresenter<View> {

        void getData();

        void editProfile(String firstname, String lastname, OrderLocation homeLocation, String email, String phone);

        void validate(String firstname, String lastname, OrderLocation homeLocation, String email, String phone);

        boolean isLocationEnabled();

    }


    interface View extends MvpView {
        void setData(User user);

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful(String message);
    }
}
