package com.acidwater.laundrytaxi.profile.model;

import android.content.Context;


import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateRedeemPoints;
import com.acidwater.laundrytaxi.data.ServerResponse;


import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.profile.ProfileMvp;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Doumith on 6/18/2017.
 */
public class ProfileInteractor implements ProfileMvp.Interactor {

    private ProfileApi profileApi;
    private Context context;

    public ProfileInteractor(Context context) {
        this.context = context;
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        profileApi = retrofit.create(ProfileApi.class);

    }

    @Override
    public Observable<ServerResponse> uploadProfilePicture(File file) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("userimage", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "userimage");

        Observable<ServerResponse> observable = profileApi.uploadFile(body, name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<UserWrapper> getProfile() {
        Observable<UserWrapper> observable = profileApi.getProfile().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        observable.subscribeWith(new ServiceObserver<UserWrapper>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(UserWrapper userWrapper) {

                if (context != null && userWrapper != null && userWrapper.getUser() != null) {
                    MainBus.getInstance().send(new UpdateRedeemPoints(userWrapper.getUser().getRewardPoints()));
                    GeneralUtils.saveUserProfile(context, userWrapper);
                }

            }

            @Override
            public void onError(LaundryTaxiException e) {
            }
        });
        return observable;
    }

    @Override
    public Observable<UserWrapper> editProfile(String firstname, String lastname, OrderLocation homeLocation, String email, String phone) {

        Observable<UserWrapper> observable = null;
        EditProfile editProfile = new EditProfile(firstname, lastname, homeLocation, email, phone);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (editProfile.toJson()).toString());
        observable =
                profileApi.editProfile(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    @Override
    public Observable<ServerResponse> uploadPushToke(String regID) {
        PushReg pushReg = new PushReg("android", regID);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (pushReg.toJson()).toString());
        Observable<ServerResponse> observable =
                profileApi.uploadPushToken(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface ProfileApi {

        @Multipart
        @POST(ConstantStrings.SERVICE_UPLOAD_PIC)
        Observable<ServerResponse> uploadFile(@Part MultipartBody.Part userimage, @Part("name") RequestBody name);


        @Headers("Content-Type: application/json")
        @GET(ConstantStrings.SERVICE_PROFILE)
        Observable<UserWrapper> getProfile();

        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_EDIT_PROFILE)
        Observable<UserWrapper> editProfile(@Body RequestBody body);


        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_UPLOAD_PUSH_TOKEN)
        Observable<ServerResponse> uploadPushToken(@Body RequestBody body);
    }

    private class EditProfile {


        private String firstname;
        private String lastname;
        private String phone;
        private OrderLocation homeLocation;

        public EditProfile(String firstname, String lastname, OrderLocation homeLocation, String email, String phone) {
            this.firstname = firstname;
            this.lastname = lastname;
            this.phone = phone;
            this.homeLocation = homeLocation;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }

    private class PushReg {
        @SerializedName("platform")
        private String platform;

        private String token;

        public PushReg(String platform, String token) {
            this.platform = platform;
            this.token = token;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }


}
