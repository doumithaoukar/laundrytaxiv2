package com.acidwater.laundrytaxi.data.order;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Doumith on 8/6/2017.
 */
public class OrderItem implements Parcelable {


    private int id;
    private int quantity;
    private String price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public OrderItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.quantity);
        dest.writeString(this.price);
    }

    protected OrderItem(Parcel in) {
        this.id = in.readInt();
        this.quantity = in.readInt();
        this.price = in.readString();
    }

    public static final Creator<OrderItem> CREATOR = new Creator<OrderItem>() {
        @Override
        public OrderItem createFromParcel(Parcel source) {
            return new OrderItem(source);
        }

        @Override
        public OrderItem[] newArray(int size) {
            return new OrderItem[size];
        }
    };
}
