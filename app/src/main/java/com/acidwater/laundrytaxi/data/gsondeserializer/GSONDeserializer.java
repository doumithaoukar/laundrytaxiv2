package com.acidwater.laundrytaxi.data.gsondeserializer;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Interface of Deserialization type whether its JSON or XML etc...
 *
 * @author CME
 */
public interface GSONDeserializer<T> {

   public List<T> deserializeArray(String source, Class<T[]> cls, Gson gson) throws JsonSyntaxException;

   public T deserialize(String source, Class<T> cls, Gson gson) throws JsonSyntaxException;

}
