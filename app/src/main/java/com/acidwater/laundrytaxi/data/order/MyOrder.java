package com.acidwater.laundrytaxi.data.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Doumith on 8/6/2017.
 */
public class MyOrder implements Parcelable {


    @SerializedName("orderID")
    private String orderID;

    @SerializedName("DeliveryMethod")
    private String deliveryMethod;

    @SerializedName("PaymentMethod")
    private String paymentMethod;


    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    @SerializedName("PromoCode")
    private String promoCode;


    @SerializedName("PickupTimestamp")
    private long pickUpTimeStamp;

    @SerializedName("DropoffTimestamp")
    private long dropOffTimeStamp;

    @SerializedName("Instruction")
    private String otherInstructions;

    @SerializedName("PickupLocation")
    private OrderLocation pickupLocation;


    @SerializedName("DropoffLocation")
    private OrderLocation dropOffLocation;

    @SerializedName("OrderData")
    private ArrayList<OrderItem> orderItems;

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public long getPickUpTimeStamp() {
        return pickUpTimeStamp;
    }

    public void setPickUpTimeStamp(long pickUpTimeStamp) {
        this.pickUpTimeStamp = pickUpTimeStamp;
    }

    public long getDropOffTimeStamp() {
        return dropOffTimeStamp;
    }

    public void setDropOffTimeStamp(long dropOffTimeStamp) {
        this.dropOffTimeStamp = dropOffTimeStamp;
    }

    public String getOtherInstructions() {
        return otherInstructions;
    }

    public void setOtherInstructions(String otherInstructions) {
        this.otherInstructions = otherInstructions;
    }

    public OrderLocation getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(OrderLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public OrderLocation getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(OrderLocation dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public MyOrder() {
    }

    public JSONObject toJson() {
        try {
            return new JSONObject(new Gson().toJson(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderID);
        dest.writeString(this.deliveryMethod);
        dest.writeString(this.paymentMethod);
        dest.writeString(this.promoCode);
        dest.writeDouble(this.pickUpTimeStamp);
        dest.writeDouble(this.dropOffTimeStamp);
        dest.writeString(this.otherInstructions);
        dest.writeParcelable(this.pickupLocation, flags);
        dest.writeParcelable(this.dropOffLocation, flags);
        dest.writeTypedList(this.orderItems);
    }

    protected MyOrder(Parcel in) {
        this.orderID = in.readString();
        this.deliveryMethod = in.readString();
        this.paymentMethod = in.readString();
        this.promoCode = in.readString();
        this.pickUpTimeStamp = in.readLong();
        this.dropOffTimeStamp = in.readLong();
        this.otherInstructions = in.readString();
        this.pickupLocation = in.readParcelable(OrderLocation.class.getClassLoader());
        this.dropOffLocation = in.readParcelable(OrderLocation.class.getClassLoader());
        this.orderItems = in.createTypedArrayList(OrderItem.CREATOR);
    }

    public static final Creator<MyOrder> CREATOR = new Creator<MyOrder>() {
        @Override
        public MyOrder createFromParcel(Parcel source) {
            return new MyOrder(source);
        }

        @Override
        public MyOrder[] newArray(int size) {
            return new MyOrder[size];
        }
    };
}
