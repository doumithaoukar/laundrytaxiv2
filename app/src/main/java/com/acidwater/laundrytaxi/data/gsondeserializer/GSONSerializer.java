package com.acidwater.laundrytaxi.data.gsondeserializer;

/**
 * Interface of Serialization type.
 *
 * @param <T>
 * @author CME
 */
public interface GSONSerializer<T> {
    public String serialize(T cls);
}
