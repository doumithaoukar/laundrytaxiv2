package com.acidwater.laundrytaxi.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 5/1/2018.
 */
public class Message implements Parcelable {

    @SerializedName("Text")
    private String text;

    @SerializedName("Blocking")
    private boolean isBlocking;


    @SerializedName("ArText")
    private String textAr;

    @SerializedName("EnText")
    private String textEn;


    public String getTextAr() {
        return textAr;
    }

    public void setTextAr(String textAr) {
        this.textAr = textAr;
    }

    public String getTextEn() {
        return textEn;
    }

    public void setTextEn(String textEn) {
        this.textEn = textEn;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isBlocking() {
        return isBlocking;
    }

    public void setBlocking(boolean blocking) {
        isBlocking = blocking;
    }

    public Message() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeByte(this.isBlocking ? (byte) 1 : (byte) 0);
        dest.writeString(this.textAr);
        dest.writeString(this.textEn);
    }

    protected Message(Parcel in) {
        this.text = in.readString();
        this.isBlocking = in.readByte() != 0;
        this.textAr = in.readString();
        this.textEn = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
