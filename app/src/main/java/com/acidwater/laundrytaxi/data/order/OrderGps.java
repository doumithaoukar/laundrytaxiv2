package com.acidwater.laundrytaxi.data.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 8/6/2017.
 */
public class OrderGps implements Parcelable {

    @SerializedName("lat")
    private double latitude;

    @SerializedName("long")
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    public OrderGps() {
    }

    protected OrderGps(Parcel in) {
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Parcelable.Creator<OrderGps> CREATOR = new Parcelable.Creator<OrderGps>() {
        @Override
        public OrderGps createFromParcel(Parcel source) {
            return new OrderGps(source);
        }

        @Override
        public OrderGps[] newArray(int size) {
            return new OrderGps[size];
        }
    };
}
