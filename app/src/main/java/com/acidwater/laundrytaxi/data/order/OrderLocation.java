package com.acidwater.laundrytaxi.data.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 8/6/2017.
 */
public class OrderLocation implements Parcelable {

    @SerializedName("Address")
    private String address;

    @SerializedName("BuildingName")
    private String building;

    @SerializedName("AppartementNo")
    private String apartmentNO;

    @SerializedName("GPS")
    private OrderGps orderGps;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OrderGps getOrderGps() {
        return orderGps;
    }

    public void setOrderGps(OrderGps orderGps) {
        this.orderGps = orderGps;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getApartmentNO() {
        return apartmentNO;
    }

    public void setApartmentNO(String apartmentNO) {
        this.apartmentNO = apartmentNO;
    }

    public OrderLocation() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeString(this.building);
        dest.writeString(this.apartmentNO);
        dest.writeParcelable(this.orderGps, flags);
    }

    protected OrderLocation(Parcel in) {
        this.address = in.readString();
        this.building = in.readString();
        this.apartmentNO = in.readString();
        this.orderGps = in.readParcelable(OrderGps.class.getClassLoader());
    }

    public static final Creator<OrderLocation> CREATOR = new Creator<OrderLocation>() {
        @Override
        public OrderLocation createFromParcel(Parcel source) {
            return new OrderLocation(source);
        }

        @Override
        public OrderLocation[] newArray(int size) {
            return new OrderLocation[size];
        }
    };
}
