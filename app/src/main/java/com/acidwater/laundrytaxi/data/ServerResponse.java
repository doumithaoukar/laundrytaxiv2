package com.acidwater.laundrytaxi.data;

/**
 * Created by Doumith on 7/11/2017.
 */
public class ServerResponse {

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
