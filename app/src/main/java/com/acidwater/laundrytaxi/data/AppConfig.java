package com.acidwater.laundrytaxi.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Doumith on 8/1/2017.
 */
public class AppConfig {

    @SerializedName("PaymentMethods")
    private ArrayList<String> paymentMethods;


    @SerializedName("Currency")
    private String currency;

    @SerializedName("FreeOrderUpToValue")
    private String freeOrderUptoValue;

    @SerializedName("ApplicationShareURL")
    private String shareURL;

    @SerializedName("DiscountPer100RewardPoint")
    private double discountPer100RewardPoint;

    @SerializedName("Message")
    private Message message;


    public Message getMessage() {
        return message;
    }

    public ArrayList<String> getPaymentMethods() {
        return paymentMethods;
    }


    public String getCurrency() {
        return currency;
    }


    public String getFreeOrderUptoValue() {
        return freeOrderUptoValue;
    }

    public String getShareURL() {
        return shareURL;
    }

    public double getDiscountPer100RewardPoint() {
        return discountPer100RewardPoint;
    }
}


