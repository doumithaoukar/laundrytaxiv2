package com.acidwater.laundrytaxi.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 8/1/2017.
 */
public class AppConfigWrapper {

    private int status;
    @SerializedName("config")
    private AppConfig config;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public AppConfig getConfig() {
        return config;
    }

    public void setConfig(AppConfig config) {
        this.config = config;
    }
}
