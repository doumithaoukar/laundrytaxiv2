package com.acidwater.laundrytaxi.verification.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.laundrytaxi.BaseFragment;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.IntroActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.verification.VerificationMvp;
import com.acidwater.laundrytaxi.verification.presenter.VerificationPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class VerificationFragment extends MvpFragment<VerificationMvp.View, VerificationMvp.Presenter> implements View.OnClickListener, VerificationMvp.View {


    private Button btnContinue;
    private Button btnResend;

    private EditText verification_code;
    private RelativeLayout progressBar;
    private IntroActivityListener mListener;


    public static VerificationFragment newInstance() {
        VerificationFragment verificationFragment = new VerificationFragment();
        return verificationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_verification, container, false);

        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        btnResend = (Button) mView.findViewById(R.id.btn_resend_code);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        verification_code = (EditText) mView.findViewById(R.id.verification_code);
        btnContinue.setOnClickListener(this);
        btnResend.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validate(verification_code.getText().toString());
                break;

            case R.id.btn_resend_code:
                presenter.resendVerificationCode();
                break;
        }
    }

    @Override
    public VerificationMvp.Presenter createPresenter() {
        return new VerificationPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void setResultSuccessful() {
        mListener.navigateToSection(IntroActivity.MAIN_ACTIVITY);

    }

    @Override
    public void showServerMessage(String message) {
        Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
    }
}