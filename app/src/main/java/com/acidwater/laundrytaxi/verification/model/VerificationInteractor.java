package com.acidwater.laundrytaxi.verification.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.acidwater.laundrytaxi.verification.VerificationMvp;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class VerificationInteractor implements VerificationMvp.Interactor {

    private VerificationCodeApi verificationCodeApi;


    public VerificationInteractor(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().cookieJar(cookieJar).addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        verificationCodeApi = retrofit.create(VerificationCodeApi.class);
    }


    @Override
    public Observable<ServerResponse> submitVerificationCode(String verificationCode) {
        Observable<ServerResponse> observable = null;
        VerificationCode mVerificationCode = new VerificationCode(verificationCode);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (mVerificationCode.toJson()).toString());
        observable =
                verificationCodeApi.submitVerificationCode(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<ServerResponse> getVerificationCode() {
        Observable<ServerResponse> observable =
                verificationCodeApi.getVerificationCode().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    public interface VerificationCodeApi {
        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_VERIFY_CODE)
        Observable<ServerResponse> submitVerificationCode(@Body RequestBody body);

        @Headers("Content-Type: application/json")
        @GET(ConstantStrings.SERVICE_RESEND_VERIFY_CODE)
        Observable<ServerResponse> getVerificationCode();
    }

    private class VerificationCode {
        private String code;

        public VerificationCode(String code) {
            this.code = code;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }
}
