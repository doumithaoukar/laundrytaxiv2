package com.acidwater.laundrytaxi.verification.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.acidwater.laundrytaxi.verification.VerificationMvp;
import com.acidwater.laundrytaxi.verification.model.VerificationInteractor;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

/**
 * Created by Doumith on 7/13/2017.
 */
public class VerificationPresenter extends MvpBasePresenter<VerificationMvp.View> implements VerificationMvp.Presenter {

    private Context context;
    private VerificationInteractor verificationInteractor;
    private CompositeDisposable compositeDisposable;

    public VerificationPresenter(Context context) {
        this.context = context;
        verificationInteractor = new VerificationInteractor(context);
        compositeDisposable = new CompositeDisposable();

    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void submitVerificationCode(String verificationCode) {

        compositeDisposable.add(verificationInteractor.submitVerificationCode(verificationCode).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    getView().setResultSuccessful();
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void validate(String verificationCode) {

        if (TextUtils.isEmpty(verificationCode)) {

            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_verification_code));
                return;
            }

        }

        submitVerificationCode(verificationCode);
    }

    @Override
    public void resendVerificationCode() {


        compositeDisposable.add(verificationInteractor.getVerificationCode().subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponseResponse) {

                if (isViewAttached()) {
                    getView().showServerMessage(serverResponseResponse.getMessage());
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));


    }
}
