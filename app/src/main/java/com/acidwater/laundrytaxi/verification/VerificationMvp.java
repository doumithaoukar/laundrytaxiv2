package com.acidwater.laundrytaxi.verification;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Doumith on 7/12/2017.
 */
public interface VerificationMvp {


    interface Interactor {
        Observable<ServerResponse> submitVerificationCode(String verificationCode);

        Observable<ServerResponse> getVerificationCode();

    }

    interface Presenter extends MvpPresenter<View> {

        void submitVerificationCode(String verificationCode);

        void validate(String verificationCode);

        void resendVerificationCode();

    }

    interface View extends MvpView {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful();

        void showServerMessage(String message);

    }


}
