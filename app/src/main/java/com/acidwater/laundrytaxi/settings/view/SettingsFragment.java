package com.acidwater.laundrytaxi.settings.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.notifications.presenter.NotificationsPresenter;
import com.acidwater.laundrytaxi.settings.SettingsMvp;
import com.acidwater.laundrytaxi.settings.model.Setting;
import com.acidwater.laundrytaxi.settings.presenter.SettingsPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.List;

/**
 * Created by Doumith on 7/8/2017.
 */
public class SettingsFragment extends MvpFragment<SettingsMvp.View, SettingsMvp.Presenter> implements SettingsMvp.View {


    private MainActivityListener mListener;
    private RecyclerView recyclerView;

    public static SettingsFragment newInstance() {
        SettingsFragment settingsFragment = new SettingsFragment();
        return settingsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_settings, container, false);

        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getSettingList();
    }

    @Override
    public SettingsMvp.Presenter createPresenter() {
        return new SettingsPresenter(getContext());
    }

    @Override
    public void renderSettingList(List<Setting> settingList) {
        SettingsAdapter settingsAdapter = new SettingsAdapter(getContext(), settingList);
        recyclerView.setAdapter(settingsAdapter);
        settingsAdapter.setOnItemClickListener(new SettingsAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(Setting setting) {
                if (setting.getId() == ConstantStrings.SETTING_LOGOUT) {
                    confirmLogout();
                } else {
                    mListener.navigationToScreen(setting.getId());
                }
            }
        });

    }

    private void confirmLogout() {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.logout))
                .setMessage(getString(R.string.confirm_logout))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GeneralUtils.logout(getContext());
                    }

                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }
}
