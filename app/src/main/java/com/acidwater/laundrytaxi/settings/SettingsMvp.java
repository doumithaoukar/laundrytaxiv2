package com.acidwater.laundrytaxi.settings;

import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.settings.model.Setting;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

/**
 * Created by Doumith on 7/30/2017.
 */
public interface SettingsMvp {


    interface Interactor {
        List<Setting> getSettingList();
    }


    interface Presenter extends MvpPresenter<View> {

        void getSettingList();
    }

    interface View extends MvpView {
        void renderSettingList(List<Setting> settingList);

    }
}
