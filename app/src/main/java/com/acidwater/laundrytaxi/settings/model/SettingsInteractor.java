package com.acidwater.laundrytaxi.settings.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.menu.MenuMvp;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.settings.SettingsMvp;
import com.acidwater.laundrytaxi.utils.GeneralUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 7/13/2017.
 */
public class SettingsInteractor implements SettingsMvp.Interactor {


    private Context context;

    public SettingsInteractor(Context context) {
        this.context = context;
    }


    @Override
    public List<Setting> getSettingList() {
        ArrayList<Setting> list = new ArrayList<>();
        if (GeneralUtils.getSharedPref(context, ConstantStrings.USER_TYPE).equals(ConstantStrings.USER_EMAIL)) {
            list.add(new Setting(ConstantStrings.SETTING_CHANGE_PASSWORD, R.drawable.ic_change_password, context.getString(R.string.change_password)));

        }
        list.add(new Setting(ConstantStrings.SETTING_CHANGE_LANGUAGE, R.drawable.ic_change_language, context.getString(R.string.change_language)));
        list.add(new Setting(ConstantStrings.SETTING_CONTACT_US, R.drawable.ic_contact, context.getString(R.string.contact_us)));
        list.add(new Setting(ConstantStrings.SETTING_ABOUT, R.drawable.ic_info, context.getString(R.string.about)));
        list.add(new Setting(ConstantStrings.SETTING_FAQ, R.drawable.ic_contact, context.getString(R.string.faq)));
        list.add(new Setting(ConstantStrings.SETTING_T_AND_C, R.drawable.ic_info, context.getString(R.string.T_and_c)));
        list.add(new Setting(ConstantStrings.SETTING_LOGOUT, R.drawable.ic_logout, context.getString(R.string.logout)));

        return list;
    }
}
