package com.acidwater.laundrytaxi.settings.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Doumith on 8/1/2017.
 */
public class Setting implements Parcelable {

    private int id;
    private int image;
    private String name;

    public Setting(int id, int image, String name) {
        this.id = id;
        this.image = image;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.image);
        dest.writeString(this.name);
    }

    public Setting() {
    }

    protected Setting(Parcel in) {
        this.id = in.readInt();
        this.image = in.readInt();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Setting> CREATOR = new Parcelable.Creator<Setting>() {
        @Override
        public Setting createFromParcel(Parcel source) {
            return new Setting(source);
        }

        @Override
        public Setting[] newArray(int size) {
            return new Setting[size];
        }
    };
}
