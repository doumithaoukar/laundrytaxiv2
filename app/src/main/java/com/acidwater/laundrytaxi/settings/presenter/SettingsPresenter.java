package com.acidwater.laundrytaxi.settings.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.settings.SettingsMvp;
import com.acidwater.laundrytaxi.settings.model.Setting;
import com.acidwater.laundrytaxi.settings.model.SettingsInteractor;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 7/13/2017.
 */
public class SettingsPresenter extends MvpBasePresenter<SettingsMvp.View> implements SettingsMvp.Presenter {


    private Context context;
    private SettingsInteractor settingsInteractor;

    public SettingsPresenter(Context context) {
        this.context = context;
        settingsInteractor = new SettingsInteractor(context);
    }


    @Override
    public void getSettingList() {
        List<Setting> list = settingsInteractor.getSettingList();
        if (isViewAttached()) {
            getView().renderSettingList(list);
        }
    }
}
