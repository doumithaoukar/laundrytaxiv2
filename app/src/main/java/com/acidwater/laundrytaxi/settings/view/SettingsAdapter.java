package com.acidwater.laundrytaxi.settings.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.settings.model.Setting;

import java.util.List;

/**
 * Created by Doumith on 7/8/2017.
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder> {


    private List<Setting> list;
    private Context context;
    private OnItemClickedListener onItemClickListener;

    public SettingsAdapter(Context context, List<Setting> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView name;
        public ImageView arrow;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            image = (ImageView) itemLayoutView.findViewById(R.id.setting_image);
            arrow = (ImageView) itemLayoutView.findViewById(R.id.setting_arrow);
            name = (TextView) itemLayoutView.findViewById(R.id.setting_name);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_setting_list, parent, false);

        return new ViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getName());
        holder.image.setImageResource(list.get(position).getImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClicked(list.get(position));
                }
            }
        });


    }

    public interface OnItemClickedListener {
        void onItemClicked(Setting setting);
    }

    public void setOnItemClickListener(OnItemClickedListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
