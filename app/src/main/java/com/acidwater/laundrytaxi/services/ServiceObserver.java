package com.acidwater.laundrytaxi.services;

import retrofit2.HttpException;

import com.acidwater.laundrytaxi.ApplicationContext;
import com.acidwater.laundrytaxi.data.ClientErrorResponse;
import com.acidwater.laundrytaxi.data.gsondeserializer.GSONDeserializer;
import com.acidwater.laundrytaxi.data.gsondeserializer.GSONDeserializerImpl;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by KamiX on 4/27/16.
 */
public abstract class ServiceObserver<T> extends DisposableObserver<T> {

    @Override
    public abstract void onStart();

    @Override
    public abstract void onComplete();

    @Override
    public void onError(Throwable e) {
        onError(createApplicationException(e));
    }

    @Override
    public abstract void onNext(T t);

    public abstract void onError(LaundryTaxiException e);

    private LaundryTaxiException createApplicationException(Throwable e) {
        LaundryTaxiException exception = null;
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;

            switch (httpException.code()) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 405:
                    GSONDeserializer<ClientErrorResponse> deserializer = new GSONDeserializerImpl<ClientErrorResponse>();
                    Gson gson = new Gson();
                    ClientErrorResponse errorResponse = null;
                    try {
                        errorResponse = deserializer.deserialize(httpException.response().errorBody().string(), ClientErrorResponse.class, gson);
                        String errorMessage = errorResponse.message == null ? "" : errorResponse.message;

                        switch (errorResponse.status) {

                            case 400:
                                // not logged in
                                exception = new LaundryTaxiException(LaundryTaxiException.Reason.CONNECTION_ERROR, errorMessage);
                                GeneralUtils.logout(ApplicationContext.getAppContext());
                                break;
                            case 401:
                                exception = new LaundryTaxiException(LaundryTaxiException.Reason.ALREADY_LOGIN, errorMessage);
                                break;

                            default:
                                exception = new LaundryTaxiException(LaundryTaxiException.Reason.BAD_REQUEST, errorMessage);

                                break;
                        }

                    } catch (IOException e1) {
                        e1.printStackTrace();
                        exception = new LaundryTaxiException(LaundryTaxiException.Reason.NOT_FOUND, "Server Error");
                    } catch (JsonSyntaxException js) {
                        exception = new LaundryTaxiException(LaundryTaxiException.Reason.SERVICE_ERROR, "Server Error");
                    }
                    break;

                default:
                    exception = new LaundryTaxiException(LaundryTaxiException.Reason.SERVICE_ERROR, "Server Error");
                    break;
            }
        } else {
            exception = new LaundryTaxiException(LaundryTaxiException.Reason.CONNECTION_ERROR, "Server Error");
        }
        return exception;
    }
}