package com.acidwater.laundrytaxi.termsandconditions.view;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.about.presenter.AboutPresenter;
import com.acidwater.laundrytaxi.contact.ContactMvp;
import com.acidwater.laundrytaxi.contact.presenter.ContactUsPresenter;
import com.acidwater.laundrytaxi.termsandconditions.TermsMvp;
import com.acidwater.laundrytaxi.termsandconditions.presenter.TermsPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class TermsFragment extends MvpFragment<TermsMvp.View, TermsMvp.Presenter> implements View.OnClickListener, TermsMvp.View {


    private MainActivityListener mListener;
    private WebView webView;


    public static TermsFragment newInstance() {
        TermsFragment termsFragment = new TermsFragment();
        return termsFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
     }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_terms, container, false);

        webView = (WebView) mView.findViewById(R.id.webview);
        webView.loadUrl(ConstantStrings.Base_URL + "" + ConstantStrings.SERVICE_TERMS+ "?lang="+ GeneralUtils.getSharedPref(getContext(),ConstantStrings.APP_LANGUAGE));
        webView.setBackgroundColor(Color.TRANSPARENT);
        return mView;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public TermsMvp.Presenter createPresenter() {
        return new TermsPresenter(getContext());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }
}