package com.acidwater.laundrytaxi.termsandconditions.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.termsandconditions.TermsMvp;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class TermsPresenter extends MvpBasePresenter<TermsMvp.View> implements TermsMvp.Presenter {

    private Context context;

    public TermsPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }


}
