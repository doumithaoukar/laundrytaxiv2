package com.acidwater.laundrytaxi.utils;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RxUtils
{

   public static void disposeIfNotNull(Disposable disposable)
   {
      if(disposable != null && !disposable.isDisposed())
      {
         disposable.dispose();
      }
   }

   public static CompositeDisposable getNewCompositeDisposableIfDisposed(CompositeDisposable disposable)
   {
      if(disposable == null || disposable.isDisposed())
      {
         return new CompositeDisposable();
      }

      return disposable;
   }
}