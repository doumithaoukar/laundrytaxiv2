package com.acidwater.laundrytaxi.utils;

/**
 * Created by Doumith on 7/11/2017.
 */
public class LaundryTaxiException extends Exception {


    private static final long serialVersionUID = 1L;
    public Reason reason;
    public String errorMessage;

    public LaundryTaxiException(Reason reason) {
        super();
        this.reason = reason;

    }

    public LaundryTaxiException(Reason reason, String errorMessage) {
        super();
        this.reason = reason;
        this.errorMessage = errorMessage;
    }

    public enum Reason {

         CONNECTION_ERROR, SERVICE_ERROR, NOT_FOUND, BAD_REQUEST,ALREADY_LOGIN
    }
}
