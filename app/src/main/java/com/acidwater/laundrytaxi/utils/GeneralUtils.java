package com.acidwater.laundrytaxi.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.Display;

import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.data.order.OrderItem;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Doumith on 7/11/2017.
 */
public class GeneralUtils {


    public static final String LAUNDRY_SHARED_PREF = "laundry_taxi_shared_pref";

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void saveUserProfile(Context context, UserWrapper userWrapper) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userWrapper.getUser());
        prefsEditor.putString("user", json);
        prefsEditor.commit();

    }

    public static void saveUserProfile(Context context, User user) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();

    }

    public static User getUserProfile(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("user", "");
        User obj = gson.fromJson(json, User.class);
        return obj;
    }

    public static void saveAppConfig(Context context, AppConfig appConfig) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(appConfig);
        prefsEditor.putString("appConfig", json);
        prefsEditor.commit();

    }

    public static void updateRewardPoints(Context context, int rewardPoints) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("user", "");
        User obj = gson.fromJson(json, User.class);
        obj.setRewardPoints(String.valueOf(rewardPoints));
        saveUserProfile(context, obj);
    }

    public static AppConfig getAppConfig(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("appConfig", "");
        AppConfig obj = gson.fromJson(json, AppConfig.class);
        return obj;
    }

    public static void saveOrderItems(Context context, ArrayList<OrderItem> orderItems) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(orderItems);
        prefsEditor.putString("orderItems", json);
        prefsEditor.commit();

    }

    public static ArrayList<OrderItem> getOrderItems(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("orderItems", "");
        Type type = new TypeToken<List<OrderItem>>() {
        }.getType();
        ArrayList<OrderItem> obj = gson.fromJson(json, type);
        if (obj == null) {
            return new ArrayList<OrderItem>();
        }
        return obj;

    }

    public static void clearOrderItems(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("orderItems", "");
        prefsEditor.commit();

    }

    public static void updateOrderItems(Context context, CategoryItem categoryItem, int quantity) {
        boolean isNew = false;
        ArrayList<OrderItem> orderItems = getOrderItems(context);
        for (OrderItem orderItem : orderItems) {
            if (String.valueOf(orderItem.getId()).equals(categoryItem.getItemID())) {
                isNew = true;
                orderItem.setQuantity(quantity);
                orderItem.setPrice(categoryItem.getPrice());
            }
        }
        if (isNew == false) {
            OrderItem orderItem = new OrderItem();
            orderItem.setId(Integer.valueOf(categoryItem.getItemID()));
            orderItem.setQuantity(quantity);
            orderItem.setPrice(categoryItem.getPrice());
            orderItems.add(orderItem);
        }

        saveOrderItems(context, orderItems);
        MainBus.getInstance().send(new UpdateTotal(orderItems));

    }

    public static void saveMyOrder(Context context, MyOrder myOrder) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(myOrder);
        prefsEditor.putString("myOrder", json);
        prefsEditor.commit();

    }

    public static MyOrder getMyOrder(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("myOrder", "");
        MyOrder obj = gson.fromJson(json, MyOrder.class);
        return obj;
    }

    public static void setSharedPref(Context context, String key, String value) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();

    }

    public static String getSharedPref(Context context, String key) {
        SharedPreferences mPrefs = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        String pref = mPrefs.getString(key, "");
        return pref;
    }


    public static void logout(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        cookieJar.clear();

        SharedPreferences preferences = context.getSharedPreferences(LAUNDRY_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        Intent intent = new Intent(context, IntroActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

    }


    public static long getMidnight(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp * 1000);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long tts = calendar.getTimeInMillis();
        return tts / 1000;
    }


    public static int getHourAmPm(int hourOfDay) {
        if (hourOfDay > 12) {
            return hourOfDay - 12;
        } else if (hourOfDay == 0) {

            return 12;

        } else {
            return hourOfDay;
        }
    }

    public static String isAmOrPm(int hourOfDay) {
        if (hourOfDay >= 12) {
            return "pm";
        } else {
            return "am";
        }
    }

    public static int getScreenHeight(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getScreenWidth(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public static String getDefaultAppURL(Context context) {

        return "https://play.google.com/store/apps/details?id=" + context.getPackageName();
    }

    public static boolean isMoreThanOneHour(long d1, long d2) {
        return Math.abs(d2 - d1) > 3600 * 1000;
    }


    public static boolean isPickupDropOffTimeEditable(long orderTime) {
        orderTime *= 1000;
        return orderTime > System.currentTimeMillis() && isMoreThanOneHour(System.currentTimeMillis(), orderTime);


    }


    public static String getDate(long time) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time * 1000L);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(d);
    }

    public static String getTime(long time) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("hh:mm aa", cal).toString();
        return date;
    }

}
