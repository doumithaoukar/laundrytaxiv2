package com.acidwater.laundrytaxi.utils.date;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.acidwater.laundrytaxi.R;

public class DateTimeDialog extends Dialog {

    private TimePicker mTime;
    private DatePicker mDate;

    public DateTimeDialog(Context context) {
        super(context);
        setContentView(R.layout.date_time_picker);

        mTime = (TimePicker) findViewById(R.id.timePicker);
        mDate = (DatePicker) findViewById(R.id.datePicker);

    }


    public void setTimeListener(OnTimeChangedListener time) {
        mTime.setOnTimeChangedListener(time);
    }

    public void setDateListener(int year, int monthOfYear, int dayOfMonth, OnDateChangedListener date) {
        mDate.init(year, monthOfYear, dayOfMonth, date);
    }
}
