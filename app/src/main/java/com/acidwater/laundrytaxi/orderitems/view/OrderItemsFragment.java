package com.acidwater.laundrytaxi.orderitems.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.data.adapters.BasePagerAdapter;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.data.order.OrderItem;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.orderitems.OrderItemMvp;
import com.acidwater.laundrytaxi.orderitems.orderItemDetails.view.OrderItemDetailsFragment;
import com.acidwater.laundrytaxi.orderitems.presenter.OrderItemPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategory;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderItemsFragment extends MvpLceFragment<View, Catalog, OrderItemMvp.View, OrderItemMvp.Presenter> implements OrderItemMvp.View, View.OnClickListener {


    private MainActivityListener mListener;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Button orderNow;
    private RelativeLayout progressBar;
    private MyOrder myOrder;
    private TextView pickupDate;
    private TextView dropOffDate;
    private TextView pickupTime;
    private TextView dropOffTime;

    private ImageView pickupDateArrow;
    private ImageView pickupTimeArrow;
    private ImageView dropOffDateArrow;
    private ImageView dropOffTimeArrow;

    private TextView totalValue;
    private int grandTotalValue;
    private AppConfig appConfig;
    private CompositeDisposable compositeDisposable;

    private Timepoint[] pickupTimePoints;
    private Timepoint[] dropOffTimePoints;
    private DeliveryMethod deliveryMethod;

    private double minOrder;

    private TextView rewardPointsMessage;
    private SwitchCompat rewardPointsSwitch;

    private OrderHistory mOrderHistory;


    public static OrderItemsFragment newInstance(Timepoint[] pickupTimePoints, Timepoint[] dropOffTimePoints,
                                                 DeliveryMethod deliveryMethod, OrderHistory orderHistory, double minOrder) {
        OrderItemsFragment orderItemsFragment = new OrderItemsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArray("pickupTimePoints", pickupTimePoints);
        bundle.putParcelableArray("dropOffTimePoints", dropOffTimePoints);
        bundle.putParcelable("selectedDeliveryMethod", deliveryMethod);
        bundle.putDouble("minOrder", minOrder);
        if (orderHistory != null) {
            bundle.putParcelable("orderHistory", orderHistory);
        }
        orderItemsFragment.setArguments(bundle);
        return orderItemsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                pickupTimePoints = (Timepoint[]) getArguments().getParcelableArray("pickupTimePoints");
                dropOffTimePoints = (Timepoint[]) getArguments().getParcelableArray("dropOffTimePoints");
                deliveryMethod = (DeliveryMethod) getArguments().getParcelable("selectedDeliveryMethod");
                mOrderHistory = getArguments().getParcelable("orderHistory");
                minOrder = getArguments().getDouble("minOrder");

            }
        } else {
            pickupTimePoints = (Timepoint[]) savedInstanceState.getParcelableArray("pickupTimePoints");
            dropOffTimePoints = (Timepoint[]) savedInstanceState.getParcelableArray("dropOffTimePoints");
            deliveryMethod = (DeliveryMethod) savedInstanceState.getParcelable("selectedDeliveryMethod");
            mOrderHistory = savedInstanceState.getParcelable("orderHistory");
            minOrder = savedInstanceState.getDouble("minOrder");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArray("pickupTimePoints", pickupTimePoints);
        outState.putParcelableArray("dropOffTimePoints", dropOffTimePoints);
        outState.putParcelable("selectedDeliveryMethod", deliveryMethod);
        outState.putParcelable("orderHistory", mOrderHistory);
        outState.putDouble("minOrder", minOrder);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        RxUtils.disposeIfNotNull(compositeDisposable);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setPickupDate(myOrder.getPickUpTimeStamp());
        presenter.setDropOffDate(myOrder.getDropOffTimeStamp());
        presenter.setPickupTime(myOrder.getPickUpTimeStamp());
        presenter.setDropOffTime(myOrder.getDropOffTimeStamp());
        loadData(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_order_items, container, false);
        pickupDate = (TextView) mView.findViewById(R.id.pickup_date);
        dropOffDate = (TextView) mView.findViewById(R.id.drop_off_date);
        pickupTime = (TextView) mView.findViewById(R.id.pickup_time);
        dropOffTime = (TextView) mView.findViewById(R.id.drop_off_time);

        pickupDateArrow = (ImageView) mView.findViewById(R.id.pickup_date_arrow);
        pickupTimeArrow = (ImageView) mView.findViewById(R.id.pickup_time_arrow);
        dropOffDateArrow = (ImageView) mView.findViewById(R.id.drop_off_date_arrow);
        dropOffTimeArrow = (ImageView) mView.findViewById(R.id.drop_off_time_arrow);

        totalValue = (TextView) mView.findViewById(R.id.total);

        pickupDateArrow.setOnClickListener(this);
        pickupTimeArrow.setOnClickListener(this);
        dropOffDateArrow.setOnClickListener(this);
        dropOffTimeArrow.setOnClickListener(this);

        viewPager = (ViewPager) mView.findViewById(R.id.pager);
        tabLayout = (TabLayout) mView.findViewById(R.id.tabLayout);
        orderNow = (Button) mView.findViewById(R.id.btn_order_now);
        orderNow.setOnClickListener(this);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        myOrder = GeneralUtils.getMyOrder(getContext());

        rewardPointsMessage = (TextView) mView.findViewById(R.id.reward_points_message);
        rewardPointsSwitch = (SwitchCompat) mView.findViewById(R.id.reward_points_switch);

        pickupDate.setOnClickListener(this);
        pickupTime.setOnClickListener(this);

        dropOffDate.setOnClickListener(this);
        dropOffTime.setOnClickListener(this);
        this.appConfig = GeneralUtils.getAppConfig(getContext());
        compositeDisposable = new CompositeDisposable();
        registerToBus();
        totalValue.setText("0" + "  " + appConfig.getCurrency());
        return mView;
    }

    private void fitTabLayout() {
        ViewTreeObserver vto = tabLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                tabLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int tabWidth = tabLayout.getMeasuredWidth();
                if (tabWidth <= GeneralUtils.getScreenWidth(getActivity()) - 100) {
                    tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                    tabLayout.setTabMode(TabLayout.MODE_FIXED);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    tabLayout.setLayoutParams(layoutParams);
                } else {
                    tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                }
            }

        });
    }

    @Override
    public OrderItemMvp.Presenter createPresenter() {
        return new OrderItemPresenter(getContext());
    }

    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void HideLoadingView() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setResultSuccessful(String orderID) {
        MyOrder order = GeneralUtils.getMyOrder(getContext());
        order.setOrderID(orderID);
        GeneralUtils.saveMyOrder(getContext(), order);
        mListener.navigationToScreen(ConstantStrings.ORDER_CONFIRMATION, mOrderHistory);
    }

    @Override
    public void setPickupDate(String dateString) {
        pickupDate.setText(dateString);
    }

    @Override
    public void setDropOffDate(String dateString) {

        dropOffDate.setText(dateString);
    }


    private void registerToBus() {
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof UpdateTotal) {

                    UpdateTotal updateTotal = (UpdateTotal) object;
                    presenter.updateTotal(updateTotal, deliveryMethod, rewardPointsSwitch.isChecked());
                }

            }

        }));
    }

    @Override
    public void setPickupTime(String timeString) {
        pickupTime.setText(timeString);
    }

    @Override
    public void setDropOffTime(String timeString) {
        dropOffTime.setText(timeString);
    }

    @Override
    public void setRewardPointMessage(String message, boolean enableRewardPoint) {

        rewardPointsMessage.setText(message);
        rewardPointsSwitch.setEnabled(enableRewardPoint);
    }

    @Override
    public void setTotal(String total) {
        totalValue.setText(total);
    }

    @Override
    public void updateTotal(int total) {
        this.grandTotalValue = total;
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(Catalog data) {
        BasePagerAdapter adapter = new BasePagerAdapter(getChildFragmentManager());

        if (mOrderHistory != null) {
            GeneralUtils.saveOrderItems(getContext(), mOrderHistory.getOrderItems());
            myOrder.setOrderID(mOrderHistory.getOrderID());
        }
        for (CatalogueCategory catalogueCategory : data.getCategories()) {

            if (mOrderHistory != null) {
                for (OrderItem orderHistory : mOrderHistory.getOrderItems()) {
                    for (CategoryItem categoryItem : catalogueCategory.getCategoryItems()) {
                        if (String.valueOf(orderHistory.getId()).equals(categoryItem.getItemID())) {
                            categoryItem.setOrderQuantity(orderHistory.getQuantity());
                        }
                    }
                }
            }
            String categoryName;
            if (GeneralUtils.getSharedPref(getContext(), ConstantStrings.APP_LANGUAGE).equals("") ||
                    GeneralUtils.getSharedPref(getContext(), ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
                categoryName = catalogueCategory.getNameEn();
            } else {
                categoryName = catalogueCategory.getNameAr();
            }
            adapter.addFragment(OrderItemDetailsFragment.newInstance(catalogueCategory, deliveryMethod, mOrderHistory), categoryName);
        }
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setOffscreenPageLimit(data.getCategories().size());
        fitTabLayout();
        presenter.getRewardPointMessage();
        rewardPointsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MainBus.getInstance().send(new UpdateTotal(GeneralUtils.getOrderItems(getContext())));
            }
        });
        if (mOrderHistory != null) {
            MainBus.getInstance().send(new UpdateTotal(mOrderHistory.getOrderItems()));
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getCatalog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_order_now:
                ArrayList<OrderItem> orderItems = GeneralUtils.getOrderItems(getContext());

                //check if quantity is empty for all items
                boolean orderEmptyItems = true;
                for (OrderItem orderItem : orderItems) {
                    if (orderItem.getQuantity() > 0) {
                        orderEmptyItems = false;
                        break;
                    }
                }
                if (orderItems.isEmpty() || orderEmptyItems) {
                    Toast.makeText(getContext(), getString(R.string.enter_order_items), Toast.LENGTH_SHORT).show();
                }
                if (grandTotalValue < minOrder && !rewardPointsSwitch.isChecked()) {
                    Toast.makeText(getContext(), getString(R.string.min_order_is) + " " + (int) minOrder + "" + appConfig.getCurrency(), Toast.LENGTH_SHORT).show();

                } else {

                    myOrder.setOrderItems(orderItems);
                    if (mOrderHistory != null) {
                        presenter.placeOrder(myOrder, rewardPointsSwitch.isChecked(), true);
                    } else {
                        presenter.placeOrder(myOrder, rewardPointsSwitch.isChecked(), false);
                    }
                }
                break;

            case R.id.pickup_date:
            case R.id.pickup_date_arrow:
                break;

            case R.id.pickup_time:
            case R.id.pickup_time_arrow:
                break;

            case R.id.drop_off_date:
            case R.id.drop_off_date_arrow:
                break;

            case R.id.drop_off_time:
            case R.id.drop_off_time_arrow:
                break;
        }
    }


}
