package com.acidwater.laundrytaxi.orderitems.orderItemDetails.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.orderitems.orderItemDetails.OrderItemDetailsMvp;
import com.acidwater.laundrytaxi.orderitems.orderItemDetails.presenter.OrderItemDetailsPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategory;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderItemDetailsFragment extends MvpFragment<OrderItemDetailsMvp.View, OrderItemDetailsMvp.Presenter> implements OrderItemDetailsMvp.View {


    private MainActivityListener mListener;
    private CatalogueCategory category;
    private static final String CATEGORY = "category";
    private static final String DELIVERY = "DeliveryMethod";

    private RecyclerView recyclerView;
    private ArrayList<CategoryItem> categoryItems;
    private MyOrder myOrder;
    private DeliveryMethod deliveryMethod;
    private OrderHistory mOrderHistory;

    public static OrderItemDetailsFragment newInstance(CatalogueCategory catalogueCategory, DeliveryMethod deliveryMethod, OrderHistory orderHistory) {
        OrderItemDetailsFragment orderItemDetailsFragment = new OrderItemDetailsFragment();
        Bundle b = new Bundle();
        b.putParcelable(CATEGORY, catalogueCategory);
        b.putParcelable(DELIVERY, deliveryMethod);
        orderItemDetailsFragment.setArguments(b);
        if (orderHistory != null) {
            b.putParcelable("orderHistory", orderHistory);
        }
        return orderItemDetailsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_order_item_details, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (savedInstanceState != null) {
            categoryItems = savedInstanceState.getParcelableArrayList("categoryItems");
        }

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            category = savedInstanceState.getParcelable(CATEGORY);
            deliveryMethod = savedInstanceState.getParcelable(DELIVERY);
            mOrderHistory = savedInstanceState.getParcelable("orderHistory");
        } else {
            if (getArguments() != null) {
                category = getArguments().getParcelable(CATEGORY);
                deliveryMethod = getArguments().getParcelable(DELIVERY);
                mOrderHistory = getArguments().getParcelable("orderHistory");
            }
        }

        setData(category);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (category != null) {
            outState.putParcelable(CATEGORY, category);
        }
        if (categoryItems != null) {
            outState.putParcelableArrayList("categoryItems", categoryItems);
        }
        if (deliveryMethod != null) {
            outState.putParcelable(DELIVERY, deliveryMethod);
        }

        if (mOrderHistory != null) {
            outState.putParcelable("orderHistory", mOrderHistory);
        }
    }

    @Override
    public OrderItemDetailsMvp.Presenter createPresenter() {
        return new OrderItemDetailsPresenter(getContext());
    }

    @Override
    public void setError(String message) {

    }

    public void setData(CatalogueCategory data) {
        categoryItems = data.getCategoryItems();
        //clearing old data
//        GeneralUtils.clearOrderItems(getContext());
        boolean isItemsEditable = true;
        if (mOrderHistory != null) {
            isItemsEditable = GeneralUtils.isPickupDropOffTimeEditable(mOrderHistory.getPickupTimeStamp());
        }
        OrderItemsAdapter adapter = new OrderItemsAdapter(getContext(), categoryItems, deliveryMethod, isItemsEditable);
        recyclerView.setAdapter(adapter);

    }


}
