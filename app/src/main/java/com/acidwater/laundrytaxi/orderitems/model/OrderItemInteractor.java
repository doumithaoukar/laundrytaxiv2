package com.acidwater.laundrytaxi.orderitems.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.myorders.OrdersMvp;
import com.acidwater.laundrytaxi.orderitems.OrderItemMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Doumith on 8/6/2017.
 */
public class OrderItemInteractor implements OrderItemMvp.Interactor {

    public PlaceOrderApi placeOrderApi;

    public OrderItemInteractor(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        placeOrderApi = retrofit.create(PlaceOrderApi.class);
    }


    @Override
    public Observable<OrderResult> placeOrder(MyOrder myOrder, boolean useRewardPoints, boolean isEdit) {
        Observable<OrderResult> observable = null;
        String URL = ConstantStrings.Base_URL.endsWith("/") ? ConstantStrings.Base_URL : ConstantStrings.Base_URL.concat("/");
        if (useRewardPoints) {
            URL += ConstantStrings.SERVICE_PLACE_ORDER + "?useRewardPoints=1";
        } else {
            URL += ConstantStrings.SERVICE_PLACE_ORDER + "/";
        }

        if (isEdit) {
            if (URL.contains("?useRewardPoints=1")) {
                URL += "&orderId=" + myOrder.getOrderID();
            } else {
                URL += "?orderId=" + myOrder.getOrderID();
            }
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (myOrder.toJson()).toString());
        observable =
                placeOrderApi.placeOrder(URL, requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface PlaceOrderApi {
        @Headers("Content-Type: application/json")
        @POST
        Observable<OrderResult> placeOrder(@Url String url, @Body RequestBody body);
    }

}
