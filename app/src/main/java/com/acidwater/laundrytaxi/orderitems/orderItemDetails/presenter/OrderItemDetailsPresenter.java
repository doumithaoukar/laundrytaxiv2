package com.acidwater.laundrytaxi.orderitems.orderItemDetails.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.orderitems.OrderItemMvp;
import com.acidwater.laundrytaxi.orderitems.orderItemDetails.OrderItemDetailsMvp;
import com.acidwater.laundrytaxi.orderitems.presenter.OrderItemPresenter;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.acidwater.laundrytaxi.pricelist.model.PriceListInteractor;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class OrderItemDetailsPresenter extends MvpBasePresenter<OrderItemDetailsMvp.View> implements OrderItemDetailsMvp.Presenter {


    private Context context;


    public OrderItemDetailsPresenter(Context context) {
        this.context = context;

    }
    
}
