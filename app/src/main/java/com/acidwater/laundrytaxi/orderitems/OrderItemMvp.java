package com.acidwater.laundrytaxi.orderitems;

import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.orderitems.model.OrderResult;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategory;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.Observable;


/**
 * Created by Doumith on 8/2/2017.
 */
public interface OrderItemMvp {

    interface Interactor {

        Observable<OrderResult> placeOrder(MyOrder myOrder, boolean useRewardPoints, boolean isEdit);
    }

    interface Presenter extends MvpPresenter<View> {

        void getCatalog();

        void placeOrder(MyOrder myOrder, boolean useRewardPoints, boolean isEdit);

        void setPickupDate(long timeStamp);

        void setDropOffDate(long timeStamp);

        void setPickupTime(long timeStamp);

        void setDropOffTime(long timeStamp);

        void getRewardPointMessage();

        void updateTotal(UpdateTotal updateTotal, DeliveryMethod deliveryMethod, boolean useRewardPoints);


    }

    interface View extends MvpLceView<Catalog> {

        void setError(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful(String orderID);

        void setPickupDate(String dateString);

        void setDropOffDate(String dateString);

        void setPickupTime(String timeString);

        void setDropOffTime(String timeString);

        void setRewardPointMessage(String message, boolean enableRewardPoint);

        void setTotal(String total);

        void updateTotal(int total);
    }
}
