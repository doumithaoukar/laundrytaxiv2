package com.acidwater.laundrytaxi.orderitems.orderItemDetails.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<CategoryItem> list;
    private Context context;

    private final static int TYPE_HEADER = 0;
    private final static int TYPE_ITEM = 1;
    private AppConfig appConfig;
    private LayoutInflater inflater;
    private DeliveryMethod deliveryMethod;
    private boolean isEditable;

    public OrderItemsAdapter(Context context, List<CategoryItem> list, DeliveryMethod deliveryMethod, boolean isEditable) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);
        this.appConfig = GeneralUtils.getAppConfig(context);
        this.deliveryMethod = deliveryMethod;
        this.isEditable = isEditable;
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView items;
        public TextView currency;


        public HeaderViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            items = (TextView) itemLayoutView.findViewById(R.id.items);
            currency = (TextView) itemLayoutView.findViewById(R.id.currency);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView price;
        public ImageView image;
        public TextView quantity;
        public ImageButton addQuantity;
        public ImageButton removeQuantity;


        public ItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            price = (TextView) itemLayoutView.findViewById(R.id.price);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            image = (ImageView) itemLayoutView.findViewById(R.id.image);
            quantity = (TextView) itemLayoutView.findViewById(R.id.order_quantity);
            addQuantity = (ImageButton) itemLayoutView.findViewById(R.id.add_order_item);
            removeQuantity = (ImageButton) itemLayoutView.findViewById(R.id.remove_order_item);

            if (isEditable) {
                addQuantity.setEnabled(true);
            } else {
                removeQuantity.setEnabled(false);
            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case TYPE_HEADER:
                viewHolder = new HeaderViewHolder(
                        inflater.inflate(R.layout.row_category_item_header, parent, false));

                break;

            case TYPE_ITEM:

                viewHolder = new ItemViewHolder(
                        inflater.inflate(R.layout.row_order_item, parent, false));
                break;
        }

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int pos) {


        switch (holder.getItemViewType()) {

            case TYPE_HEADER:
                break;

            case TYPE_ITEM:
                final int position = pos;
                final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                String itemName;
                if (GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals("") ||
                        GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
                    itemName = list.get(position).getNameEn();
                } else {
                    itemName = list.get(position).getNameAr();
                }
                itemViewHolder.name.setText(itemName);
                double itemPrice = Double.parseDouble(list.get(position).getPrice()) * deliveryMethod.getMultiply();
                itemViewHolder.price.setText(String.format(Locale.ENGLISH, "%.0f", itemPrice) + " " + appConfig.getCurrency());
                if (!TextUtils.isEmpty(list.get(position).getImage())) {
                    Picasso.with(context).load(list.get(position).getImage()).into(itemViewHolder.image);
                }
                itemViewHolder.quantity.setText(String.valueOf(list.get(pos).getOrderQuantity()));
                itemViewHolder.addQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(position).setOrderQuantity(list.get(position).getOrderQuantity() + 1);
                        itemViewHolder.quantity.setText(String.valueOf(list.get(pos).getOrderQuantity()));
                        GeneralUtils.updateOrderItems(context, list.get(position), list.get(position).getOrderQuantity());
                    }
                });
                itemViewHolder.removeQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (list.get(position).getOrderQuantity() > 0) {
                            list.get(position).setOrderQuantity(list.get(position).getOrderQuantity() - 1);
                            itemViewHolder.quantity.setText(String.valueOf(list.get(pos).getOrderQuantity()));
                            GeneralUtils.updateOrderItems(context, list.get(position), list.get(position).getOrderQuantity());
                        }
                    }
                });
                break;
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (position == 0)
//            return TYPE_HEADER;

        return TYPE_ITEM;
    }
}
