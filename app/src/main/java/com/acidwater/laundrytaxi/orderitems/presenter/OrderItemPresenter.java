package com.acidwater.laundrytaxi.orderitems.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.data.order.OrderItem;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.orderitems.OrderItemMvp;
import com.acidwater.laundrytaxi.orderitems.model.OrderItemInteractor;
import com.acidwater.laundrytaxi.orderitems.model.OrderResult;
import com.acidwater.laundrytaxi.pricelist.model.Catalog;
import com.acidwater.laundrytaxi.pricelist.model.PriceListInteractor;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.Calendar;
import java.util.Locale;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class OrderItemPresenter extends MvpBasePresenter<OrderItemMvp.View> implements OrderItemMvp.Presenter {


    private Context context;
    private PriceListInteractor priceListInteractor;
    private CompositeDisposable compositeDisposable;
    private OrderItemInteractor orderItemInteractor;
    public static final int MIN_REWARD_POINTS_REQUIRED = 100;

    public OrderItemPresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        priceListInteractor = new PriceListInteractor(context);
        orderItemInteractor = new OrderItemInteractor(context);
    }


    @Override
    public void getCatalog() {
        compositeDisposable.add(priceListInteractor.getCatalog().subscribeWith(new ServiceObserver<Catalog>() {
            @Override
            public void onStart() {

                if (isViewAttached()) {
                    getView().showLoading(false);
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(Catalog catalog) {
                if (isViewAttached()) {
                    getView().setData(catalog);
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }


    @Override
    public void placeOrder(MyOrder myOrder, boolean useRewardPoints, boolean isEdit) {
        compositeDisposable.add(orderItemInteractor.placeOrder(myOrder, useRewardPoints, isEdit).subscribeWith(new ServiceObserver<OrderResult>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(OrderResult serverResponse) {
                GeneralUtils.updateRewardPoints(context, serverResponse.getNewRewardPointsCount());
                if (isViewAttached()) {
                    getView().setResultSuccessful(serverResponse.getOrderID());
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {

                    getView().setError(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void setPickupDate(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        String shortMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
        String dayOfMonth = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        String year = String.format("%02d", calendar.get(Calendar.YEAR)).substring(2);

        String fullDate = dayOfMonth + " " + shortMonthName + " " + year;
        if (isViewAttached()) {
            getView().setPickupDate(fullDate);
        }
    }

    @Override
    public void setDropOffDate(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        String shortMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
        String dayOfMonth = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        String year = String.format("%02d", calendar.get(Calendar.YEAR)).substring(2);

        String fullDate = dayOfMonth + " " + shortMonthName + " " + year;
        if (isViewAttached()) {
            getView().setDropOffDate(fullDate);
        }
    }

    @Override
    public void setPickupTime(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        String hour = String.format("%02d", GeneralUtils.getHourAmPm(calendar.get(Calendar.HOUR_OF_DAY)));
        String minute = String.format("%02d", calendar.get(Calendar.MINUTE));
        String amPm = GeneralUtils.isAmOrPm(calendar.get(Calendar.HOUR_OF_DAY));

        String fullTime = hour + ":" + minute + " " + amPm;
        if (isViewAttached()) {
            getView().setPickupTime(fullTime);
        }
    }

    @Override
    public void setDropOffTime(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        String hour = String.format("%02d", GeneralUtils.getHourAmPm(calendar.get(Calendar.HOUR_OF_DAY)));
        String minute = String.format("%02d", calendar.get(Calendar.MINUTE));
        String amPm = GeneralUtils.isAmOrPm(calendar.get(Calendar.HOUR_OF_DAY));

        String fullTime = hour + ":" + minute + " " + amPm;
        if (isViewAttached()) {
            getView().setDropOffTime(fullTime);
        }
    }

    @Override
    public void getRewardPointMessage() {
        User user = GeneralUtils.getUserProfile(context);
        String message;

        boolean enableRewardPoints = false;
        if (Integer.parseInt(user.getRewardPoints()) <= MIN_REWARD_POINTS_REQUIRED) {
            enableRewardPoints = false;
            message = context.getString(R.string.cant_use_reward_points);
        } else {
            enableRewardPoints = true;
            message = context.getString(R.string.use_reward_points) + " " + user.getRewardPoints() + " " + context.getString(R.string.use_reward_points_2);
        }

        if (isViewAttached()) {
            getView().setRewardPointMessage(message, enableRewardPoints);
        }
    }

    @Override
    public void updateTotal(UpdateTotal updateTotal, DeliveryMethod deliveryMethod, boolean useRewardPoints) {
        double total = 0;
        AppConfig appConfig = GeneralUtils.getAppConfig(context);
        int rewardPoints = Integer.parseInt(GeneralUtils.getUserProfile(context).getRewardPoints());
        for (int i = 0; i < updateTotal.orderItems.size(); i++) {
            OrderItem orderItem = updateTotal.orderItems.get(i);
            if (!TextUtils.isEmpty(orderItem.getPrice())) {
                double itemPrice = Double.parseDouble(orderItem.getPrice());
                if (deliveryMethod != null) {
                    itemPrice = deliveryMethod.getMultiply() * itemPrice;
                }
                total += itemPrice * orderItem.getQuantity();
            }
        }

        if (useRewardPoints) {

            int discountDivider = rewardPoints / 100;
            double discount_amount = appConfig.getDiscountPer100RewardPoint() * discountDivider;
            if (discount_amount > total) {
                total = 0;
            } else {
                total = total - discount_amount;
            }
        }


        if (isViewAttached()) {
            getView().updateTotal((int) total);
            getView().setTotal(String.format(Locale.ENGLISH, "%.0f", total) + "  " + appConfig.getCurrency());
        }

    }
}
