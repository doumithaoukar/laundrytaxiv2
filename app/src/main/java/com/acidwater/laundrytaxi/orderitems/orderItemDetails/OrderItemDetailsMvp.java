package com.acidwater.laundrytaxi.orderitems.orderItemDetails;

import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

/**
 * Created by Doumith on 8/2/2017.
 */
public interface OrderItemDetailsMvp {


    interface Presenter extends MvpPresenter<View> {

    }

    interface View extends MvpView {

        void setError(String message);
    }
}
