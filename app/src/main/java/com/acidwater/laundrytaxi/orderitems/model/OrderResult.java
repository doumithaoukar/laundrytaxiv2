package com.acidwater.laundrytaxi.orderitems.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 8/6/2017.
 */
public class OrderResult implements Parcelable {
    private int status;
    private String message;


    @SerializedName("NewRewardPointsCount")
    private int newRewardPointsCount;


    @SerializedName("OrderId")
    private String orderID;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public int getNewRewardPointsCount() {
        return newRewardPointsCount;
    }

    public void setNewRewardPointsCount(int newRewardPointsCount) {
        this.newRewardPointsCount = newRewardPointsCount;
    }


    public OrderResult() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.status);
        dest.writeString(this.message);
        dest.writeInt(this.newRewardPointsCount);
        dest.writeString(this.orderID);
    }

    protected OrderResult(Parcel in) {
        this.status = in.readInt();
        this.message = in.readString();
        this.newRewardPointsCount = in.readInt();
        this.orderID = in.readString();
    }

    public static final Creator<OrderResult> CREATOR = new Creator<OrderResult>() {
        @Override
        public OrderResult createFromParcel(Parcel source) {
            return new OrderResult(source);
        }

        @Override
        public OrderResult[] newArray(int size) {
            return new OrderResult[size];
        }
    };
}
