package com.acidwater.laundrytaxi.orderconfirmation.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateHistoryList;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.notifications.presenter.NotificationsPresenter;
import com.acidwater.laundrytaxi.orderconfirmation.OrderConfirmationMvp;
import com.acidwater.laundrytaxi.orderconfirmation.presenter.OrderConfirmationPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderConfirmationFragment extends MvpFragment<OrderConfirmationMvp.View, OrderConfirmationMvp.Presenter> implements View.OnClickListener, OrderConfirmationMvp.View {


    private MainActivityListener mListener;
    private TextView orderId;
    private TextView pickupDate;
    private TextView dropOffDate;
    private Button btnPlaceOrder;
    private TextView goToMyOrders;
    private MyOrder myOrder;

    public static OrderConfirmationFragment newInstance(OrderHistory orderHistory) {
        OrderConfirmationFragment orderConfirmationFragment = new OrderConfirmationFragment();
        if (orderHistory != null) {
            Bundle b = new Bundle();
            b.putParcelable("orderHistory", orderHistory);
            orderConfirmationFragment.setArguments(b);
        }
        return orderConfirmationFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_order_confirmation, container, false);
        orderId = (TextView) mView.findViewById(R.id.order_id);
        goToMyOrders = (TextView) mView.findViewById(R.id.tv_my_orders);
        pickupDate = (TextView) mView.findViewById(R.id.pickup_date);
        dropOffDate = (TextView) mView.findViewById(R.id.drop_off_date);
        btnPlaceOrder = (Button) mView.findViewById(R.id.btn_place_order);

        btnPlaceOrder.setOnClickListener(this);
        goToMyOrders.setOnClickListener(this);

        myOrder = GeneralUtils.getMyOrder(getContext());
        if (getArguments() != null) {
            OrderHistory orderHistory = getArguments().getParcelable("orderHistory");
            if (orderHistory != null) {
                myOrder.setOrderID(orderHistory.getOrderID());
            }
        }

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setOrderID(myOrder);
        presenter.getPickupDateString(myOrder.getPickUpTimeStamp());
        presenter.getDropOffDateString(myOrder.getDropOffTimeStamp());
    }

    @Override
    public OrderConfirmationMvp.Presenter createPresenter() {
        return new OrderConfirmationPresenter(getContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_place_order:
                // clearing order from shared preferences
                GeneralUtils.saveMyOrder(getContext(), new MyOrder());
                MainBus.getInstance().send(new UpdateHistoryList());
                mListener.destroyFragments();
                mListener.getUserProfile();
                break;

            case R.id.tv_my_orders:
                break;
        }
    }

    @Override
    public void setPickUpDateString(String dateString) {
        pickupDate.setText(dateString);
    }

    @Override
    public void setDropOffDateString(String dateString) {
        dropOffDate.setText(dateString);
    }

    @Override
    public void setOrderID(MyOrder order) {
        orderId.setText(getString(R.string.order_id) + " " +order.getOrderID());

    }
}
