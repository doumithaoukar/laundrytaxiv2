package com.acidwater.laundrytaxi.orderconfirmation.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.orderconfirmation.OrderConfirmationMvp;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Doumith on 7/13/2017.
 */
public class OrderConfirmationPresenter extends MvpBasePresenter<OrderConfirmationMvp.View> implements OrderConfirmationMvp.Presenter {


    private Context context;

    public OrderConfirmationPresenter(Context context) {
        this.context = context;
    }


    @Override
    public void getPickupDateString(long timeStamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        String name = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
        String dayOfMonth = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        String month = String.format("%02d", calendar.get(Calendar.MONTH) + 1);
        String year = String.format("%02d", calendar.get(Calendar.YEAR));
        String hour = String.format("%02d", GeneralUtils.getHourAmPm(calendar.get(Calendar.HOUR_OF_DAY)));
        String minute = String.format("%02d", calendar.get(Calendar.MINUTE));

        String fullDate = hour + ":" + minute + " " + GeneralUtils.isAmOrPm(calendar.get(Calendar.HOUR_OF_DAY)) + " " + name + " " + dayOfMonth + "/" + month + "/" + year;
        if (isViewAttached()) {
            getView().setPickUpDateString(fullDate);
        }

    }

    @Override
    public void getDropOffDateString(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp * 1000);
        String name = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
        String dayOfMonth = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        String month = String.format("%02d", calendar.get(Calendar.MONTH) + 1);
        String year = String.format("%02d", calendar.get(Calendar.YEAR));
        String hour = String.format("%02d", GeneralUtils.getHourAmPm(calendar.get(Calendar.HOUR_OF_DAY)));
        String minute = String.format("%02d", calendar.get(Calendar.MINUTE));


        String fullDate = hour + ":" + minute + " " + GeneralUtils.isAmOrPm(calendar.get(Calendar.HOUR_OF_DAY)) + " " + name + " " + dayOfMonth + "/" + month + "/" + year;
        if (isViewAttached()) {
            getView().setDropOffDateString(fullDate);
        }
    }

    @Override
    public void setOrderID(MyOrder myOrder) {
        if (isViewAttached()) {
            getView().setOrderID(myOrder);
        }
    }
}
