package com.acidwater.laundrytaxi.orderconfirmation;

import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Doumith on 7/30/2017.
 */
public interface OrderConfirmationMvp {


    interface Interactor {
    }


    interface Presenter extends MvpPresenter<View> {

        void getPickupDateString(long timeStamp);

        void getDropOffDateString(long timeStamp);

        void setOrderID(MyOrder myOrder);
    }


    interface View extends MvpView {

        void setPickUpDateString(String dateString);

        void setDropOffDateString(String dateString);

        void setOrderID(MyOrder myOrder);

    }
}
