package com.acidwater.laundrytaxi.address.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.address.AddressMvp;
import com.acidwater.laundrytaxi.address.presenter.AddressPresenter;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.AddressEvent;
import com.acidwater.laundrytaxi.data.order.MyOrder;
import com.acidwater.laundrytaxi.data.order.OrderGps;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.map.MapMvp;
import com.acidwater.laundrytaxi.map.presenter.MapPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/3/2017.
 */
public class AddressFragment extends MvpFragment<AddressMvp.View, AddressMvp.Presenter> implements View.OnClickListener, AddressMvp.View, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private TextView pickupLocation;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastKnownLocation;
    float DEFAULT_ZOOM = 14.0f;

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 9002;
    private LinearLayout pickupContainer;


    private MarkerOptions pickupMarker;

    private MainActivityListener mListener;
    private Button btnContinue;
    private RelativeLayout progressBar;


    public static AddressFragment newInstance() {
        AddressFragment mapFragment = new AddressFragment();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_navigation).setIcon(mListener.getNavigationIcon());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_address, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_navigation:
                mListener.openDrawer();
                break;
            case R.id.action_search_address:
                try {
                    AutocompleteFilter lebanonFilter = new AutocompleteFilter.Builder()
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                            .setCountry("LB")
                            .build();

                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setFilter(lebanonFilter)
                                    .build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                break;
        }
        return false;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_address, container, false);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        progressBar = (RelativeLayout) rootView.findViewById(R.id.main_progress);
        setHasOptionsMenu(true);
        pickupLocation = (TextView) rootView.findViewById(R.id.pickup_location);
        btnContinue = (Button) rootView.findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(this);

        pickupContainer = (LinearLayout) rootView.findViewById(R.id.pickup_container);

        pickupContainer.setOnClickListener(this);


        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mListener.initializeMapView();

        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable("lastKnowLocation");
            pickupMarker = savedInstanceState.getParcelable("pickupMarker");
        }

        return rootView;
    }

    public void syncMap() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                setMarkerListener();

                if (mLastKnownLocation != null) {
                    User user = GeneralUtils.getUserProfile(getContext());
                    if (user.getHomeLocation() != null) {
                        if (user.getHomeLocation().getOrderGps() != null) {
                            if (user.getHomeLocation().getOrderGps().getLatitude() != 0
                                    && user.getHomeLocation().getOrderGps().getLongitude() != 0) {
                                setMarker(user.getHomeLocation().getOrderGps().getLatitude(),
                                        user.getHomeLocation().getOrderGps().getLongitude(), true);
                            } else {
                                setMarker(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), true);
                            }
                        } else {
                            setMarker(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), true);
                        }
                    } else {
                        setMarker(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), true);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getContext(),
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            //Location Permission already granted
                            buildGoogleApiClient();
                            mGoogleMap.setMyLocationEnabled(true);
                        } else {
                            //Request Location Permission
                            mListener.initializeMapView();
                        }
                    } else {
                        buildGoogleApiClient();
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } else {
                    buildGoogleApiClient();
                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();

    }

    @Override
    public AddressMvp.Presenter createPresenter() {
        return new AddressPresenter(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    public synchronized void buildGoogleApiClient() {
        progressBar.setVisibility(View.VISIBLE);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            if (mGoogleMap != null) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                mListener.initializeMapView();
            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (getActivity() != null) {


            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                mLastKnownLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);
                progressBar.setVisibility(View.GONE);

                if (mLastKnownLocation != null) {
                    pickUpContainerClicked();
                } else {
                    buildGoogleApiClient();
                }

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                setMarker(place.getLatLng().latitude, place.getLatLng().longitude, true);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private void setMarker(double latitude, double longitude, boolean moveCamera) {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            pickupMarker = marker;
            pickupMarker.draggable(true);
            pickupMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_bus));
            presenter.getPickOffAddress(pickupMarker.getPosition().latitude, pickupMarker.getPosition().longitude);
            mGoogleMap.addMarker(marker);
            if (moveCamera) {
                moveCamera(latitude, longitude);
            }
        }


    }

    @Override
    public void setPickOffAddress(String address) {
        pickupLocation.setText(address);
    }


    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSuccess() {

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);
    }

    private void setMarkerListener() {
        mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                pickupMarker = new MarkerOptions().position(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                presenter.getPickOffAddress(marker.getPosition().latitude, marker.getPosition().longitude);


            }
        });
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mGoogleMap.clear();

                setMarker(latLng.latitude, latLng.longitude, false);

            }
        });
    }

    private void moveCamera(double latitude, double longitude) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(latitude, longitude), DEFAULT_ZOOM));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pickup_container:
                pickUpContainerClicked();
                break;

            case R.id.btn_continue:
                if (pickupMarker != null) {
                    MainBus.getInstance().send(new AddressEvent(pickupLocation.getText().toString(), pickupMarker.getPosition().latitude, pickupMarker.getPosition().longitude));
                    destroyThis();
                } else {
                    Toast.makeText(getContext(), getString(R.string.enter_your_address), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void pickUpContainerClicked() {
        if (pickupMarker == null) {
            if (mLastKnownLocation != null) {
                setMarker(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), true);

            } else {
                buildGoogleApiClient();
            }
        } else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(pickupMarker.getPosition().latitude,
                            pickupMarker.getPosition().longitude), DEFAULT_ZOOM));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }

    }


    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }
}
