package com.acidwater.laundrytaxi.address;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Doumith on 7/8/2017.
 */
public interface AddressMvp {


    interface Interactor {

    }

    interface Presenter extends MvpPresenter<View> {

        void getPickOffAddress(double latitude, double longitude);

    }

    interface View extends MvpView {

        void setPickOffAddress(String address);

        void setError(String message);

        void setSuccess();

        void showLoadingView();

        void hideLoadingView();
    }
}
