package com.acidwater.laundrytaxi.address.presenter;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.address.AddressMvp;
import com.acidwater.laundrytaxi.map.MapMvp;
import com.acidwater.laundrytaxi.map.model.MapInteractor;
import com.acidwater.laundrytaxi.map.model.Result;
import com.acidwater.laundrytaxi.map.model.ResultWrapper;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;
import java.util.Locale;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/11/2017.
 */
public class AddressPresenter extends MvpBasePresenter<AddressMvp.View> implements AddressMvp.Presenter {

    private Context context;
    private CompositeDisposable compositeDisposable;
    StringBuilder mAddress = new StringBuilder();

    public AddressPresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }


    private String getAddress(double latitude, double longitude) {
        boolean error;
        try {
            Geocoder geo = new Geocoder(context, Locale.getDefault());


            if (geo.isPresent()) {
                mAddress = new StringBuilder();
                List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
                if (addresses.isEmpty()) {
                    mAddress.append(context.getString(R.string.waiting_location));
                } else {
                    if (addresses.size() > 0) {

                        if (!TextUtils.isEmpty(addresses.get(0).getFeatureName())) {
                            mAddress.append(addresses.get(0).getFeatureName() + ",");
                        }

                        if (!TextUtils.isEmpty(addresses.get(0).getLocality())) {
                            mAddress.append(addresses.get(0).getLocality() + ",");
                        }

                        if (!TextUtils.isEmpty(addresses.get(0).getAdminArea())) {
                            mAddress.append(addresses.get(0).getAdminArea() + ",");
                        }

                        if (!TextUtils.isEmpty(addresses.get(0).getSubAdminArea())) {
                            mAddress.append(addresses.get(0).getSubAdminArea() + ",");
                        }
                        if (!TextUtils.isEmpty(addresses.get(0).getCountryName())) {
                            mAddress.append(addresses.get(0).getCountryName() + ",");
                        }

                        if (!TextUtils.isEmpty(mAddress.toString()) && mAddress.toString().endsWith(",")) {
                            mAddress.setLength(mAddress.length() - 1);

                        }
                    }
                }
                if (mAddress.toString().trim().isEmpty()) {
                    error = true;
                } else {
                    error = false;
                }
            } else {
                error = true;
            }

        } catch (Exception e) {
            error = true;
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }

        if (error == true) {
            setGoogleApiAddress(latitude, longitude);
        }

        return mAddress.toString();
    }

    private void setGoogleApiAddress(double latitude, double longitude) {
        mAddress = new StringBuilder();
        // try to get location using google api services
        MapInteractor mapInteractor = new MapInteractor(context);
        compositeDisposable.add(mapInteractor.getAddress(latitude, longitude).subscribeWith(new ServiceObserver<ResultWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                }
            }

            @Override
            public void onNext(ResultWrapper resultWrapper) {
                if (resultWrapper != null) {
                    if (resultWrapper.getResults().size() != 0) {
                        Result address1 = resultWrapper.getResults().get(0);
                        if (address1 != null) {
                            for (int i = 0; i < address1.getAddressComponents().size(); i++) {
                                mAddress.append(address1.getAddressComponents().get(i).getLongName() + ",");
                            }
                        }
                    }
                    if (!TextUtils.isEmpty(mAddress.toString()) && mAddress.toString().endsWith(",")) {
                        mAddress.setLength(mAddress.length() - 1);

                    }
                    if (isViewAttached()) {
                        getView().setPickOffAddress(mAddress.toString());
                    }
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                }
            }
        }));
    }

    @Override
    public void getPickOffAddress(double latitude, double longitude) {
        if (isViewAttached()) {
            getView().setPickOffAddress(getAddress(latitude, longitude));
        }
    }


}
