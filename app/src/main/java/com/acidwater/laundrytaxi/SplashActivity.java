package com.acidwater.laundrytaxi;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.acidwater.laundrytaxi.utils.GeneralUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;


public class SplashActivity extends AppCompatActivity {


    private static final long DELAY = 1500;
    private static Handler sHandler = new Handler();
    private Runnable mRunnableHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getIntent();
        setContentView(R.layout.activity_splash);
        setLanguage();

        mRunnableHome = new Runnable() {
            @Override
            public void run() {
                if (GeneralUtils.getSharedPref(SplashActivity.this, ConstantStrings.USER_SESSION).equals(ConstantStrings.user_is_logged_in)) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    if (getIntent() != null && getIntent().getExtras() != null)
                        intent.putExtras(getIntent().getExtras());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        };
        sHandler.postDelayed(mRunnableHome, DELAY);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mRunnableHome != null) {
            sHandler.removeCallbacks(mRunnableHome);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRunnableHome != null) {
            sHandler.removeCallbacks(mRunnableHome);
        }
    }

    private void setLanguage() {
        if (GeneralUtils.getSharedPref(this, ConstantStrings.APP_LANGUAGE).isEmpty()) {
            GeneralUtils.setSharedPref(this, ConstantStrings.APP_LANGUAGE, ConstantStrings.LANGUAGE_EN);
        }

        Locale locale = new Locale(GeneralUtils.getSharedPref(this, ConstantStrings.APP_LANGUAGE));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }


}
