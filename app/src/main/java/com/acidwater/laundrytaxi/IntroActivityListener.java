package com.acidwater.laundrytaxi;

/**
 * Created by Doumith on 7/17/2017.
 */
public interface IntroActivityListener {

    void navigateToSection(int sectionID);
}
