package com.acidwater.laundrytaxi.bus.events;

/**
 * Created by Doumith on 3/13/2018.
 */
public class UpdateRedeemPoints {

    String redeemPoints;

    public UpdateRedeemPoints(String redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public String getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(String redeemPoints) {
        this.redeemPoints = redeemPoints;
    }
}
