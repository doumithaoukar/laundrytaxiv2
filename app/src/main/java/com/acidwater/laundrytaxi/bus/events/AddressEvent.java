package com.acidwater.laundrytaxi.bus.events;

/**
 * Created by Doumith on 9/18/2017.
 */
public class AddressEvent {

    private String location;
    private Double latitude;
    private Double longitude;

    public AddressEvent(String location, Double latitude, Double longitude) {
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
