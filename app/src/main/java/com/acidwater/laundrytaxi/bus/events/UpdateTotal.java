package com.acidwater.laundrytaxi.bus.events;

import com.acidwater.laundrytaxi.data.order.OrderItem;

import java.util.List;

/**
 * Created by Doumith on 11/11/2017.
 */
public class UpdateTotal {

    public List<OrderItem> orderItems;

    public UpdateTotal(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
