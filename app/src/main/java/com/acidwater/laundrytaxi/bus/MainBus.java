package com.acidwater.laundrytaxi.bus;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Cme on 4/10/2017.
 */

public class MainBus
{
   private static MainBus mainBus;

   private final PublishSubject bus = PublishSubject.create();

   private MainBus()
   {
   }

   public static MainBus getInstance()
   {
      if(mainBus == null)
      {

         mainBus = new MainBus();
      }

      return mainBus;
   }

   public void send(Object o)
   {
      bus.onNext(o);
   }

   public Observable<Object> getBusObservable()
   {
      return bus;
   }

}
