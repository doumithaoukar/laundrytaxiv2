package com.acidwater.laundrytaxi.bus.events;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import android.support.annotation.IntDef;

/**
 * Created by Cme on 4/7/2017.
 */

public class ProfilePictureEvent
{

   public static final int UPDATE_PICTURE_SUCCESS = 0;
   public static final int UPDATE_PICTURE_ERROR = 1;
   public static final int UPDATE_PICTURE_STARTED = 2;
   public static final int UPDATE_PICTURE_COMPLETED = 3;

   public static final int DELETE_PICTURE_SUCCESS = 4;
   public static final int DELETE_PICTURE_ERROR = 5;
   public static final int DELETE_PICTURE_STARTED = 6;
   public static final int DELETE_PICTURE_COMPLETED = 7;

   public static final int RETRIEVE_PICTURE_SUCCESS = 8;
   public static final int RETRIEVE_PICTURE_ERROR = 9;
   public static final int RETRIEVE_PICTURE_STARTED = 10;
   public static final int RETRIEVE_PICTURE_COMPLETED = 11;
   public static final int RETRIEVE_PICTURE_CANCELLED=12;

   @IntDef({UPDATE_PICTURE_SUCCESS, UPDATE_PICTURE_ERROR, UPDATE_PICTURE_STARTED, UPDATE_PICTURE_COMPLETED, DELETE_PICTURE_SUCCESS, DELETE_PICTURE_ERROR, DELETE_PICTURE_STARTED,
      DELETE_PICTURE_COMPLETED, RETRIEVE_PICTURE_SUCCESS, RETRIEVE_PICTURE_ERROR, RETRIEVE_PICTURE_STARTED, RETRIEVE_PICTURE_COMPLETED,RETRIEVE_PICTURE_CANCELLED })
   @Retention(RetentionPolicy.SOURCE)
   public @interface Event {
   }

   @Event
   int event;

   public ProfilePictureEvent(@Event int event)
   {
      this.event = event;
   }

   @Event
   public int getEvent()
   {
      return event;
   }

}
