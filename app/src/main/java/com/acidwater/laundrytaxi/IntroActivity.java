package com.acidwater.laundrytaxi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.acidwater.laundrytaxi.forgotpassword.view.ForgotPasswordFragment;
import com.acidwater.laundrytaxi.login.view.LoginFragment;
import com.acidwater.laundrytaxi.registration.view.RegistrationFragment;
import com.acidwater.laundrytaxi.signup.view.SignUpFragment;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.verification.view.VerificationFragment;
import com.facebook.FacebookSdk;

/**
 * Created by Doumith on 7/6/2017.
 */
public class IntroActivity extends BaseActivity implements IntroActivityListener {

    public static final int LOGIN = 1;
    public static final int SIGN_UP = 2;
    public static final int FORGOT_PASSWORD = 3;
    public static final int VERIFICATION = 4;
    public static final int MAIN_ACTIVITY = 5;

    FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, SignUpFragment.newInstance(),
                getString(R.string.tag_signup)).commit();

        if (GeneralUtils.getSharedPref(getBaseContext(), ConstantStrings.USER_SESSION).equals(ConstantStrings.user_need_verification)) {
            setVerification();
        }
    }


    private void setRegistrationFragment() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_registration)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_registration));
        } else {
            fragment = RegistrationFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_registration))
                    .addToBackStack(getString(R.string.tag_registration))
                    .commit();
        }

    }

    private void setLoginFragment() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_login)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_login));
        } else {
            fragment = LoginFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_login))
                    .addToBackStack(getString(R.string.tag_login))
                    .commit();
        }

    }

    private void setForgotPassword() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_forgot_password)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_forgot_password));
        } else {
            fragment = ForgotPasswordFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_forgot_password))
                    .addToBackStack(getString(R.string.tag_forgot_password))
                    .commit();
        }

    }

    private void setVerification() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_verification)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_verification));
        } else {
            fragment = VerificationFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_verification))
                    .addToBackStack(getString(R.string.tag_verification))
                    .commit();
        }

    }

    private void launchMainActivity() {
        Intent it = new Intent(this, MainActivity.class);
        startActivity(it);
        finish();
    }

    @Override
    public void navigateToSection(int sectionID) {

        switch (sectionID) {
            case SIGN_UP:
                setRegistrationFragment();
                break;

            case FORGOT_PASSWORD:
                setForgotPassword();
                break;

            case LOGIN:
                setLoginFragment();
                break;

            case VERIFICATION:
                setVerification();
                break;

            case MAIN_ACTIVITY:
                launchMainActivity();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            RegistrationFragment fragment = (RegistrationFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_registration));
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }

        }
    }

}
