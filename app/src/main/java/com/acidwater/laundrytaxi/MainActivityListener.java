package com.acidwater.laundrytaxi;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;

import com.acidwater.laundrytaxi.orderdetails.model.DeliveryMethod;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

/**
 * Created by Doumith on 7/17/2017.
 */
public interface MainActivityListener {


    void navigationToScreen(int sectionID);

    void initializeMapView();

    void openDrawer();

    void destroyFragments();

    void navigateToOrderDetails(OrderHistory orderHistory);

    void navigateToEditOrder(OrderHistory orderHistory);

    void navigateToOrderItems(Timepoint[] pickupTimePoints, Timepoint[] dropOffTimePoints, DeliveryMethod deliveryMethod, OrderHistory orderHistory, double minOrder);

    Drawable getNavigationIcon();

    void getUserProfile();

    void navigationToScreen(int sectionID, OrderHistory orderHistory);

    void showGpsDialog();

}
