package com.acidwater.laundrytaxi.notifications.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.notifications.model.NotificationInteractor;
import com.acidwater.laundrytaxi.notifications.model.NotificationsWrapper;
import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryInteractor;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 8/2/2017.
 */
public class NotificationsPresenter extends MvpBasePresenter<NotificationsMvp.View> implements NotificationsMvp.Presenter {


    private Context context;
    private CompositeDisposable compositeDisposable;
    private NotificationInteractor interactor;

    public NotificationsPresenter(Context context) {

        this.context = context;
        compositeDisposable = new CompositeDisposable();
        interactor = new NotificationInteractor(context);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getUserNotifications() {
        compositeDisposable.add(interactor.getUserNotifications().subscribeWith(new ServiceObserver<NotificationsWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoading(false);
                }

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(NotificationsWrapper notificationsWrapper) {

                if (isViewAttached()) {
                    getView().setData(notificationsWrapper);
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {
                    getView().showContent();
                    getView().setError(e.errorMessage);
                }

            }
        }));
    }
}
