package com.acidwater.laundrytaxi.notifications.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by Doumith on 8/2/2017.
 */
public class NotificationInteractor implements NotificationsMvp.Interactor {


    private Context context;
    private NotificationsAPI notificationsAPI;

    public NotificationInteractor(Context context) {
        this.context = context;
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        notificationsAPI = retrofit.create(NotificationsAPI.class);
    }


    @Override
    public Observable<NotificationsWrapper> getUserNotifications() {
        Observable<NotificationsWrapper> observable =
                notificationsAPI.getUserNotifications().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    public interface NotificationsAPI {

        @Headers("Content-Type: application/json")
        @GET(ConstantStrings.SERVICE_NOTIFICAIONS)
        Observable<NotificationsWrapper> getUserNotifications();
    }
}
