package com.acidwater.laundrytaxi.notifications;

import com.acidwater.laundrytaxi.menu.model.Menu;
import com.acidwater.laundrytaxi.notifications.model.NotificationsWrapper;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.Observable;


/**
 * Created by Doumith on 7/30/2017.
 */
public interface NotificationsMvp {


    interface Interactor {

        Observable<NotificationsWrapper> getUserNotifications();
    }

    interface Presenter extends MvpPresenter<View> {

        void getUserNotifications();
    }


    interface View extends MvpLceView<NotificationsWrapper> {

        void setError(String message);

    }
}
