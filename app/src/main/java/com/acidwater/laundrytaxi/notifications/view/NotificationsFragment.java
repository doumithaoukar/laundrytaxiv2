package com.acidwater.laundrytaxi.notifications.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.notifications.NotificationsMvp;
import com.acidwater.laundrytaxi.notifications.model.Notification;
import com.acidwater.laundrytaxi.notifications.model.NotificationsWrapper;
import com.acidwater.laundrytaxi.notifications.presenter.NotificationsPresenter;
import com.acidwater.laundrytaxi.orderdetails.OrderDetailsMvp;
import com.acidwater.laundrytaxi.orderdetails.presenter.OrderDetailsPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.view.OrderHistoryAdapter;
import com.acidwater.laundrytaxi.utils.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

/**
 * Created by Doumith on 7/8/2017.
 */
public class NotificationsFragment extends MvpLceFragment<View, NotificationsWrapper, NotificationsMvp.View, NotificationsMvp.Presenter> implements NotificationsMvp.View {


    private MainActivityListener mListener;
    private RecyclerView recyclerView;

    public static NotificationsFragment newInstance() {
        NotificationsFragment notificationsFragment = new NotificationsFragment();
        return notificationsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_notifications, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));
        recyclerView.setLayoutManager(layoutManager);
         return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public NotificationsMvp.Presenter createPresenter() {
        return new NotificationsPresenter(getContext());
    }

    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void setData(NotificationsWrapper data) {
        NotificationsAdapter historyAdapter = new NotificationsAdapter(getContext(), data.getNotifications());
        historyAdapter.setOnItemClickListener(new NotificationsAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(Notification notification) {
            }
        });
        recyclerView.setAdapter(historyAdapter);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getUserNotifications();
    }
}
