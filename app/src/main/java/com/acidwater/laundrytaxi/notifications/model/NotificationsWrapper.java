package com.acidwater.laundrytaxi.notifications.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 9/19/2017.
 */
public class NotificationsWrapper implements Parcelable {

    private int status;

    @SerializedName("Notifications")
    private ArrayList<Notification> notifications;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.status);
        dest.writeTypedList(this.notifications);
    }

    public NotificationsWrapper() {
    }

    protected NotificationsWrapper(Parcel in) {
        this.status = in.readInt();
        this.notifications = in.createTypedArrayList(Notification.CREATOR);
    }

    public static final Parcelable.Creator<NotificationsWrapper> CREATOR = new Parcelable.Creator<NotificationsWrapper>() {
        @Override
        public NotificationsWrapper createFromParcel(Parcel source) {
            return new NotificationsWrapper(source);
        }

        @Override
        public NotificationsWrapper[] newArray(int size) {
            return new NotificationsWrapper[size];
        }
    };
}
