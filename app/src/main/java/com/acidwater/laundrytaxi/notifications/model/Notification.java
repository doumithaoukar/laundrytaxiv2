package com.acidwater.laundrytaxi.notifications.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 9/19/2017.
 */
public class Notification implements Parcelable {

    @SerializedName("icon")
    private String image;
    @SerializedName("message")
    private String message;
    @SerializedName("timestamp")
    private long date;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.image);
        dest.writeString(this.message);
        dest.writeLong(this.date);
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        this.image = in.readString();
        this.message = in.readString();
        this.date = in.readLong();
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
