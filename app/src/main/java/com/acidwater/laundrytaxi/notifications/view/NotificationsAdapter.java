package com.acidwater.laundrytaxi.notifications.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.notifications.model.Notification;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Doumith on 7/8/2017.
 */
public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {


    private List<Notification> list;
    private Context context;
    private LayoutInflater inflater;
    private OnItemClickListener onItemClickListener;


    public NotificationsAdapter(Context context, List<Notification> list) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text;
        public TextView date;
        public ImageView image;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            text = (TextView) itemLayoutView.findViewById(R.id.notification_text);
            date = (TextView) itemLayoutView.findViewById(R.id.notification_date);
            image = (ImageView) itemLayoutView.findViewById(R.id.notification_image);
        }
    }


    @Override
    public NotificationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        return new ViewHolder(
                inflater.inflate(R.layout.row_notification_item, parent, false));

    }

    @Override
    public void onBindViewHolder(NotificationsAdapter.ViewHolder holder, final int pos) {
        holder.text.setText(list.get(pos).getMessage());
        holder.date.setText(GeneralUtils.getDate(list.get(pos).getDate()) + "  " + GeneralUtils.getTime(list.get(pos).getDate()));
        if (!TextUtils.isEmpty(list.get(pos).getImage())) {
            Picasso.with(context).load(list.get(pos).getImage()).placeholder(R.drawable.notification_place_holder).into(holder.image);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClicked(list.get(pos));
                }
            }
        });

    }

    public interface OnItemClickListener {
        void onItemClicked(Notification notification);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
