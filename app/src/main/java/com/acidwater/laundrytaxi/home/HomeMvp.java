package com.acidwater.laundrytaxi.home;

import android.view.View;

import com.acidwater.laundrytaxi.data.AppConfigWrapper;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/13/2017.
 */
public interface HomeMvp {


    interface Interactor {

        Observable<AppConfigWrapper> getAppConfig();
    }

    interface Presenter extends MvpPresenter<View> {

        void getAppConfig();


        boolean isLocationEnabled();
    }

    interface View extends MvpView {


        void setAppConfigLimitation(String message, boolean isBlocking);

        void showLoadingView();

        void HideLoadingView();

        void setAppConfigSession();


        void showError(String message);
    }
}
