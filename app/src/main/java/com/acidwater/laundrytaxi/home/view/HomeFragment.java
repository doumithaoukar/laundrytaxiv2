package com.acidwater.laundrytaxi.home.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.BaseFragment;
import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.SplashActivity;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateRedeemPoints;
import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.home.HomeMvp;
import com.acidwater.laundrytaxi.home.presenter.HomePresenter;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Doumith on 7/8/2017.
 */
public class HomeFragment extends MvpFragment<HomeMvp.View, HomeMvp.Presenter> implements OnClickListener, HomeMvp.View {


    private TextView reward_points;
    private TextView tv_welcome_user;
    private Button btn_create_order;
    private LinearLayout rewardPointsInfo;
    private MainActivityListener mListener;
    private CompositeDisposable compositeDisposable;
    private RelativeLayout progressBar;

    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            if (activity.activityFirstLaunch) {
                presenter.getAppConfig();
            }
        }
        mListener.getUserProfile();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        RxUtils.disposeIfNotNull(compositeDisposable);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_home, container, false);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        reward_points = (TextView) mView.findViewById(R.id.reward_points);
        rewardPointsInfo = (LinearLayout) mView.findViewById(R.id.reward_points_info);
        tv_welcome_user = (TextView) mView.findViewById(R.id.tv_welcome_user);
        btn_create_order = (Button) mView.findViewById(R.id.btn_create_order);
        btn_create_order.setOnClickListener(this);

        registerToBus();

        rewardPointsInfo.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        User user = GeneralUtils.getUserProfile(getContext());
        if (user != null) {
            tv_welcome_user.setText(getString(R.string.hi) + " " + user.getFirstName() + " " + user.getLastName());
            reward_points.setText(user.getRewardPoints());
        }

    }


    private void registerToBus() {
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof UpdateRedeemPoints) {

                    UpdateRedeemPoints updateRedeemPoints = (UpdateRedeemPoints) object;
                    reward_points.setText(updateRedeemPoints.getRedeemPoints());
                }

            }

        }));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create_order:
                if (presenter.isLocationEnabled()) {
                    mListener.navigationToScreen(ConstantStrings.MAP_FRAGMENT);
                } else {
                    mListener.showGpsDialog();
                }
                break;

            case R.id.reward_points_info:
                mListener.navigationToScreen(ConstantStrings.REWARD_POINTS);
                break;
        }
    }

    @Override
    public HomeMvp.Presenter createPresenter() {
        return new HomePresenter(getContext());
    }

    @Override
    public void setAppConfigLimitation(String message, boolean isBlocking) {
        showBlockingDialog(message, isBlocking);
    }

    private void showBlockingDialog(String message, boolean isBlocking) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage(message);

        if (isBlocking) {
            alertDialogBuilder.setCancelable(false);
        } else {
            alertDialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        alertDialogBuilder.show();
    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void HideLoadingView() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setAppConfigSession() {
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.activityFirstLaunch = false;
        }
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

}
