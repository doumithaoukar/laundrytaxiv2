package com.acidwater.laundrytaxi.home.presenter;

import android.content.Context;
import android.location.LocationManager;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.AppConfigWrapper;
import com.acidwater.laundrytaxi.home.HomeMvp;
import com.acidwater.laundrytaxi.home.model.HomeInteractor;
import com.acidwater.laundrytaxi.login.LoginMvp;
import com.acidwater.laundrytaxi.login.model.LoginInteractor;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/11/2017.
 */
public class HomePresenter extends MvpBasePresenter<HomeMvp.View> implements HomeMvp.Presenter {

    private Context context;
    private CompositeDisposable compositeDisposable;
    private HomeInteractor homeInteractor;

    public HomePresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        homeInteractor = new HomeInteractor(context);
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getAppConfig() {
        compositeDisposable.add(homeInteractor.getAppConfig().subscribeWith(new ServiceObserver<AppConfigWrapper>() {
            @Override
            public void onStart() {

                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(AppConfigWrapper appConfigWrapper) {
                if (isViewAttached()) {
                    getView().setAppConfigSession();
                }
                if (appConfigWrapper != null && appConfigWrapper.getConfig() != null) {
                    GeneralUtils.saveAppConfig(context, appConfigWrapper.getConfig());
                    if (appConfigWrapper.getConfig().getMessage() != null) {
                        if (isViewAttached()) {
                            if (GeneralUtils.getSharedPref(context, ConstantStrings.APP_LANGUAGE).equals(ConstantStrings.LANGUAGE_EN)) {
                                getView().setAppConfigLimitation(appConfigWrapper.getConfig().getMessage().getTextEn(), appConfigWrapper.getConfig().getMessage().isBlocking());
                            } else {
                                getView().setAppConfigLimitation(appConfigWrapper.getConfig().getMessage().getTextAr(), appConfigWrapper.getConfig().getMessage().isBlocking());
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {
                    getView().showError(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }
}
