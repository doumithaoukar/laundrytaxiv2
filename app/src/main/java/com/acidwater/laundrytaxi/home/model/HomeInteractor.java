package com.acidwater.laundrytaxi.home.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.AppConfigWrapper;
import com.acidwater.laundrytaxi.home.HomeMvp;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 8/1/2017.
 */
public class HomeInteractor implements HomeMvp.Interactor {

    private Context context;
    private CompositeDisposable compositeDisposable;
    private HomeApi homeApi;

    public HomeInteractor(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();

        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        homeApi = retrofit.create(HomeApi.class);
    }


    @Override
    public Observable<AppConfigWrapper> getAppConfig() {

        Observable<AppConfigWrapper> observable =
                homeApi.getAppConfig().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface HomeApi {


        @Headers("Content-Type: application/json")
        @GET(ConstantStrings.SERVICE_APP_CONFIG)
        Observable<AppConfigWrapper> getAppConfig();
    }
}
