package com.acidwater.laundrytaxi.managepayment.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.managepayment.ManagePaymentMvp;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ManagePaymentPresenter extends MvpBasePresenter<ManagePaymentMvp.View> implements ManagePaymentMvp.Presenter {

    private Context context;

    public ManagePaymentPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }


    @Override
    public void getRewardPoints() {
        User user = GeneralUtils.getUserProfile(context);
        if (isViewAttached()) {
            getView().setRewardPoints(user.getRewardPoints());
        }
    }
}
