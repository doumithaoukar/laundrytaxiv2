package com.acidwater.laundrytaxi.managepayment.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.about.AboutMvp;
import com.acidwater.laundrytaxi.about.presenter.AboutPresenter;
import com.acidwater.laundrytaxi.managepayment.ManagePaymentMvp;
import com.acidwater.laundrytaxi.managepayment.presenter.ManagePaymentPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class ManagePaymentFragment extends MvpFragment<ManagePaymentMvp.View, ManagePaymentMvp.Presenter> implements View.OnClickListener, ManagePaymentMvp.View {


    private MainActivityListener mListener;
    private TextView rewardPoints;
    private TextView title;
    private TextView cash;


    public static ManagePaymentFragment newInstance() {
        ManagePaymentFragment aboutFragment = new ManagePaymentFragment();
        return aboutFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getRewardPoints();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_manage_payment, container, false);

        rewardPoints = (TextView) mView.findViewById(R.id.reward_points);
        title = (TextView) mView.findViewById(R.id.manage_payment_title);
        cash = (TextView) mView.findViewById(R.id.payment_cash);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public ManagePaymentMvp.Presenter createPresenter() {
        return new ManagePaymentPresenter(getContext());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }


    @Override
    public void setRewardPoints(String points) {
        rewardPoints.setText(points);
    }
}