package com.acidwater.laundrytaxi.managepayment;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Doumith on 8/13/2017.
 */
public interface ManagePaymentMvp {


    interface Presenter extends MvpPresenter<View> {

        void getRewardPoints();

    }

    interface View extends MvpView {

        void setRewardPoints(String points);

    }
}
