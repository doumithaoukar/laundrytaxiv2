package com.acidwater.laundrytaxi.login.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 7/15/2017.
 */
public class UserWrapper implements Parcelable {


    private int status;
    private String message;

    @SerializedName("UserObject")
    private User user;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.status);
        dest.writeString(this.message);
        dest.writeParcelable(this.user, flags);
    }

    public UserWrapper() {
    }

    protected UserWrapper(Parcel in) {
        this.status = in.readInt();
        this.message = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserWrapper> CREATOR = new Parcelable.Creator<UserWrapper>() {
        @Override
        public UserWrapper createFromParcel(Parcel source) {
            return new UserWrapper(source);
        }

        @Override
        public UserWrapper[] newArray(int size) {
            return new UserWrapper[size];
        }
    };
}
