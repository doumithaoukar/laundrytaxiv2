package com.acidwater.laundrytaxi.login.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.IntroActivity;
import com.acidwater.laundrytaxi.IntroActivityListener;
import com.acidwater.laundrytaxi.login.LoginMvp;
import com.acidwater.laundrytaxi.login.presenter.LoginPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class LoginFragment extends MvpFragment<LoginMvp.View, LoginMvp.Presenter> implements View.OnClickListener, LoginMvp.View {


    private Button btnContinue;
    private EditText email;
    private EditText password;
    private TextView forgotPassword;
    private RelativeLayout progressBar;

    private IntroActivityListener mListener;

    public static LoginFragment newInstance() {
        LoginFragment loginFragment = new LoginFragment();
        return loginFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_login, container, false);

        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        email = (EditText) mView.findViewById(R.id.email);
        password = (EditText) mView.findViewById(R.id.password);

        forgotPassword = (TextView) mView.findViewById(R.id.btnForgotPassword);
        forgotPassword.setOnClickListener(this);

        btnContinue.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validate(email.getText().toString(), password.getText().toString());
                break;

            case R.id.btnForgotPassword:
                mListener.navigateToSection(IntroActivity.FORGOT_PASSWORD);
                break;


        }
    }

    @Override
    public LoginMvp.Presenter createPresenter() {
        return new LoginPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void HideLoadingView() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setResultSuccessful() {
        mListener.navigateToSection(IntroActivity.MAIN_ACTIVITY);
    }

    @Override
    public void openVerificationFragment() {
        destroyThis();
        mListener.navigateToSection(IntroActivity.VERIFICATION);
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }
}