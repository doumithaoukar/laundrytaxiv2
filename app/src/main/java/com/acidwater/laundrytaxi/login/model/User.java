package com.acidwater.laundrytaxi.login.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.acidwater.laundrytaxi.data.order.OrderGps;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 7/15/2017.
 */
public class User implements Parcelable {


    @SerializedName("firstname")
    @Expose
    private String firstName;

    @SerializedName("lastname")
    @Expose
    private String lastName;

    @SerializedName("phone")
    @Expose
    private String phoneNumber;

    @SerializedName("rewardPoints")
    @Expose
    private String rewardPoints;

    @SerializedName("Username")
    @Expose
    private String username;

    @SerializedName("UserId")
    @Expose
    private String userID;

    @SerializedName("AccountStatus")
    @Expose
    private int accountStatus;

    @SerializedName("profilePicter")
    @Expose
    private String image;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @SerializedName("UserInvitationCode")
    private String invitationCode;


    @SerializedName("homeLocation")
    private OrderLocation homeLocation;

    public OrderLocation getHomeLocation() {
        return homeLocation;
    }

    public void setHomeLocation(OrderLocation homeLocation) {
        this.homeLocation = homeLocation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRewardPoints() {
        return rewardPoints;
    }

    public void setRewardPoints(String rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }


    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.rewardPoints);
        dest.writeString(this.username);
        dest.writeString(this.userID);
        dest.writeInt(this.accountStatus);
        dest.writeString(this.image);
        dest.writeString(this.invitationCode);
        dest.writeParcelable(this.homeLocation, flags);
    }

    protected User(Parcel in) {
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.phoneNumber = in.readString();
        this.rewardPoints = in.readString();
        this.username = in.readString();
        this.userID = in.readString();
        this.accountStatus = in.readInt();
        this.image = in.readString();
        this.invitationCode = in.readString();
        this.homeLocation = in.readParcelable(OrderLocation.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
