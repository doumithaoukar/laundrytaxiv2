package com.acidwater.laundrytaxi.login;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by Doumith on 7/6/2017.
 */
public interface LoginMvp {

    interface Interactor {
        Observable<UserWrapper> login(String email, String password);

        Observable<ServerResponse> logout( );
    }

    interface Presenter extends MvpPresenter<View> {

        void signIn(String email, String password);

        void validate(String email, String password);

        void signOut();

    }

    interface View extends MvpView {


        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful();

        void openVerificationFragment();

    }
}
