package com.acidwater.laundrytaxi.login.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.LoginMvp;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/11/2017.
 */
public class LoginInteractor implements LoginMvp.Interactor {

    private LoginApi loginApi;

    public LoginInteractor(Context context) {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        loginApi = retrofit.create(LoginApi.class);
    }

    @Override
    public Observable<UserWrapper> login(String email, String password) {

        Observable<UserWrapper> observable = null;
        UserRequest user = new UserRequest(email, password);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (user.toJson()).toString());
        observable =
                loginApi.login(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    @Override
    public Observable<ServerResponse> logout() {
        Observable<ServerResponse> observable = null;
        observable =
                loginApi.logout().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface LoginApi {
        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_LOGIN)
        Observable<UserWrapper> login(@Body RequestBody body);

        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_LOGOUT)
        Observable<ServerResponse> logout();
    }


    private class UserRequest {
        @SerializedName("user")
        String email;

        @SerializedName("pass")
        String password;

        public UserRequest(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }

}
