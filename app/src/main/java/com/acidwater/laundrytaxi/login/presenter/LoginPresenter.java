package com.acidwater.laundrytaxi.login.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.LoginMvp;
import com.acidwater.laundrytaxi.login.model.LoginInteractor;
import com.acidwater.laundrytaxi.login.model.User;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

/**
 * Created by Doumith on 7/11/2017.
 */
public class LoginPresenter extends MvpBasePresenter<LoginMvp.View> implements LoginMvp.Presenter {

    private Context context;
    private LoginInteractor loginInteractor;
    private CompositeDisposable compositeDisposable;

    public LoginPresenter(Context context) {
        this.context = context;
        loginInteractor = new LoginInteractor(context);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void signIn(String username, String password) {

        compositeDisposable.add(loginInteractor.login(username, password).
                subscribeWith(new ServiceObserver<UserWrapper>() {
                                  @Override
                                  public void onStart() {
                                      if (isViewAttached()) {
                                          getView().showLoadingView();
                                      }
                                  }

                                  @Override
                                  public void onComplete() {

                                      if (isViewAttached()) {

                                          getView().HideLoadingView();
                                      }
                                  }

                                  @Override
                                  public void onNext(UserWrapper userWrapper) {

                                      GeneralUtils.saveUserProfile(context, userWrapper);
                                      GeneralUtils.setSharedPref(context, ConstantStrings.USER_TYPE, ConstantStrings.USER_EMAIL);

                                      if (isViewAttached()) {
                                          if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_NOT_ACTIVATED) {
                                              getView().openVerificationFragment();
                                              GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_need_verification);
                                          } else if (userWrapper.getUser().getAccountStatus() == ConstantStrings.ACCOUNT_BLOCKED) {

                                              getView().showErrorMessage(context.getString(R.string.account_blocked));

                                          } else {
                                              GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                                              getView().setResultSuccessful();
                                          }
                                      }
                                  }

                                  @Override
                                  public void onError(LaundryTaxiException e) {

                                      if (isViewAttached()) {
                                          if (e.reason == LaundryTaxiException.Reason.ALREADY_LOGIN) {
                                              signOut();
                                          } else {
                                              getView().showErrorMessage(e.errorMessage);
                                          }

                                          getView().HideLoadingView();
                                      }
                                  }
                              }

                ));


    }

    @Override
    public void validate(String email, String password) {

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_username_password));
                return;
            }
        } else if (!GeneralUtils.isEmailValid(email)) {
            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.invalid_email));
                return;
            }
        }

        signIn(email, password);

    }

    @Override
    public void signOut() {

        compositeDisposable.add(loginInteractor.logout().subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                ClearableCookieJar cookieJar =
                        new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                cookieJar.clear();
                GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, "");
                GeneralUtils.setSharedPref(context, ConstantStrings.USER_TYPE, "");

            }

            @Override
            public void onError(LaundryTaxiException e) {

            }
        }));
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }
}
