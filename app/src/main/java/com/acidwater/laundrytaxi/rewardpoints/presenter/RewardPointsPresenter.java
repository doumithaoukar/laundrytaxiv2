package com.acidwater.laundrytaxi.rewardpoints.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.contact.ContactMvp;
import com.acidwater.laundrytaxi.rewardpoints.RewardPointsMvp;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Doumith on 7/13/2017.
 */
public class RewardPointsPresenter extends MvpBasePresenter<RewardPointsMvp.View> implements RewardPointsMvp.Presenter {

    private Context context;

    public RewardPointsPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }


}
