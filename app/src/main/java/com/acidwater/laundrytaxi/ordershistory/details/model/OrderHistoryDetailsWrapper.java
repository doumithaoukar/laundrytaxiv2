package com.acidwater.laundrytaxi.ordershistory.details.model;

import com.acidwater.laundrytaxi.data.order.OrderItem;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrderHistoryDetailsWrapper {

    private int status;

    @SerializedName("OrderHistory")
    private ArrayList<OrderItem> orders;

    @SerializedName("OrderInfo")
    private OrderHistoryDetails orderInfo;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<OrderItem> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrderItem> orders) {
        this.orders = orders;
    }

    public OrderHistoryDetails getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderHistoryDetails orderInfo) {
        this.orderInfo = orderInfo;
    }
}
