package com.acidwater.laundrytaxi.ordershistory.details.presenter;

import android.content.Context;

import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.ordershistory.details.OrdersHistoryDetailsMvp;
import com.acidwater.laundrytaxi.ordershistory.details.model.OrderHistoryDetailsWrapper;
import com.acidwater.laundrytaxi.ordershistory.details.model.OrderHistoryIDetailsInteractor;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryInteractor;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrdersHistoryDetailsPresenter extends MvpBasePresenter<OrdersHistoryDetailsMvp.View> implements OrdersHistoryDetailsMvp.Presenter {


    private Context context;
    private CompositeDisposable compositeDisposable;
    private OrderHistoryIDetailsInteractor interactor;

    public OrdersHistoryDetailsPresenter(Context context) {

        this.context = context;
        compositeDisposable = new CompositeDisposable();
        interactor = new OrderHistoryIDetailsInteractor(context);
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getOrderDetails(OrderHistory orderHistory) {
        if(isViewAttached()){
            getView().setData(orderHistory);
        }
//        compositeDisposable.add(interactor.getOrderDetails(orderID).subscribeWith(new ServiceObserver<OrderHistoryDetailsWrapper>() {
//            @Override
//            public void onStart() {
//                if (isViewAttached()) {
//                    getView().showLoading(false);
//                }
//
//            }
//
//            @Override
//            public void onComplete() {
//
//                if (isViewAttached()) {
//                    getView().showContent();
//                }
//            }
//
//            @Override
//            public void onNext(OrderHistoryDetailsWrapper orderHistoryWrapper) {
//
//                if (isViewAttached()) {
//                    getView().setData(orderHistoryWrapper);
//                }
//            }
//
//            @Override
//            public void onError(LaundryTaxiException e) {
//                if (isViewAttached()) {
//                    getView().showContent();
//                    getView().setError(e.errorMessage);
//                }
//
//            }
//        }));
    }
}
