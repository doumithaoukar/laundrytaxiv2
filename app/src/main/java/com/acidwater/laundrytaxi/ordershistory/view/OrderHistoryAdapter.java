package com.acidwater.laundrytaxi.ordershistory.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.data.AppConfig;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {


    private List<OrderHistory> list;
    private Context context;
    private LayoutInflater inflater;
    private OnItemClickListener onItemClickListener;
    private AppConfig appConfig;


    public OrderHistoryAdapter(Context context, List<OrderHistory> list) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);
        appConfig = GeneralUtils.getAppConfig(context);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView orderID;
        public TextView pickupDate;
        public TextView dropOffDate;
        public TextView status;
        public TextView price;
        public ImageView image;
        public Button cancel;
        public Button edit;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            orderID = (TextView) itemLayoutView.findViewById(R.id.order_id);
            status = (TextView) itemLayoutView.findViewById(R.id.order_status);
            pickupDate = (TextView) itemLayoutView.findViewById(R.id.pickupDate);
            dropOffDate = (TextView) itemLayoutView.findViewById(R.id.dropOffDate);
            price = (TextView) itemLayoutView.findViewById(R.id.price);
            cancel = (Button) itemLayoutView.findViewById(R.id.btn_cancel);
            edit = (Button) itemLayoutView.findViewById(R.id.btn_edit);

        }
    }


    @Override
    public OrderHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        return new ViewHolder(
                inflater.inflate(R.layout.row_my_order_list, parent, false));

    }

    @Override
    public void onBindViewHolder(OrderHistoryAdapter.ViewHolder holder, final int pos) {

        holder.orderID.setText(context.getString(R.string.order_id) + " " + list.get(pos).getOrderID());
        holder.pickupDate.setText(list.get(pos).getPickupDateTime());
        holder.dropOffDate.setText(list.get(pos).getDropOffDateTime());
        holder.price.setText(list.get(pos).getTotal() + appConfig.getCurrency());
        holder.status.setText(list.get(pos).getStatus());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClicked(list.get(pos));
                }
            }
        });
        if (list.get(pos).isCancelable()) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onCancelClicked(list.get(pos));
                    }
                }
            });
        } else {
            holder.cancel.setVisibility(View.GONE);
        }

        if (list.get(pos).isEditable()) {
            holder.edit.setVisibility(View.VISIBLE);
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onEditClicked(list.get(pos));
                    }
                }
            });
        } else {
            holder.edit.setVisibility(View.GONE);
        }

    }

    public interface OnItemClickListener {
        void onItemClicked(OrderHistory orderHistory);

        void onCancelClicked(OrderHistory orderHistory);

        void onEditClicked(OrderHistory orderHistory);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
