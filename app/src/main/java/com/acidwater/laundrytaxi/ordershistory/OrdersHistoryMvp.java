package com.acidwater.laundrytaxi.ordershistory;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.orderitems.model.OrderResult;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItem;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/13/2017.
 */
public interface OrdersHistoryMvp {


    interface Interactor {

        Observable<OrderHistoryWrapper> getHistory();

        Observable<OrderResult> cancelOrder(OrderHistory orderHistory);
    }

    interface Presenter extends MvpPresenter<View> {

        void getHistory();

        void cancelOrder(OrderHistory orderHistory);

    }


    interface View extends MvpLceView<OrderHistoryWrapper> {

        void setError(String message);

        void showLoadingView();

        void hideLoadingView();

        void showErrorMessage(String message);

    }
}
