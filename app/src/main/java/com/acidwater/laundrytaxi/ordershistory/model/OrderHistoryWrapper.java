package com.acidwater.laundrytaxi.ordershistory.model;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrderHistoryWrapper {

    private int status;
    private String message;

    @SerializedName("OrderHistory")
    private ArrayList<OrderHistory> orders;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<OrderHistory> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrderHistory> orders) {
        this.orders = orders;
    }


    private ArrayList<OrderHistory> getOrdersHistory() {
        ArrayList<OrderHistory> filtered = new ArrayList<>();
        for (OrderHistory orderHistory : orders) {
            if (orderHistory.getStatus().equalsIgnoreCase(ConstantStrings.TYPE_ORDER_DELIVERED) ||
                    orderHistory.getStatus().equalsIgnoreCase(ConstantStrings.TYPE_ORDER_CANCEL)) {
                filtered.add(orderHistory);
            }
        }
        return filtered;
    }

    private ArrayList<OrderHistory> getOrdersInProgress() {
        ArrayList<OrderHistory> filtered = new ArrayList<>();
        for (OrderHistory orderHistory : orders) {
            if (orderHistory.getStatus().equalsIgnoreCase(ConstantStrings.TYPE_ORDER_CREATED) ||
                    orderHistory.getStatus().equalsIgnoreCase(ConstantStrings.TYPE_ORDER_PICKING_UP) ||
                    orderHistory.getStatus().equalsIgnoreCase(ConstantStrings.TYPE_ORDER_CLEANING) ||
                    orderHistory.getStatus().equalsIgnoreCase(ConstantStrings.TYPE_ORDER_DELIVERING)) {
                filtered.add(orderHistory);
            }
        }
        return filtered;
    }

    public ArrayList<OrderHistory> filter(String type) {

        if (type.equalsIgnoreCase(ConstantStrings.MY_ORDERS_IN_PROGRESS))
            return getOrdersInProgress();

        return getOrdersHistory();
    }


}
