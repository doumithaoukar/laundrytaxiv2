package com.acidwater.laundrytaxi.ordershistory.details.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.ordershistory.details.OrdersHistoryDetailsMvp;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrderHistoryIDetailsInteractor implements OrdersHistoryDetailsMvp.Interactor {


    private Context context;
    private OrderHistoryApi orderHistoryApi;

    public OrderHistoryIDetailsInteractor(Context context) {
        this.context = context;
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        orderHistoryApi = retrofit.create(OrderHistoryApi.class);
    }


    @Override
    public Observable<OrderHistoryDetailsWrapper> getOrderDetails(OrderHistory orderHistory) {
        OrderRequest orderRequest = new OrderRequest(orderHistory.getOrderID());
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (orderRequest.toJson()).toString());
        Observable<OrderHistoryDetailsWrapper> observable =
                orderHistoryApi.getOrderDetails(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    public interface OrderHistoryApi {
        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_ORDER_DETAILS)
        Observable<OrderHistoryDetailsWrapper> getOrderDetails(@Body RequestBody body);
    }

    private class OrderRequest {
        @SerializedName("orderId")
        private String orderId;


        public OrderRequest(String orderId) {
            this.orderId = orderId;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }
}
