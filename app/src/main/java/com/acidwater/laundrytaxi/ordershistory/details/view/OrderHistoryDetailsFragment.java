package com.acidwater.laundrytaxi.ordershistory.details.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.ordershistory.details.OrdersHistoryDetailsMvp;
import com.acidwater.laundrytaxi.ordershistory.details.model.OrderHistoryDetailsWrapper;
import com.acidwater.laundrytaxi.ordershistory.details.presenter.OrdersHistoryDetailsPresenter;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import org.w3c.dom.Text;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrderHistoryDetailsFragment extends MvpLceFragment<View, OrderHistory, OrdersHistoryDetailsMvp.View, OrdersHistoryDetailsMvp.Presenter> implements OrdersHistoryDetailsMvp.View {


    private MainActivityListener mListener;
    private OrderHistory orderHistory;

    private TextView mOrderID;
    private TextView mPickupLocation;
    private TextView mDropOffLocation;

    public static OrderHistoryDetailsFragment newInstance(OrderHistory orderHistory) {
        OrderHistoryDetailsFragment ordersFragment = new OrderHistoryDetailsFragment();
        Bundle b = new Bundle();
        b.putParcelable("orderHistory", orderHistory);
        ordersFragment.setArguments(b);
        return ordersFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_order_histroy_details, container, false);
        mOrderID = (TextView) mView.findViewById(R.id.orderID);
        mPickupLocation = (TextView) mView.findViewById(R.id.pickup_location);
        mDropOffLocation = (TextView) mView.findViewById(R.id.drop_off_location);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            orderHistory = savedInstanceState.getParcelable("orderHistory");
        } else if (getArguments() != null) {
            orderHistory = getArguments().getParcelable("orderHistory");
        }
        loadData(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("orderHistory", orderHistory);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }


    @Override
    public OrdersHistoryDetailsMvp.Presenter createPresenter() {
        return new OrdersHistoryDetailsPresenter(getContext());
    }

    @Override
    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void setData(OrderHistory data) {
        mOrderID.setText(orderHistory.getOrderID());
        mPickupLocation.setText(data.getPickupLocation().getAddress());
        mDropOffLocation.setText(data.getDropOffLocation().getAddress());
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getOrderDetails(orderHistory);
    }
}
