package com.acidwater.laundrytaxi.ordershistory.details;

import com.acidwater.laundrytaxi.ordershistory.details.model.OrderHistoryDetailsWrapper;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/13/2017.
 */
public interface OrdersHistoryDetailsMvp {


    interface Interactor {

        Observable<OrderHistoryDetailsWrapper> getOrderDetails(OrderHistory orderHistory);
    }

    interface Presenter extends MvpPresenter<View> {

        void getOrderDetails(OrderHistory orderHistory);
    }


    interface View extends MvpLceView<OrderHistory> {

        void setError(String message);

    }
}
