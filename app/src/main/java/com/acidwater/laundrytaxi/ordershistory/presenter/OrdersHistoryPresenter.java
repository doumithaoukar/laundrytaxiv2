package com.acidwater.laundrytaxi.ordershistory.presenter;

import android.content.Context;
import android.widget.Toast;

import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.orderitems.model.OrderResult;
import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryInteractor;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.pricelist.PriceListMvp;
import com.acidwater.laundrytaxi.services.ServiceObserver;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.LaundryTaxiException;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrdersHistoryPresenter extends MvpBasePresenter<OrdersHistoryMvp.View> implements OrdersHistoryMvp.Presenter {


    private Context context;
    private CompositeDisposable compositeDisposable;
    private OrderHistoryInteractor interactor;

    public OrdersHistoryPresenter(Context context) {

        this.context = context;
        compositeDisposable = new CompositeDisposable();
        interactor = new OrderHistoryInteractor(context);
    }

    @Override
    public void getHistory() {

        compositeDisposable.add(interactor.getHistory().subscribeWith(new ServiceObserver<OrderHistoryWrapper>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoading(false);
                }

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(OrderHistoryWrapper orderHistoryWrapper) {

                if (isViewAttached()) {
                    getView().setData(orderHistoryWrapper);
                }
            }

            @Override
            public void onError(LaundryTaxiException e) {
                if (isViewAttached()) {
                    getView().showContent();
                    getView().setError(e.errorMessage);
                }

            }
        }));
    }

    @Override
    public void cancelOrder(OrderHistory orderHistory) {

        compositeDisposable.add(interactor.cancelOrder(orderHistory).subscribeWith(new ServiceObserver<OrderResult>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                }
            }

            @Override
            public void onNext(OrderResult serverResponse) {
                GeneralUtils.updateRewardPoints(context, serverResponse.getNewRewardPointsCount());
                getHistory();
            }

            @Override
            public void onError(LaundryTaxiException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().hideLoadingView();
                }
            }
        }));
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }
}
