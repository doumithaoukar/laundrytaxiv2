package com.acidwater.laundrytaxi.ordershistory.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.acidwater.laundrytaxi.data.order.OrderItem;
import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrderHistory implements Parcelable {


    @SerializedName("Instruction")
    private String instruction;

    @SerializedName("PickupLocation")
    private OrderLocation pickupLocation;

    @SerializedName("DropoffLocation")
    private OrderLocation dropOffLocation;


    @SerializedName("UserOrderId")
    private String orderID;
    @SerializedName("Status")
    private String status;
    @SerializedName("TimeOfOrder")
    private String orderTime;
    @SerializedName("PaymentMethod")
    private String paymentMethod;
    @SerializedName("DeliveryMethod")
    private String deliveryMethod;
    @SerializedName("Total")
    private String total;

    @SerializedName("PromoCode")
    private String promoCode;


    public long getPickupTimeStamp() {
        return pickupTimeStamp;
    }

    public void setPickupTimeStamp(long pickupTimeStamp) {
        this.pickupTimeStamp = pickupTimeStamp;
    }

    public long getDropOffTimeStamp() {
        return dropOffTimeStamp;
    }

    public void setDropOffTimeStamp(long dropOffTimeStamp) {
        this.dropOffTimeStamp = dropOffTimeStamp;
    }

    @SerializedName("PickupTimestamp")
    private long pickupTimeStamp;

    @SerializedName("DropoffTimestamp")
    private long dropOffTimeStamp;


    @SerializedName("Editable")
    private boolean editable;

    @SerializedName("Cancelable")
    private boolean cancelable;


    @SerializedName("PickupDateTime")
    private String pickupDateTime;


    @SerializedName("DropoffDateTime")
    private String dropOffDateTime;


    @SerializedName("Details")
    private ArrayList<OrderItem> orderItems;


    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public OrderLocation getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(OrderLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public OrderLocation getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(OrderLocation dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isCancelable() {
        return cancelable;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getDropOffDateTime() {
        return dropOffDateTime;
    }

    public void setDropOffDateTime(String dropOffDateTime) {
        this.dropOffDateTime = dropOffDateTime;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderHistory() {
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.instruction);
        dest.writeParcelable(this.pickupLocation, flags);
        dest.writeParcelable(this.dropOffLocation, flags);
        dest.writeString(this.orderID);
        dest.writeString(this.status);
        dest.writeString(this.orderTime);
        dest.writeString(this.paymentMethod);
        dest.writeString(this.deliveryMethod);
        dest.writeString(this.total);
        dest.writeString(this.promoCode);
        dest.writeLong(this.pickupTimeStamp);
        dest.writeLong(this.dropOffTimeStamp);
        dest.writeByte(this.editable ? (byte) 1 : (byte) 0);
        dest.writeByte(this.cancelable ? (byte) 1 : (byte) 0);
        dest.writeString(this.pickupDateTime);
        dest.writeString(this.dropOffDateTime);
        dest.writeTypedList(this.orderItems);
    }

    protected OrderHistory(Parcel in) {
        this.instruction = in.readString();
        this.pickupLocation = in.readParcelable(OrderLocation.class.getClassLoader());
        this.dropOffLocation = in.readParcelable(OrderLocation.class.getClassLoader());
        this.orderID = in.readString();
        this.status = in.readString();
        this.orderTime = in.readString();
        this.paymentMethod = in.readString();
        this.deliveryMethod = in.readString();
        this.total = in.readString();
        this.promoCode = in.readString();
        this.pickupTimeStamp = in.readLong();
        this.dropOffTimeStamp = in.readLong();
        this.editable = in.readByte() != 0;
        this.cancelable = in.readByte() != 0;
        this.pickupDateTime = in.readString();
        this.dropOffDateTime = in.readString();
        this.orderItems = in.createTypedArrayList(OrderItem.CREATOR);
    }

    public static final Creator<OrderHistory> CREATOR = new Creator<OrderHistory>() {
        @Override
        public OrderHistory createFromParcel(Parcel source) {
            return new OrderHistory(source);
        }

        @Override
        public OrderHistory[] newArray(int size) {
            return new OrderHistory[size];
        }
    };
}
