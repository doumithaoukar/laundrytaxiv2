package com.acidwater.laundrytaxi.ordershistory.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.MainActivity;
import com.acidwater.laundrytaxi.MainActivityListener;
import com.acidwater.laundrytaxi.R;
import com.acidwater.laundrytaxi.bus.MainBus;
import com.acidwater.laundrytaxi.bus.events.UpdateHistoryList;
import com.acidwater.laundrytaxi.bus.events.UpdateTotal;
import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistory;
import com.acidwater.laundrytaxi.ordershistory.model.OrderHistoryWrapper;
import com.acidwater.laundrytaxi.ordershistory.presenter.OrdersHistoryPresenter;
import com.acidwater.laundrytaxi.utils.GeneralUtils;
import com.acidwater.laundrytaxi.utils.RxUtils;
import com.acidwater.laundrytaxi.utils.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Doumith on 7/8/2017.
 */
public class OrdersHistoryFragment extends MvpLceFragment<View, OrderHistoryWrapper, OrdersHistoryMvp.View, OrdersHistoryMvp.Presenter> implements OrdersHistoryMvp.View {


    private MainActivityListener mListener;
    private RecyclerView recyclerView;
    private String type;
    private RelativeLayout progressBar;
    private CompositeDisposable compositeDisposable;

    public static OrdersHistoryFragment newInstance(String type) {
        OrdersHistoryFragment ordersFragment = new OrdersHistoryFragment();
        Bundle b = new Bundle();
        b.putString("type", type);
        ordersFragment.setArguments(b);
        return ordersFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        RxUtils.disposeIfNotNull(compositeDisposable);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_orders_history, container, false);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));
        recyclerView.setLayoutManager(layoutManager);
        compositeDisposable = new CompositeDisposable();
        registerToBus();
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            type = savedInstanceState.getString("type");
        } else if (getArguments() != null) {
            type = getArguments().getString("type");
        }
        loadData(false);
    }


    private void registerToBus() {
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof UpdateHistoryList) {
                    loadData(false);
                }

            }

        }));
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("type", type);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }


    @Override
    public OrdersHistoryMvp.Presenter createPresenter() {
        return new OrdersHistoryPresenter(getContext());
    }


    public void setError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void setData(OrderHistoryWrapper data) {
        OrderHistoryAdapter historyAdapter = new OrderHistoryAdapter(getContext(), data.filter(type));
        historyAdapter.setOnItemClickListener(new OrderHistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(OrderHistory orderHistory) {
                mListener.navigateToOrderDetails(orderHistory);
            }

            @Override
            public void onCancelClicked(OrderHistory orderHistory) {
                confirmLogout(orderHistory);
            }

            @Override
            public void onEditClicked(OrderHistory orderHistory) {
                mListener.navigateToEditOrder(orderHistory);
            }
        });
        recyclerView.setAdapter(historyAdapter);

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void confirmLogout(final OrderHistory orderHistory) {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.cancel_order))
                .setMessage(getString(R.string.confirm_cancel))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.cancelOrder(orderHistory);
                    }

                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getHistory();
    }
}
