package com.acidwater.laundrytaxi.ordershistory.details.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.acidwater.laundrytaxi.data.order.OrderLocation;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 8/28/2017.
 */
public class OrderHistoryDetails implements Parcelable {

    @SerializedName("Status")
    private String status;
    @SerializedName("TimeOfOrder")
    private String timeOfOrder;
    @SerializedName("DeliveryMethod")
    private String deliveryMethod;
    @SerializedName("PaymentMethod")
    private String paymentMethod;
    @SerializedName("PickupTimestamp")
    private String pickupTimeStamp;
    @SerializedName("DropoffTimestamp")
    private String dropOffTimeStamp;
    @SerializedName("Instruction")
    private String instruction;
    @SerializedName("Total")
    private String total;
    @SerializedName("PickupLocation")
    private OrderLocation pickupLocation;
    @SerializedName("DropoffLocation")
    private OrderLocation dropOffLocation;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeOfOrder() {
        return timeOfOrder;
    }

    public void setTimeOfOrder(String timeOfOrder) {
        this.timeOfOrder = timeOfOrder;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPickupTimeStamp() {
        return pickupTimeStamp;
    }

    public void setPickupTimeStamp(String pickupTimeStamp) {
        this.pickupTimeStamp = pickupTimeStamp;
    }

    public String getDropOffTimeStamp() {
        return dropOffTimeStamp;
    }

    public void setDropOffTimeStamp(String dropOffTimeStamp) {
        this.dropOffTimeStamp = dropOffTimeStamp;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public OrderLocation getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(OrderLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public OrderLocation getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(OrderLocation dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.timeOfOrder);
        dest.writeString(this.deliveryMethod);
        dest.writeString(this.paymentMethod);
        dest.writeString(this.pickupTimeStamp);
        dest.writeString(this.dropOffTimeStamp);
        dest.writeString(this.instruction);
        dest.writeString(this.total);
        dest.writeParcelable(this.pickupLocation, flags);
        dest.writeParcelable(this.dropOffLocation, flags);
    }

    public OrderHistoryDetails() {
    }

    protected OrderHistoryDetails(Parcel in) {
        this.status = in.readString();
        this.timeOfOrder = in.readString();
        this.deliveryMethod = in.readString();
        this.paymentMethod = in.readString();
        this.pickupTimeStamp = in.readString();
        this.dropOffTimeStamp = in.readString();
        this.instruction = in.readString();
        this.total = in.readString();
        this.pickupLocation = in.readParcelable(OrderLocation.class.getClassLoader());
        this.dropOffLocation = in.readParcelable(OrderLocation.class.getClassLoader());
    }

    public static final Parcelable.Creator<OrderHistoryDetails> CREATOR = new Parcelable.Creator<OrderHistoryDetails>() {
        @Override
        public OrderHistoryDetails createFromParcel(Parcel source) {
            return new OrderHistoryDetails(source);
        }

        @Override
        public OrderHistoryDetails[] newArray(int size) {
            return new OrderHistoryDetails[size];
        }
    };
}
