package com.acidwater.laundrytaxi.ordershistory.model;

import android.content.Context;

import com.acidwater.laundrytaxi.ConstantStrings;
import com.acidwater.laundrytaxi.data.ServerResponse;
import com.acidwater.laundrytaxi.login.model.UserWrapper;
import com.acidwater.laundrytaxi.orderitems.model.OrderResult;
import com.acidwater.laundrytaxi.ordershistory.OrdersHistoryMvp;
import com.acidwater.laundrytaxi.pricelist.model.CatalogueCategoriesWrapper;
import com.acidwater.laundrytaxi.pricelist.model.CategoryItemWrapper;
import com.acidwater.laundrytaxi.services.TokenInterceptor;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 8/2/2017.
 */
public class OrderHistoryInteractor implements OrdersHistoryMvp.Interactor {


    private Context context;
    private OrderHistoryApi orderHistoryApi;

    public OrderHistoryInteractor(Context context) {
        this.context = context;
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                cookieJar(cookieJar).
                addInterceptor(new TokenInterceptor()).build();
        String baseURL = ConstantStrings.Base_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        orderHistoryApi = retrofit.create(OrderHistoryApi.class);
    }

    @Override
    public Observable<OrderHistoryWrapper> getHistory() {

        Observable<OrderHistoryWrapper> observable =
                orderHistoryApi.getHistory().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<OrderResult> cancelOrder(OrderHistory orderHistory) {

        Observable<OrderResult> observable = null;
        CancelRequest cancelRequest = new CancelRequest(orderHistory.getOrderID());
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (cancelRequest.toJson()).toString());
        observable =
                orderHistoryApi.cancelOrder(requestBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface OrderHistoryApi {

        @Headers("Content-Type: application/json")
        @GET(ConstantStrings.SERVICE_ORDER_HISTORY)
        Observable<OrderHistoryWrapper> getHistory();

        @Headers("Content-Type: application/json")
        @POST(ConstantStrings.SERVICE_CANCEL_ORDER)
        Observable<OrderResult> cancelOrder(@Body RequestBody body);
    }

    private class CancelRequest {

        @SerializedName("orderId")
        String orderId;


        public CancelRequest(String orderId) {
            this.orderId = orderId;
        }

        public JSONObject toJson() {
            try {
                return new JSONObject(new Gson().toJson(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }
    }
}
